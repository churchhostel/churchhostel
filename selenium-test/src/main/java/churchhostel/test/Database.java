package churchhostel.test;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.logging.Logger;

public class Database {

	private static final Logger log = Logger.getLogger(Database.class.getName());

	private static final String[] TRUNCATE_TABLES = new String[] { "additional_hint", "churchhostel", "contact",
			"facility", "image", "service", "trail" };

	private String url;
	private String user;
	private String password;

	Database(String url, String user, String password) throws ExceptionInInitializerError {
		this.url = url;
		this.user = user;
		this.password = password;
		try {
			testConnection();
		} catch (Exception e) {
			throw new ExceptionInInitializerError(e);
		}
	}

	private void testConnection() throws SQLException, FileNotFoundException {
		try (Connection con = DriverManager.getConnection(url, user, password);
				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT 1 FROM churchhostel")) {
			rs.next();
		}
	}

	public boolean clearTables() throws SQLException {
		try (Connection con = DriverManager.getConnection(url, user, password);
				Statement stmt = con.createStatement()) {

			for (String s : getTruncateStatements()) {
				stmt.addBatch(s);
			}
			int[] resultCodes = stmt.executeBatch();
			return Arrays.stream(resultCodes).noneMatch(code -> Statement.EXECUTE_FAILED == code);
		}
	}

	private String[] getTruncateStatements() {
		String[] sqlStatements = new String[TRUNCATE_TABLES.length + 2]; // +2 FOREIGN KEY Statements
		sqlStatements[0] = "SET FOREIGN_KEY_CHECKS = 0";
		for (int i = 0; i < TRUNCATE_TABLES.length; i++) {
			sqlStatements[i + 1] = "DELETE FROM " + TRUNCATE_TABLES[i];
		}
		sqlStatements[sqlStatements.length - 1] = "SET FOREIGN_KEY_CHECKS = 1";
		return sqlStatements;
	}

}
