package churchhostel.test;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;

import churchhostel.test.util.ScreenshotFailuresListener;

@Listeners({ ScreenshotFailuresListener.class })
public class TestBase extends Suite {

	private static final Logger LOG = Logger.getLogger("TestBase");

	protected WebDriver driver;

	@BeforeTest
	public void setupDriver(ITestContext testCtx) throws MalformedURLException {
		LOG.info("Setting up a new webdriver instance for test " + testCtx.getName());
//		FirefoxOptions options = new FirefoxOptions();
//		options.addArguments("--headless");
//		driver = new FirefoxDriver(options);
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.addArguments("--start-maximized");
		chromeOptions.addArguments("--disable-web-security");
		chromeOptions.addArguments("--window-size=1920,1080");
		capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
		capabilities.setJavascriptEnabled(true);
		WebDriver driver = new RemoteWebDriver(new URL(seleniumNodeUrl), capabilities);
		testCtx.setAttribute("driver", driver);
	}

	@AfterTest(alwaysRun = true)
	public void quitDriver(ITestContext testCtx) {
		LOG.info("Quitting webdriver instance after test " + testCtx.getName());
		getDriverFromContext(testCtx).quit();
	}

	@BeforeClass
	public void retrieveDriver(ITestContext testCtx) {
		driver = getDriverFromContext(testCtx);
	}

	@AfterClass(alwaysRun = true)
	public void resetState() throws SQLException {
		LOG.info("Cleaning database after test class");
		database.clearTables();
	}

	public WebDriver getDriver() {
		return driver;
	}

	private WebDriver getDriverFromContext(ITestContext testCtx) {
		return (WebDriver) testCtx.getAttribute("driver");
	}

	// Dataproviders

	// protected Churchhostel addFromFixture(String name) throws
	// FileNotFoundException {
	// Churchhostel loaded = fixturesLoader.loadHostel(name);
	// api.createHostel(APICommands.createFrom(loaded));
	// hostels.add(loaded);
	// return loaded;
	// }

	// @DataProvider(name = "hostels")
	// protected Object[][] dataProvider() {
	// return new Object[][] { hostels.stream().map(hostel -> new Object[] { hostel
	// }).toArray() };
	// }

}
