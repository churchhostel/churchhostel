package churchhostel.test;

import org.testng.ITestContext;
import org.testng.annotations.BeforeSuite;

import churchhostel.test.pages.Page;

public class Suite {
	protected static final String dbUrl = "jdbc:mysql://" + System.getProperty("dbUrl", "localhost")
			+ ":3306/ch?useSSL=false";
	protected static final String apiUrl = System.getProperty("apiUrl", "http://localhost:8080/api");
	protected static final String indexPage = System.getProperty("indexUrl", "http://localhost:8081/");
	protected static final String adminPage = indexPage + "/admin.html";
	protected static final String seleniumNodeUrl = "http://" + System.getProperty("seleniumNodeUrl", "localhost:4444")
			+ "/wd/hub";
	protected static final String dbUser = System.getProperty("dbUser", "root");
	protected static final String dbPassword = System.getProperty("dbPassword", "root");
	protected static final String apiUser = System.getProperty("apiUser", "test");
	protected static final String apiPassword = System.getProperty("apiPassword", "test");

	protected static Database database;
	protected static Api api;

	@BeforeSuite
	public void setup(ITestContext suiteCtx) throws ClassNotFoundException {
		// System.setProperty("webdriver.gecko.driver", "/usr/bin/geckodriver");
		// System.setProperty("phantomjs.binary.path",
		Class.forName("com.mysql.jdbc.Driver");
		System.out.println("Using DB at " + dbUrl);
		System.out.println("Accessing DB with user '" + dbUser + "' and password '" + dbPassword + "'");
		System.out.println("Using API at " + apiUrl);
		System.out.println("Accessing API with user '" + apiUser + "' and password '" + apiPassword + "'");
		System.out.println("Using index page at " + indexPage);
		System.out.println("Using admin page at " + adminPage);
		database = new Database(dbUrl, dbUser, dbPassword);
		api = new Api(apiUrl, apiUser, apiPassword);
		Page.indexUrl = indexPage;
		Page.adminUrl = adminPage;
	}
}
