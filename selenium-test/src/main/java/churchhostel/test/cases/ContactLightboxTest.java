package churchhostel.test.cases;

import static org.testng.Assert.assertTrue;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import churchhostel.test.TestBase;
import churchhostel.test.pages.ContactLightbox;
import churchhostel.test.pages.Header;
import churchhostel.test.pages.Menu;

public class ContactLightboxTest extends TestBase {

	private Menu menu;

	@BeforeMethod
	public void setupData() {
		menu = Header.init(driver).open().openMenu();
		assertTrue(menu.isAt());
	}

	@Test(description = "The contact lightbox should open and initially show the choice buttons")
	public void openContactLightbox() {
		ContactLightbox lightbox = menu.openContactLink();
		Assert.assertTrue(lightbox.isAt());
		Assert.assertTrue(lightbox.isAtChoice());
	}

	@Test(description = "Choosing the 'form' contact method should display the form")
	public void chooseFormAsContactMethod() {
		ContactLightbox lightbox = menu.openContactLink().chooseForm();
		Assert.assertTrue(lightbox.isAtForm());
	}

	@Test(description = "Choosing the 'other' contact method should display other means of contacting")
	public void chooseOtherAsContactMethod() {
		ContactLightbox lightbox = menu.openContactLink().chooseOther();
		Assert.assertTrue(lightbox.isAtOther());
	}

	@Test(description = "The lightbox should be closable")
	public void closeContactLightbox() {
		ContactLightbox lightbox = menu.openContactLink().chooseOther();
		Assert.assertTrue(lightbox.isAtOther());
		lightbox.close();
		Assert.assertFalse(lightbox.isAt());
	}
}
