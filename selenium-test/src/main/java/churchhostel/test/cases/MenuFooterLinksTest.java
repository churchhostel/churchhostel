package churchhostel.test.cases;

import static org.testng.Assert.assertTrue;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import churchhostel.test.TestBase;
import churchhostel.test.pages.AboutLightbox;
import churchhostel.test.pages.Header;
import churchhostel.test.pages.ImprintLightbox;
import churchhostel.test.pages.InfosForNewHostelsLightbox;
import churchhostel.test.pages.Menu;

public class MenuFooterLinksTest extends TestBase {

	private Menu menu;

	@BeforeMethod
	public void setupData() {
		menu = Header.init(driver).open().openMenu();
		assertTrue(menu.isAt());
	}

	@Test(description = "The 'Ueber Churchhostel' lightbox should open and close")
	public void openAboutChurchhostel() {
		AboutLightbox lightbox = menu.openAbout();
		Assert.assertTrue(lightbox.isAt());
		lightbox.close();
		Assert.assertFalse(lightbox.isAt());
	}

	@Test(description = "The 'Imprint' lightbox should open")
	public void openImprint() {
		ImprintLightbox lightbox = menu.openImprint();
		Assert.assertTrue(lightbox.isAt());
		lightbox.close();
		Assert.assertFalse(lightbox.isAt());
	}

	@Test(description = "The 'Infos fuer neue Gemeinden' lightbox should open")
	public void openInfosForNewHostels() {
		InfosForNewHostelsLightbox lightbox = menu.openInfosForNewHostels();
		Assert.assertTrue(lightbox.isAt());
		lightbox.close();
		Assert.assertFalse(lightbox.isAt());
	}

}
