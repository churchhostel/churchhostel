package churchhostel.test.cases;

import java.math.BigDecimal;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import churchhostel.test.TestBase;
import churchhostel.test.pages.Map;
import de.churchhostel.rest.hostel.dto.CreateHostel;

public class SelectMarkerOnMapTest extends TestBase {

	private Map map;

	@BeforeClass
	public void setupData() {
		CreateHostel hostelSetup = new CreateHostel.Builder(11.111111, 22.222222, "My hostel", "My Address", 5, BigDecimal.valueOf(10.99)).build();
		api.createHostel(hostelSetup);
	}

	@BeforeMethod
	public void navigateToMap() {
		map = Map.init(driver).open();
		Assert.assertTrue(map.isAt());
	}

	@Test(description = "When a hostel exists there should be a clickable marker on the map")
	public void selectAnyHostelFromMap() {
		Assert.assertTrue(map.getMarkers().size() > 0);
		map.clickMarker();
	}

}
