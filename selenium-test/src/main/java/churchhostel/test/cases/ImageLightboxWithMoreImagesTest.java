package churchhostel.test.cases;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.math.BigDecimal;

import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import churchhostel.test.TestBase;
import churchhostel.test.pages.HostelSidebar;
import churchhostel.test.pages.ImageLightbox;
import churchhostel.test.pages.Map;
import de.churchhostel.rest.hostel.dto.CreateHostel;
import de.churchhostel.rest.image.dto.CreateImage;

public class ImageLightboxWithMoreImagesTest extends TestBase {

	private HostelSidebar sidebar;

	@BeforeClass
	public void setupData() {
		CreateHostel hostelSetup = new CreateHostel.Builder(11.111111, 22.222222, "My hostel", "My Address", 5, BigDecimal.valueOf(10.99)).withFacility("Test facility 1")
				.withFacility("Test facility 2").withService("Test service 1").withService("Test service 2").withAdditionalHint("Test additional hint 1").withAdditionalHint("Test additional hint 2")
				.build();
		long hostelId = api.createHostel(hostelSetup);
		api.addImage(new CreateImage("https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Havana_Brown_-_choco.jpg/517px-Havana_Brown_-_choco.jpg", hostelId));
		api.addImage(new CreateImage("https://upload.wikimedia.org/wikipedia/commons/4/4c/Blackcat-Lilith.jpg", hostelId));
		api.addImage(new CreateImage("https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Britishblue.jpg/372px-Britishblue.jpg", hostelId));
	}

	@BeforeMethod
	public void navigateToSidebar() {
		Reporter.log("Driver is " + driver);
		sidebar = Map.init(driver).open().clickMarker();
		assertTrue(sidebar.isAt());
	}

	@Test(description = "When there is more than one image, next and previous buttons should be displayed")
	public void navigationButtonsShouldBeDisplayed() {
		ImageLightbox imageLightbox = sidebar.clickImage(0);
		assertTrue(imageLightbox.isAt());
		assertTrue(imageLightbox.isLeftButtonVisible());
		assertTrue(imageLightbox.isRightButtonVisible());
	}

	@Test(description = "Clicking an image should open exactly the clicked image")
	public void lightboxShouldOpenAtClickedImage() {
		ImageLightbox imageLightbox = sidebar.clickImage("https://upload.wikimedia.org/wikipedia/commons/4/4c/Blackcat-Lilith.jpg");
		assertTrue(imageLightbox.isAt());
		assertTrue(imageLightbox.getCurrentlyDisplayedImageUrl().equals("https://upload.wikimedia.org/wikipedia/commons/4/4c/Blackcat-Lilith.jpg"));
	}

	@Test(description = "Clicking the next button should move to the next image")
	public void showNextImage() {
		ImageLightbox imageLightbox = sidebar.clickImage("https://upload.wikimedia.org/wikipedia/commons/4/4c/Blackcat-Lilith.jpg");
		assertTrue(imageLightbox.isAt());
		imageLightbox.clickRightButton();
		assertFalse(imageLightbox.getCurrentlyDisplayedImageUrl().equals("https://upload.wikimedia.org/wikipedia/commons/4/4c/Blackcat-Lilith.jpg"));
	}

	@Test(description = "Clicking the previous button should move to the previous image")
	public void showPreviousImage() {
		ImageLightbox imageLightbox = sidebar.clickImage("http://myimage.url/img2.jpg");
		assertTrue(imageLightbox.isAt());
		imageLightbox.clickLeftButton();
		assertFalse(imageLightbox.getCurrentlyDisplayedImageUrl().equals("https://upload.wikimedia.org/wikipedia/commons/4/4c/Blackcat-Lilith.jpg"));
	}
}
