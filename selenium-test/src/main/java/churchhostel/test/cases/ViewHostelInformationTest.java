package churchhostel.test.cases;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import churchhostel.test.TestBase;
import churchhostel.test.pages.HostelSidebar;
import churchhostel.test.pages.Map;
import de.churchhostel.model.Contact;
import de.churchhostel.rest.contact.dto.CreateContact;
import de.churchhostel.rest.hostel.dto.CreateHostel;
import de.churchhostel.rest.image.dto.CreateImage;

public class ViewHostelInformationTest extends TestBase {

	private HostelSidebar sidebar;

	@BeforeClass
	public void setupData() {
		CreateHostel hostelSetup = new CreateHostel.Builder(11.111111, 22.222222, "My hostel", "My Address", 5, BigDecimal.valueOf(10.99)).withFacility("Test facility 1")
				.withFacility("Test facility 2").withService("Test service 1").withService("Test service 2").withAdditionalHint("Test additional hint 1").withAdditionalHint("Test additional hint 2")
				.build();
		long hostelId = api.createHostel(hostelSetup);
		api.addImage(new CreateImage("http://myimage.url/img1.jpg", hostelId));
		api.addImage(new CreateImage("http://myotherimage.url/img2.jpg", hostelId));
		api.addContact(new CreateContact.Builder("Hannes", "+49 12345", hostelId).build());
		api.addContact(new CreateContact.Builder("Peter Pansen", "015212345", hostelId).withEmail("peter@example.com").build());

		sidebar = Map.init(driver).open().clickMarker();
		assertTrue(sidebar.isAt());
	}

	@Test(description = "Compare hostel name")
	public void testName() {
		assertEquals("My hostel", sidebar.getName());
	}

	@Test(description = "Compare address")
	public void testAddress() {
		assertEquals("My Address", sidebar.getAddress());
	}

	@Test
	public void testImageUrls() {
		List<String> actualUrls = sidebar.getImageUrls().stream().sorted().collect(Collectors.toList());
		List<String> expected = Stream.of("http://myimage.url/img1.jpg", "http://myotherimage.url/img2.jpg").sorted().collect(Collectors.toList());
		assertEquals(actualUrls, expected);
	}

	@Test
	public void testContacts() {
		List<Contact> sidebarContacts = sidebar.getContacts();
		Optional<Contact> hannesContact = sidebarContacts.stream().filter(c -> c.getName().equals("Hannes")).findFirst();
		assertTrue(hannesContact.isPresent());
		assertEquals(hannesContact.get().getPhone(), "+49 12345");
		Optional<Contact> peterContact = sidebarContacts.stream().filter(c -> c.getName().equals("Peter Pansen")).findFirst();
		assertTrue(peterContact.isPresent());
		assertEquals(peterContact.get().getPhone(), "015212345");
		assertEquals(peterContact.get().getEmail(), "peter@example.com");
	}

	@Test
	public void testPrice() {
		assertEquals(10.99f, Float.parseFloat(sidebar.getPrice()), 0.001);
	}

	@Test
	public void testFacilities() {
		assertEquals(Arrays.asList("Test facility 1", "Test facility 2"), sidebar.getFacilities());
	}

	@Test
	public void testServices() {
		assertEquals(Arrays.asList("Test service 1", "Test service 2"), sidebar.getServices());
	}

	@Test
	public void testAdditionalHints() {
		assertEquals(Arrays.asList("Test additional hint 1", "Test additional hint 2"), sidebar.getAdditionalHints());
	}

	@Test
	public void testBeds() {
		assertTrue(sidebar.getBedsString().contains("5"));
	}
}
