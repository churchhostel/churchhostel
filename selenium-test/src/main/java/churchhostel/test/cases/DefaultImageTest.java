package churchhostel.test.cases;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.math.BigDecimal;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import churchhostel.test.TestBase;
import churchhostel.test.pages.HostelSidebar;
import churchhostel.test.pages.ImageLightbox;
import churchhostel.test.pages.Map;
import de.churchhostel.rest.hostel.dto.CreateHostel;

public class DefaultImageTest extends TestBase {

	private HostelSidebar sidebar;

	@BeforeClass
	public void setupData() {
		CreateHostel hostelSetup = new CreateHostel.Builder(11.111111, 22.222222, "My hostel", "My Address", 5, BigDecimal.valueOf(10.99)).withFacility("Test facility 1")
				.withFacility("Test facility 2").withService("Test service 1").withService("Test service 2").withAdditionalHint("Test additional hint 1").withAdditionalHint("Test additional hint 2")
				.build();
		api.createHostel(hostelSetup);
	}

	@BeforeMethod
	public void navigateToSidebar() {
		sidebar = Map.init(driver).open().clickMarker();
		assertTrue(sidebar.isAt());
	}

	@Test(description = "When a hostel has no image a default image should have been added")
	public void testForDefaultImage() {
		assertEquals(sidebar.getImageUrls().size(), 1);
	}

	@Test(description = "The default image should not be clickable")
	public void defaultImageShouldNotBeClickable() {
		ImageLightbox lightbox = sidebar.clickImage(0);
		assertFalse(lightbox.isAt());
	}

}
