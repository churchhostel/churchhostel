package churchhostel.test.cases.admin;

import org.testng.Assert;
import org.testng.annotations.Test;

import churchhostel.test.TestBase;
import churchhostel.test.pages.admin.AdminPage;

public class OpenAdminPageTest extends TestBase {

    @Test(description = "The Admin page should be reachable")
    public void openAdminPage() {
        Assert.assertTrue(new AdminPage(driver).open().isAt());
    }
}