package churchhostel.test.cases;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.math.BigDecimal;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import churchhostel.test.TestBase;
import churchhostel.test.pages.HostelSidebar;
import churchhostel.test.pages.ImageLightbox;
import churchhostel.test.pages.Map;
import de.churchhostel.rest.hostel.dto.CreateHostel;
import de.churchhostel.rest.image.dto.CreateImage;

public class ImageLightboxWithOneImageTest extends TestBase {

	private HostelSidebar sidebar;

	@BeforeClass
	public void setupData() {
		CreateHostel hostelSetup = new CreateHostel.Builder(11.111111, 22.222222, "My hostel", "My Address", 5, BigDecimal.valueOf(10.99)).withFacility("Test facility 1")
				.withFacility("Test facility 2").withService("Test service 1").withService("Test service 2").withAdditionalHint("Test additional hint 1").withAdditionalHint("Test additional hint 2")
				.build();
		long hostelId = api.createHostel(hostelSetup);
		api.addImage(new CreateImage("https://upload.wikimedia.org/wikipedia/commons/7/70/Example.png", hostelId));

	}

	@BeforeMethod
	public void navigateToSidebar() {
		sidebar = Map.init(driver).open().clickMarker();
		assertTrue(sidebar.isAt());
	}

	@Test(description = "When clicking the image the lightbox should open")
	public void testOpenLightbox() {
		ImageLightbox imageLightbox = sidebar.clickImage(0);
		assertTrue(imageLightbox.isAt());
	}

	@Test(description = "When there is only one image in the image lightbox, no navigation arrows should be displayed")
	public void testNoNavigationWithOneImage() {
		ImageLightbox imageLightbox = sidebar.clickImage(0);
		assertTrue(imageLightbox.isAt());
		assertFalse(imageLightbox.isLeftButtonVisible());
		assertFalse(imageLightbox.isRightButtonVisible());
	}

	@Test(description = "Clicking the X on the lightbox should close it")
	public void closeLightbox() {
		ImageLightbox imageLightbox = sidebar.clickImage(0);
		assertTrue(imageLightbox.isAt());
	}
}
