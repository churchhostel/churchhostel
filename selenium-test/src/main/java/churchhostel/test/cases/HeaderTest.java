package churchhostel.test.cases;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import churchhostel.test.TestBase;
import churchhostel.test.pages.Header;
import churchhostel.test.pages.Page;

public class HeaderTest extends TestBase {

	private Header header;

	@BeforeMethod
	public void launchTest() {
		header = Header.init(driver).open();
		Assert.assertTrue(header.isAt());
	}

	@Test(description = "The header text should say 'Churchhostel'")
	public void headerTextIsVisible() {
		Assert.assertEquals(header.getHeaderText(), "Churchhostel");
	}

	@Test(description = "A click on the header should navigate to the index page")
	public void clickOnHeaderLink() {
		header.clickHeader();
		Assert.assertTrue(driver.getCurrentUrl().equals(Page.indexUrl));
	}

}
