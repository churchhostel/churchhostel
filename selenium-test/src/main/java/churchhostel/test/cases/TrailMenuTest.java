package churchhostel.test.cases;

import static org.testng.Assert.assertTrue;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import churchhostel.test.TestBase;
import churchhostel.test.pages.Header;
import churchhostel.test.pages.Menu;
import de.churchhostel.rest.trail.dto.CreateTrail;

public class TrailMenuTest extends TestBase {

	private Menu menu;

	@BeforeClass
	public void setupData() {
		CreateTrail trail = new CreateTrail("Test trail", "http://example.com/trail", "Test start", "Test end", 100);
		CreateTrail trail2 = new CreateTrail("Test trail 2", "http://example.com/trail", "Test start", "Test end", 100);
		api.createTrail(trail);
		api.createTrail(trail2);

		menu = Header.init(driver).open().openMenu();
		assertTrue(menu.isAt());
	}

	@Test(description = "The trail menu should initially be opened and show all trails")
	public void trailsShouldBeShownInitially() {
		Assert.assertTrue(menu.isTrailListVisible());
	}

	@Test(description = "When clicking the trail menu, the trails should be hidden", dependsOnMethods = { "trailsShouldBeShownInitially" })
	public void clickingTheTrailMenuShouldHideTrails() {
		menu.toggleTrailMenu();
		Assert.assertFalse(menu.isTrailListVisible());
	}

	@Test(description = "All trails should be listed in the menu with their name")
	public void allTrailsShouldBeListed() {
		List<String> sortedExpectedNames = Stream.of("Test trail", "Test trail 2").sorted().collect(Collectors.toList());
		List<String> sortedActualNames = menu.getTrailNames().stream().sorted().collect(Collectors.toList());
		Assert.assertEquals(sortedActualNames, sortedExpectedNames);
	}
}
