package churchhostel.test.cases;

import static org.testng.Assert.assertTrue;

import java.math.BigDecimal;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import churchhostel.test.TestBase;
import churchhostel.test.pages.Header;
import churchhostel.test.pages.Map;
import churchhostel.test.pages.Menu;
import de.churchhostel.rest.hostel.dto.CreateHostel;

public class HostelBedsFilterTest extends TestBase {

	private Map map;
	private Menu menu;

	@BeforeClass
	public void setupData() {
		CreateHostel hostelWith1Bed = new CreateHostel.Builder(11.111111, 22.222222, "My hostel", "My Address", 1, BigDecimal.valueOf(10.99)).build();
		CreateHostel hostelWith5Beds = new CreateHostel.Builder(11.111111, 22.222222, "My hostel", "My Address", 5, BigDecimal.valueOf(10.99)).build();
		CreateHostel hostelWith7Beds = new CreateHostel.Builder(11.111111, 22.222222, "My hostel", "My Address", 7, BigDecimal.valueOf(10.99)).build();
		api.createHostel(hostelWith1Bed);
		api.createHostel(hostelWith5Beds);
		api.createHostel(hostelWith7Beds);

		map = Map.init(driver).open();
		menu = Header.init(driver).openMenu();
		assertTrue(map.isAt());
		assertTrue(menu.isAt());
	}

	@Test(description = "Initially, all markers should be shown on the map")
	public void allMarkersShouldBeShown() {
		Assert.assertEquals(map.getMarkers().size(), 3);
	}

	@Test(description = "When increasing the hostel beds filter, markers should be hidden on the map", dependsOnMethods = { "allMarkersShouldBeShown" })
	public void hideHostelsByIncreasingBedsFilter() {
		menu.increaseSlider(6);
		Assert.assertEquals(map.getMarkers().size(), 1);
	}

	@Test(description = "When decreasing the hostel beds filter, markers should be shown again", dependsOnMethods = { "hideHostelsByIncreasingBedsFilter" })
	public void showHostelsByDecreasingBedsFilter() {
		menu.decreaseSlider(2);
		Assert.assertEquals(map.getMarkers().size(), 2);
	}
}
