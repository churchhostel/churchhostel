package churchhostel.test;

import org.testng.TestNG;

import churchhostel.test.cases.ContactLightboxTest;
import churchhostel.test.cases.DefaultImageTest;
import churchhostel.test.cases.HostelBedsFilterTest;
import churchhostel.test.cases.ImageLightboxWithMoreImagesTest;
import churchhostel.test.cases.ImageLightboxWithOneImageTest;
import churchhostel.test.cases.MenuFooterLinksTest;
import churchhostel.test.cases.SelectMarkerOnMapTest;
import churchhostel.test.cases.TrailMenuTest;
import churchhostel.test.cases.ViewHostelInformationTest;
import churchhostel.test.cases.admin.OpenAdminPageTest;

public class Main {

	public static void main(String[] args) {
		TestNG tng = new TestNG();

		if (args.length != 1 || (!args[0].equals("visitor")) || !(args[0].equals("admin"))) {
			System.out.println("The test takes exactly one argument, 'visitor' or 'admin'");
		}

		tng.setTestClasses(args[0].equals("visitor") ? getVisitorPageTests() : getAdminPageTests());
		tng.run();
	}

	private static Class[] getVisitorPageTests() {
		return new Class[] { SelectMarkerOnMapTest.class, ViewHostelInformationTest.class, DefaultImageTest.class,
				ImageLightboxWithOneImageTest.class, ImageLightboxWithMoreImagesTest.class, HostelBedsFilterTest.class,
				TrailMenuTest.class, MenuFooterLinksTest.class, ContactLightboxTest.class };
	}

	private static Class[] getAdminPageTests() {
		return new Class[] { OpenAdminPageTest.class };
	}
}
