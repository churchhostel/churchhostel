package churchhostel.test;

import java.io.StringReader;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.churchhostel.rest.contact.dto.CreateContact;
import de.churchhostel.rest.hostel.dto.CreateHostel;
import de.churchhostel.rest.image.dto.CreateImage;
import de.churchhostel.rest.trail.dto.CreateTrail;

public final class Api {

	private static final Logger log = Logger.getLogger(Api.class.getName());

	private ObjectMapper objMapper = new ObjectMapper();
	private final Client client = ClientBuilder.newClient();
	private String url;
	private String token;

	public Api(String url, String user, String password) {
		this.url = url;
		JsonObject loginBody = Json.createObjectBuilder().add("username", user).add("password", password).build();
		Response loginResponse = POST(url + "/login", loginBody.toString());
		token = loginResponse.getHeaderString("Authorization");
		if (token == null) {
			throw new ExceptionInInitializerError("Could not acquire auth token.");
		}
	}

	public String addContact(CreateContact createContactCommand) {
		String contactResponse;
		try {
			contactResponse = POST(url + "/contact", objMapper.writeValueAsString(createContactCommand)).readEntity(String.class);
			return contactResponse;
		} catch (JsonProcessingException e) {
			log.warning("Failed to create contact" + e);
		}
		return null;
	}

	/**
	 * Returns the ID of the created hostel
	 * 
	 * @param createHostelCommand
	 * @return
	 */
	public long createHostel(CreateHostel createHostelCommand) {
		try {
			JsonObject responseJson = string2Object(POST(url + "/hostel", objMapper.writeValueAsString(createHostelCommand)).readEntity(String.class));
			return responseJson.getJsonNumber("id").longValue();
		} catch (JsonProcessingException e) {
			log.warning("Failed to create hostel" + e);
		}
		return -1;
	}

	public long createTrail(CreateTrail createTrailCommand) {
		try {
			JsonObject responseJson = string2Object(POST(url + "/trail", objMapper.writeValueAsString(createTrailCommand)).readEntity(String.class));
			return responseJson.getJsonNumber("id").longValue();
		} catch (JsonProcessingException e) {
			log.warning("Failed to create trail" + e);
		}
		return -1;
	}

	public long addImage(CreateImage createImage) {
		try {
			JsonObject responseObject = string2Object(POST(url + "/image", objMapper.writeValueAsString(createImage)).readEntity(String.class));
			return responseObject.getJsonNumber("id").longValue();
		} catch (JsonProcessingException e) {
			log.warning("Failed to add image" + e);
		}
		return -1;
	}

	public void cleanHostels() {
		JsonArray getHostelsResponseJson = Json.createReader(new StringReader(GET(url + "/hostel").readEntity(String.class))).readArray();
		for (JsonValue jsonValue : getHostelsResponseJson) {
			int hostelId = jsonValue.asJsonObject().getInt("id");
			DELETE(url + "/hostel/" + hostelId);
		}
	}

	public void cleanTrails() {
		// Clean all available trails
		JsonArray getTrailsResponseJson = Json.createReader(new StringReader(GET(url + "/trail").readEntity(String.class))).readArray();
		for (JsonValue jsonValue : getTrailsResponseJson) {
			int trailId = jsonValue.asJsonObject().getInt("id");
			DELETE(url + "/hostel/" + trailId);
		}
	}

	protected Response DELETE(String url) {
		WebTarget target = client.target(url);
		return target.request(MediaType.APPLICATION_JSON).delete();
	}

	protected Response GET(String url) {
		WebTarget target = client.target(url);
		return target.request(MediaType.APPLICATION_JSON).get();
	}

	protected Response POST(String url, Object entity) {
		WebTarget target = client.target(url);
		Response response = target.request(MediaType.APPLICATION_JSON).header("Authorization", token).post(Entity.entity(entity, MediaType.APPLICATION_JSON));
		if (response.getStatus() > 299) {
			System.out.println("WARNING: Error code 300 or above while sending POST request: " + response.getStatus() + " : " + response.readEntity(String.class));
		}
		return response;
	}

	protected Response PUT(String url, Object entity) {
		WebTarget target = client.target(url);
		Response response = target.request(MediaType.APPLICATION_JSON).header("Authorization", token).put(Entity.entity(entity, MediaType.APPLICATION_JSON));
		if (response.getStatus() > 299) {
			System.out.println("WARNING: Error code 300 or above while sending POST request: " + response.getStatus() + " : " + response.readEntity(String.class));
		}
		return response;
	}

	protected JsonObject string2Object(String s) {
		JsonReader reader = Json.createReader(new StringReader(s));
		JsonObject obj = reader.readObject();
		reader.close();
		return obj;
	}
}
