package churchhostel.test.util;

import java.util.logging.Logger;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.TestListenerAdapter;

import churchhostel.test.TestBase;

public class ScreenshotFailuresListener extends TestListenerAdapter {

	private ScreenshotTaker screenshotTaker = new ScreenshotTaker();

	@Override
	public void onConfigurationFailure(ITestResult tr) {
		try {
			WebDriver driver = ((TestBase) tr.getInstance()).getDriver();
			screenshotTaker.takeScreenshot(driver, tr);
		} catch (Exception e) {
			Logger.getLogger(getClass().getName())
					.warning("Could not take screenshot from failed test " + tr.getTestName() + e.getMessage());
		}
	}

	@Override
	public void onTestFailure(ITestResult tr) {
		try {
			WebDriver driver = ((TestBase) tr.getInstance()).getDriver();
			Reporter.setCurrentTestResult(tr);
			Reporter.log("<h3>" + tr.getThrowable().getMessage() + "</h3>");
			String base64Screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64);
			Reporter.log("<img src='data:image/png;base64," + base64Screenshot + "' /> ");
			Reporter.setCurrentTestResult(null);
			screenshotTaker.takeScreenshot(driver, tr);
		} catch (Exception e) {
			Logger.getLogger(getClass().getName())
					.warning("Could not take screenshot from failed test " + tr.getTestName() + e.getMessage());
		}
	}

}
