package churchhostel.test.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;

public class ScreenshotTaker {

	public void takeScreenshot(WebDriver driver, ITestResult testResult, String filename) throws IOException {
		// Make sure the output directory exists or create it otherwise
		String reportDir = testResult.getTestContext().getOutputDirectory();
		Path screenshotDir = Paths.get(reportDir, "screenshots");
		if (!Files.exists(screenshotDir)) {
			Files.createDirectories(screenshotDir);
		}

		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		File destination = screenshotDir.resolve(filename + ".jpg").toFile();
		System.out.println("Saving screenshot as " + destination.getAbsolutePath());
		FileUtils.copyFile(screenshot, destination);
		Reporter.setCurrentTestResult(testResult);
		Reporter.log(String.format("<img src=\"%s\">Screenshot</img>", destination.getAbsolutePath()));
		Reporter.setCurrentTestResult(null);
	}

	public void takeScreenshot(WebDriver driver, ITestResult testResult) throws IOException {
		takeScreenshot(driver, testResult, testResult.getName());
	}
}
