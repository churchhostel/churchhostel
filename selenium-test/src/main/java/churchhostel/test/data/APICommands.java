package churchhostel.test.data;

import de.churchhostel.model.Churchhostel;
import de.churchhostel.rest.hostel.dto.CreateHostel;

public class APICommands {

	public static CreateHostel createFrom(Churchhostel h) {
		CreateHostel createHostelCommand = new CreateHostel();
		createHostelCommand.setLatitude(h.getLatitude());
		createHostelCommand.setLongitude(h.getLongitude());
		createHostelCommand.setName(h.getName());
		createHostelCommand.setAddress(h.getAddress());
		createHostelCommand.setBeds(h.getBeds());
		createHostelCommand.setEmail(h.getEmail());
		createHostelCommand.setPrice(h.getPrice());
		createHostelCommand.setAdditionalHints(h.getAdditionalHints());
		createHostelCommand.setServices(h.getServices());
		createHostelCommand.setFacilities(h.getFacilities());
		return createHostelCommand;
	}
}
