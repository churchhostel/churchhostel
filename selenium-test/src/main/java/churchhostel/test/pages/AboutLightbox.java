package churchhostel.test.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class AboutLightbox extends Lightbox {

	private AboutLightbox(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	public static AboutLightbox init(WebDriver driver) {
		return new AboutLightbox(driver);
	}

	@Override
	public boolean isAt() {
		return "Was ist \"Churchhostel\" ?".equals(getVisibleHeader().map(WebElement::getText).orElse(""));
	}

}
