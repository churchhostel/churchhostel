package churchhostel.test.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ContactLightbox extends Lightbox {

	@FindBy(css = ".contact-lightbox")
	private WebElement lightbox;
	@FindBy(css = ".contact-lightbox-choice-form")
	private WebElement chooseFormButton;
	@FindBy(css = ".contact-lightbox-choice-other")
	private WebElement chooseOtherButton;
	@FindBy(css = ".contact-lightbox-form")
	private WebElement contactForm;
	@FindBy(css = ".contact-lightbox-other")
	private WebElement contactOther;

	private ContactLightbox(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	public static ContactLightbox init(WebDriver driver) {
		return new ContactLightbox(driver);
	}

	/**
	 * Needs to be overriden because the sub pages have their own close button, so
	 * the currently visible close button needs to be found.
	 */
	@Override
	public Menu close() {
		getVisibleCloseButton().ifPresent(WebElement::click);
		return Menu.init(driver);
	}

	@Override
	public boolean isAt() {
		return lightbox.isDisplayed();
	}

	public boolean isAtChoice() {
		return chooseFormButton.isDisplayed();
	}

	public boolean isAtForm() {
		return contactForm.isDisplayed();
	}

	public boolean isAtOther() {
		return contactOther.isDisplayed();
	}

	public ContactLightbox chooseForm() {
		chooseFormButton.click();
		return this;
	}

	public ContactLightbox chooseOther() {
		chooseOtherButton.click();
		return this;
	}
}
