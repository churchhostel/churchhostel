package churchhostel.test.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class InfosForNewHostelsLightbox extends Lightbox {

	private InfosForNewHostelsLightbox(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	public static InfosForNewHostelsLightbox init(WebDriver driver) {
		return new InfosForNewHostelsLightbox(driver);
	}

	@Override
	public boolean isAt() {
		return getVisibleHeader().map(WebElement::getText).orElse("").contains("Informationen");
	}

}
