package churchhostel.test.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import de.churchhostel.model.Contact;

public class HostelSidebar extends Page {

	@FindBy(id = "sidebar")
	private WebElement self;
	@FindBy(id = "hostelName")
	private WebElement name;
	@FindBy(id = "hostelAddress")
	private WebElement address;
	@FindBy(how = How.CSS, using = "#hostelImages img")
	private List<WebElement> images;
	@FindBy(how = How.CSS, using = ".contact")
	private List<WebElement> contacts;
	private By contactNameSelector = By.cssSelector(".contact-name");
	private By contactPhoneSelector = By.cssSelector(".contact-phone");
	private By contactMailSelector = By.cssSelector(".contact-mail");
	@FindBy(how = How.CSS, using = ".beds")
	private WebElement beds;
	@FindBy(how = How.CSS, using = ".facility")
	private List<WebElement> facilities;
	@FindBy(how = How.CSS, using = ".service")
	private List<WebElement> services;
	@FindBy(how = How.CSS, using = ".additional-hint")
	private List<WebElement> additionalHints;
	@FindBy(id = "hostelPrice")
	private WebElement price;

	private HostelSidebar(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	public static HostelSidebar init(WebDriver usingDriver) {
		return new HostelSidebar(usingDriver);
	}

	public HostelSidebar open(int specificHostelId) {
		driver.get(indexUrl + "/hostel/" + specificHostelId);
		return this;
	}

	@Override
	public boolean isAt() {
		return self.isDisplayed();
	}

	public String getName() {
		return name.getText();
	}

	public String getAddress() {
		return address.getText();
	}

	public List<String> getImageUrls() {
		return images.stream().map(e -> e.getAttribute("src")).collect(Collectors.toList());
	}

	public ImageLightbox clickImage(int atIndex) {
		images.get(atIndex).click();
		return ImageLightbox.init(driver);
	}

	public ImageLightbox clickImage(String withUrl) {
		images.stream().filter(img -> img.getAttribute("src").equals(withUrl)).findFirst().ifPresent(WebElement::click);
		return ImageLightbox.init(driver);
	}

	public List<Contact> getContacts() {
		List<Contact> cls = new ArrayList<>();
		for (WebElement elem : contacts) {
			Contact c = new Contact(elem.findElement(contactNameSelector).getText(), elem.findElement(contactPhoneSelector).getText());
			WebElement mailElement = elem.findElement(contactMailSelector);
			if (mailElement.isDisplayed()) {
				c.setEmail(mailElement.getText());
			}
			cls.add(c);
		}
		if (contacts.size() != cls.size()) {
			Logger.getLogger("HostelSidebarPageobject").warning("Contact elements and contact objects size did not match");
		}
		return cls;
	}

	public String getBedsString() {
		return beds.getText();
	}

	public List<String> getFacilities() {
		return facilities.stream().map(WebElement::getText).collect(Collectors.toList());
	}

	public List<String> getServices() {
		return services.stream().map(WebElement::getText).collect(Collectors.toList());
	}

	public List<String> getAdditionalHints() {
		return additionalHints.stream().map(WebElement::getText).collect(Collectors.toList());
	}

	public String getPrice() {
		return price.getText();
	}
}
