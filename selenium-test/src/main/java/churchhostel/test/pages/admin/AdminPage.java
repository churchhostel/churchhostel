package churchhostel.test.pages.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import churchhostel.test.pages.Page;

public class AdminPage extends Page {

    @FindBy(how = How.CSS, using = "#loginLightbox input[name='username']")
    private WebElement loginName;
    @FindBy(how = How.CSS, using = "#loginLightbox input[name='password']")
    private WebElement loginPassword;
    @FindBy(how = How.CSS, using = "#loginLightbox .submit-button")
    private WebElement loginSubmit;

    public AdminPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public AdminPage open() {
        driver.get(adminUrl);
        return this;
    }

    @Override
    public boolean isAt() {
        return driver.getTitle().equals("Churchhostel Admin");
    }

}