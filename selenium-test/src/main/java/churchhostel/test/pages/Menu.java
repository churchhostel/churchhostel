package churchhostel.test.pages;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Menu extends Page {

	@FindBy(id = "menu")
	private WebElement menu;
	@FindBy(id = "aboutChurchhostel")
	private WebElement aboutLink;
	@FindBy(id = "infoForNewHostels")
	private WebElement infosForNewHostelsLink;
	@FindBy(id = "contactLink")
	private WebElement contactLink;
	@FindBy(id = "imprint")
	private WebElement imprintLink;
	@FindBy(css = ".slider")
	private WebElement filterBedsSlider;
	@FindBy(css = ".filter-slider-label")
	private WebElement filterBedsSliderLabel;
	@FindBy(css = ".routes-menu .menu-button-wrapper")
	private WebElement trailMenuToggle;
	@FindBy(css = ".routes-menu .menu-content")
	private WebElement trailList;
	@FindBy(css = ".routes-menu .menu-content li")
	private List<WebElement> trails;
	@FindBy(id = "mobileMenuButton")
	private WebElement menuButton;

	private Menu(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	public static Menu init(WebDriver driver) {
		return new Menu(driver);
	}

	public Menu increaseSlider(int by) {
		IntStream.range(0, by).forEach(_x_ -> filterBedsSlider.sendKeys(Keys.RIGHT));
		return this;
	}

	public Menu decreaseSlider(int by) {
		IntStream.range(0, by).forEach(_x_ -> filterBedsSlider.sendKeys(Keys.LEFT));
		return this;
	}

	public Menu toggleTrailMenu() {
		trailMenuToggle.click();
		return this;
	}

	public boolean isTrailListVisible() {
		return isVisible(trailList);
	}

	public List<String> getTrailNames() {
		return trails.stream().map(WebElement::getText).collect(Collectors.toList());
	}

	public AboutLightbox openAbout() {
		aboutLink.click();
		return AboutLightbox.init(driver);
	}

	public InfosForNewHostelsLightbox openInfosForNewHostels() {
		infosForNewHostelsLink.click();
		return InfosForNewHostelsLightbox.init(driver);
	}

	public ContactLightbox openContactLink() {
		contactLink.click();
		return ContactLightbox.init(driver);
	}

	public ImprintLightbox openImprint() {
		imprintLink.click();
		return ImprintLightbox.init(driver);
	}

	public Map close() {
		menuButton.click();
		return Map.init(driver);
	}

	@Override
	public boolean isAt() {
		return !menu.getAttribute("class").contains("menu-hidden");
	}
}
