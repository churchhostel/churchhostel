package churchhostel.test.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Map extends Page {

	@FindBy(how = How.CSS, using = "div[title*='Churchhostel Marker']")
	private List<WebElement> markers;

	private Map(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	public static Map init(WebDriver usingDriver) {
		return new Map(usingDriver);
	}

	public Map open() {
		driver.get(indexUrl);
		return this;
	}

	public HostelSidebar clickMarker() {
		List<WebElement> visibleMarkers = getMarkers();
		if (!visibleMarkers.isEmpty()) {
			visibleMarkers.get(0).click();
		}
		return HostelSidebar.init(driver);
	}

	public List<WebElement> getMarkers() {
		// return new WebDriverWait(driver,
		// 5).until(ExpectedConditions.visibilityOfAllElements(markers));
		return new WebDriverWait(driver, 5).until(
				ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div[title*='Churchhostel Marker']")));
	}

	@Override
	public boolean isAt() {
		return getTitle().matches("Churchhostel");
	}
}
