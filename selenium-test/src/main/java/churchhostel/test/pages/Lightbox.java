package churchhostel.test.pages;

import java.util.List;
import java.util.Optional;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public abstract class Lightbox extends Page {

	@FindBy(css = ".lightbox-close-button")
	protected List<WebElement> closeButtons;
	@FindBy(css = ".lightbox-header")
	protected List<WebElement> headers;

	protected Lightbox(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	protected Optional<WebElement> getVisibleCloseButton() {
		return closeButtons.stream().filter(WebElement::isDisplayed).findFirst();
	}

	protected Optional<WebElement> getVisibleHeader() {
		return headers.stream().filter(WebElement::isDisplayed).findFirst();
	}

	public Menu close() {
		getVisibleCloseButton().ifPresent(WebElement::click);
		return Menu.init(driver);
	}
}
