package churchhostel.test.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Abstract class representation of a Page in the UI. Page object pattern
 */
public abstract class Page {

	public static String indexUrl;
	public static String adminUrl;

	protected WebDriver driver;

	/*
	 * Constructor injecting the WebDriver interface
	 * 
	 * @param webDriver
	 */
	public Page(WebDriver driver) {
		this.driver = driver;
	}

	public String getTitle() {
		return driver.getTitle();
	}

	public abstract boolean isAt();

	protected boolean isVisible(WebElement element) {
		return element.isDisplayed() && (element.getSize().width > 0 && element.getSize().height > 0);
	}

}
