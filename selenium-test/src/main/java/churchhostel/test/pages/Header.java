package churchhostel.test.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Header extends Page {

	@FindBy(id = "headerText")
	private WebElement text;
	@FindBy(id = "headerLogo")
	private WebElement logo;
	@FindBy(id = "mobileMenuButton")
	private WebElement menuButton;
	@FindBy(id = "pac-input")
	private WebElement searchInput;

	private Header(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	public static Header init(WebDriver driver) {
		return new Header(driver);
	}

	public Header open() {
		driver.get(indexUrl);
		return this;
	}

	public String getHeaderText() {
		return text.getText();
	}

	public Header clickHeader() {
		text.click();
		return this;
	}

	public Header enterSearchValue(String value) {
		searchInput.sendKeys(value);
		return this;
	}

	public Header submitSearch() {
		searchInput.sendKeys(Keys.RETURN);
		return this;
	}

	public Menu openMenu() {
		menuButton.click();
		return Menu.init(driver);
	}

	@Override
	public boolean isAt() {
		return "Churchhostel".equals(text.getText());
	}
}
