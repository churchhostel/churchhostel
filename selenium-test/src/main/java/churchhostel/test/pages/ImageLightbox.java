package churchhostel.test.pages;

import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ImageLightbox extends Page {

	@FindBy(id = "image-lightbox")
	private WebElement lightbox;
	@FindBy(how = How.CSS, using = "#image-lightbox .images img")
	private List<WebElement> images;
	@FindBy(how = How.CSS, using = "#image-lightbox .nav-buttons .left-button")
	private WebElement leftButton;
	@FindBy(how = How.CSS, using = "#image-lightbox .nav-buttons .right-button")
	private WebElement rightButton;
	@FindBy(how = How.CSS, using = "#image-lightbox .close-button")
	private WebElement closeButton;

	private ImageLightbox(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	public static ImageLightbox init(WebDriver usingDriver) {
		return new ImageLightbox(usingDriver);
	}

	@Override
	public boolean isAt() {
		return lightbox.isDisplayed();
	}

	public List<String> getImageUrls() {
		return images.stream().map(e -> e.getAttribute("src")).collect(Collectors.toList());
	}

	public boolean isLeftButtonVisible() {
		return leftButton.isDisplayed();
	}

	public boolean isRightButtonVisible() {
		return rightButton.isDisplayed();
	}

	public void clickLeftButton() {
		leftButton.click();
	}

	public void clickRightButton() {
		rightButton.click();
	}

	public HostelSidebar close() {
		closeButton.click();
		return HostelSidebar.init(driver);
	}

	public String getCurrentlyDisplayedImageUrl() {
		return images.stream().filter(imgElem -> imgElem.isDisplayed()).findFirst().get().getAttribute("src");
	}
}
