package churchhostel.test.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class ImprintLightbox extends Lightbox {

	private ImprintLightbox(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	public static ImprintLightbox init(WebDriver driver) {
		return new ImprintLightbox(driver);
	}

	@Override
	public boolean isAt() {
		return "Impressum".equals(getVisibleHeader().map(WebElement::getText).orElse(""));
	}

}
