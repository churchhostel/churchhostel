docker network create --driver bridge chnw
docker run --rm --name chdb -p 3306:3306 -d --net=chnw ch/database
docker run --rm --name chserver -v %cd%\server\config\config_docker:/mnt/config -p 8080:8080 -d --net=chnw ch/server
docker run --rm --name chwebapp -p 8081:80 -d --net=chnw ch/webapp
docker run --rm --name selenium -d -p 4444:4444 --net=chnw selenium/standalone-chrome:3.6.0

