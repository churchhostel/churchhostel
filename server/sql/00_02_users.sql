USE `ch`;

-- pw_hash is 44 chars fixed size, because thats the size of a base64 encoded SHA-256 encrypted string
CREATE TABLE `ch_user` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) UNIQUE NOT NULL,
  `pw_hash` char(44) NOT NULL
) ENGINE=InnoDB;

INSERT INTO `schema_migrations` (major_version, minor_version, script_name) VALUES
    (0, 2, "00_02_users.sql");
