USE `ch`;

ALTER TABLE `churchhostel` ADD email VARCHAR(100) DEFAULT NULL;

INSERT INTO `schema_migrations` (major_version, minor_version, script_name) VALUES
    (0, 4, "00_04_churchhostel_mail.sql");
