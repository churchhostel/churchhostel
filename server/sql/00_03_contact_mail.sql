USE `ch`;

-- pw_hash is 44 chars fixed size, because thats the size of a base64 encoded SHA-256 encrypted string
ALTER TABLE `contact` ADD email VARCHAR(100) DEFAULT NULL;

INSERT INTO `schema_migrations` (major_version, minor_version, script_name) VALUES
    (0, 3, "00_03_contact_mail.sql");
