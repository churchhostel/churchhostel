CREATE DATABASE `ch` CHARACTER SET utf8 COLLATE utf8_general_ci;

USE `ch`;

CREATE TABLE `churchhostel` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `address` varchar(255) NOT NULL,
  `beds` int(11) NOT NULL,
  `latitude` decimal(9,6) NOT NULL,
  `longitude` decimal(9,6) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(19,2) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB;

CREATE TABLE `additional_hint` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `churchhostel_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  FOREIGN KEY (`churchhostel_id`) 
    REFERENCES `churchhostel`(`id`) 
    ON UPDATE CASCADE
    ON DELETE CASCADE
) ENGINE=InnoDB;


CREATE TABLE `contact` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `churchhostel_id` int(11) NOT NULL,
  FOREIGN KEY (`churchhostel_id`)
    REFERENCES `churchhostel`(`id`) 
    ON UPDATE CASCADE
    ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE `facility` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `churchhostel_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  FOREIGN KEY (`churchhostel_id`) 
    REFERENCES `churchhostel`(`id`) 
    ON UPDATE CASCADE
    ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE `image` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `filePath` varchar(512) NOT NULL,
  `churchhostel_id` int(11) NOT NULL,
  FOREIGN KEY (`churchhostel_id`) 
    REFERENCES `churchhostel`(`id`) 
    ON UPDATE CASCADE
    ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE `service` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `churchhostel_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  FOREIGN KEY (`churchhostel_id`)
    REFERENCES `churchhostel`(`id`) 
    ON UPDATE CASCADE
    ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE `trail` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `fileUrl` varchar(255),
  `length` SMALLINT UNSIGNED,
  `name` varchar(255) NOT NULL,
  `startLocation` varchar(255) NOT NULL,
  `endLocation` varchar(255)
) ENGINE=InnoDB;

CREATE TABLE `schema_migrations` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `major_version` TINYINT UNSIGNED NOT NULL,
  `minor_version` TINYINT UNSIGNED NOT NULL,
  `script_name` VARCHAR(128) NOT NULL,
  `applied_on` DATETIME DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB;

INSERT INTO `schema_migrations` (major_version, minor_version, script_name) VALUES
    (0, 1, "baseline.sql");
