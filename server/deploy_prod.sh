#!/bin/bash
ssh churchhostel 'systemctl stop churchhostel-api'
scp build/libs/server-1.0-SNAPSHOT.jar churchhostel:/opt/api/api.jar
ssh churchhostel 'systemctl start churchhostel-api'
