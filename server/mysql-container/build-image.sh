#!/bin/bash
# Copy all sql files to entrypoint directory
cp -r ../sql docker-entrypoint-initdb.d 
cp test_user.sql docker-entrypoint-initdb.d/99_test_user.sql

docker build . -t ch/database

rm -r docker-entrypoint-initdb.d/
