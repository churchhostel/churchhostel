#!/bin/bash
# Build the server jar first
../gradlew :server:build

docker build . -t ch/server
