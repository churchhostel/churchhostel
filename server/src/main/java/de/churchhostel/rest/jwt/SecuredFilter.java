package de.churchhostel.rest.jwt;

import de.churchhostel.ServerConfiguration;
import io.jsonwebtoken.Jwts;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import static de.churchhostel.util.Utils.nullOrEmpty;

@Provider
@Secured
@Priority(Priorities.AUTHENTICATION)
public class SecuredFilter implements ContainerRequestFilter {

    private static final Logger log = LogManager.getLogger(SecuredFilter.class);

    @Override
    public void filter(ContainerRequestContext requestContext) {
        String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        if (nullOrEmpty(authorizationHeader)) {
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
            return;
        }
        String token = authorizationHeader.substring("Bearer".length()).trim();
        try {
            Jwts.parser().setSigningKey(ServerConfiguration.getJWTSigningKey()).parseClaimsJws(token);
            log.info("Request to " + requestContext.getUriInfo() + " was granted after verfying token " + token);
        } catch (Exception e) {
            log.warn("Request to " + requestContext.getUriInfo() + " was DENIED. Invalid token " + token);
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
        }
    }
}
