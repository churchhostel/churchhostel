package de.churchhostel.rest;

import de.churchhostel.model.Marker;
import de.churchhostel.repository.ChurchhostelRepository;
import de.churchhostel.service.MarkerService;
import de.churchhostel.service.TransactionManagedMarkerService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * REST entry point for the marker resource.
 * Provides Methods for accessing markers.
 */
@Path("/marker")
public class MarkerResource {

	private MarkerService markerService;

	public MarkerResource() {
		markerService = new TransactionManagedMarkerService(new ChurchhostelRepository());
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllMarker() {
		List<Marker> markers = markerService.getAllMarker();
		return Response.ok(markers).build();
	}

}
