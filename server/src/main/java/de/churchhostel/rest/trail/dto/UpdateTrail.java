package de.churchhostel.rest.trail.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class UpdateTrail {

    private String name;
    @Pattern(regexp = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]", message = "Invalid file URL number")
    private String fileUrl;
    private String startLocation;
    private String endLocation;
    private Integer length;
    private Long churchhostelId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(String startLocation) {
        this.startLocation = startLocation;
    }

    public String getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(String endLocation) {
        this.endLocation = endLocation;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Long getChurchhostelId() {
        return churchhostelId;
    }

    public void setChurchhostelId(Long churchhostelId) {
        this.churchhostelId = churchhostelId;
    }
}
