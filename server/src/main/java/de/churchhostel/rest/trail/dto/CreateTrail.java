package de.churchhostel.rest.trail.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class CreateTrail {

	@NotNull
	private String name;
	@NotNull
	@Pattern(regexp = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]", message = "Invalid file URL number")
	private String fileUrl;
	@NotNull
	private String startLocation;
	@NotNull
	private String endLocation;
	@NotNull
	private Integer length;

	public CreateTrail() {

	}

	public CreateTrail(String name, String fileUrl, String startLocation, String endLocation, Integer length) {
		this.name = name;
		this.fileUrl = fileUrl;
		this.startLocation = startLocation;
		this.endLocation = endLocation;
		this.length = length;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	public String getStartLocation() {
		return startLocation;
	}

	public void setStartLocation(String startLocation) {
		this.startLocation = startLocation;
	}

	public String getEndLocation() {
		return endLocation;
	}

	public void setEndLocation(String endLocation) {
		this.endLocation = endLocation;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

}
