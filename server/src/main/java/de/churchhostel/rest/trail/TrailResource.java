package de.churchhostel.rest.trail;

import de.churchhostel.model.Trail;
import de.churchhostel.repository.TrailRepository;
import de.churchhostel.rest.jwt.Secured;
import de.churchhostel.rest.trail.dto.CreateTrail;
import de.churchhostel.rest.trail.dto.UpdateTrail;
import de.churchhostel.service.TrailService;
import de.churchhostel.service.TransactionManagedTrailService;
import de.churchhostel.service.exception.InvalidDataException;
import de.churchhostel.service.exception.TrailNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import static de.churchhostel.util.Utils.nullOrEmpty;

/**
 * REST entry point for the trail resource.
 * Provides Methods for accessing trails.
 */
@Path("/trail")
public class TrailResource {
	
	private static final Logger log = LogManager.getLogger(TrailResource.class);

	private TrailService trailService;

	public TrailResource() {
        trailService = new TransactionManagedTrailService(new TrailRepository());
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {
		List<Trail> trails = trailService.getAll();
		return Response.ok(trails).build();
	}


	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOne(@PathParam("id") long id) throws TrailNotFoundException {
        Trail trail = trailService.getTrail(id);
		return Response.ok(trail).build();
	}

	@POST
    @Secured
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createTrail(@Valid @NotNull CreateTrail createData) throws InvalidDataException {
        Trail trail = trailService.addTrail(createModel(createData));
		return Response.status(Response.Status.CREATED).entity(trail).build();
	}

	@PUT
    @Secured
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateTrail(@PathParam("id") long id, UpdateTrail updateData) throws InvalidDataException {
        trailService.updateTrail(id, createModel(updateData));
		return Response.ok().build();
	}

	@DELETE
    @Secured
	@Path("{id}")
	public Response deleteTrail(@PathParam("id") long id) {
        trailService.deleteTrail(id);
		return Response.ok().build();
	}

	private Trail createModel(CreateTrail c) {
	    Trail t = new Trail(c.getName(), c.getFileUrl(), c.getStartLocation(), c.getEndLocation(), c.getLength());
	    return t;
    }
    private Trail createModel(UpdateTrail c) {
        Trail t = new Trail();
        if (!nullOrEmpty(c.getName())) {
            t.setName(c.getName());
        }
        if (!nullOrEmpty(c.getFileUrl())) {
            t.setFileUrl(c.getFileUrl());
        }
        if (!nullOrEmpty(c.getStartLocation())) {
            t.setStartLocation(c.getStartLocation());
        }
        if (!nullOrEmpty(c.getEndLocation())) {
            t.setEndLocation(c.getEndLocation());
        }
        if (c.getLength() != null) {
            t.setLength(c.getLength());
        }
        return t;
    }

}
