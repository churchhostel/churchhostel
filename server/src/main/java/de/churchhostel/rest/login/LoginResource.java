package de.churchhostel.rest.login;

import de.churchhostel.ServerConfiguration;
import de.churchhostel.repository.UserRepository;
import de.churchhostel.rest.jwt.Secured;
import de.churchhostel.rest.login.dto.Credentials;
import de.churchhostel.service.AuthenticationService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

@Path("/login")
public class LoginResource {

    private AuthenticationService authService = new AuthenticationService(new UserRepository());

    @Context
    UriInfo uriInfo;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(@Valid @NotNull Credentials credentials) {
        if (!authService.isAuthenticated(credentials.getUsername(), credentials.getPassword())) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        // Create a token
        String jwtToken = Jwts.builder()
            .setSubject(credentials.getUsername())
            .setIssuer(uriInfo.getAbsolutePath().toString())
            .setIssuedAt(new Date())
            .setExpiration(Date.from(ZonedDateTime.now(ZoneOffset.UTC).plusMinutes(30L).toInstant()))
            .signWith(SignatureAlgorithm.HS256, ServerConfiguration.getJWTSigningKey())
            .compact();
        return Response.ok().header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken).build();
    }

    @GET
    @Secured
    public Response isAuthenticated() {
        return Response.ok().build();
    }
}
