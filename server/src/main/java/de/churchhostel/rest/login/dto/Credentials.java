package de.churchhostel.rest.login.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class Credentials {

    @NotNull
    private String username;
    @NotNull
    private String password;

    public Credentials() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
