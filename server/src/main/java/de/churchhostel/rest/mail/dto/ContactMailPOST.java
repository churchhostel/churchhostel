package de.churchhostel.rest.mail.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author Simon Arnold <simonh.arnold@gmail.com>
 */
public class ContactMailPOST {

    @NotNull
    private String contactName;
    @NotNull
    @Pattern(regexp = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", flags = { Pattern.Flag.CASE_INSENSITIVE }, message = "Invalid email format")
    private String email;
    @NotNull
    private String message;

    // Optional fields
    private String communityName;
    private String communityAddress;
    @Pattern(regexp = "^(\\d[ /-]?)+\\d$", message = "Invalid phone number")
    private String phoneNumber;

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public String getCommunityAddress() {
        return communityAddress;
    }

    public void setCommunityAddress(String communityAddress) {
        this.communityAddress = communityAddress;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
