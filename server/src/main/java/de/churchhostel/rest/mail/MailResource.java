package de.churchhostel.rest.mail;

import de.churchhostel.rest.mail.dto.ContactMailPOST;
import de.churchhostel.service.exception.InvalidDataException;
import de.churchhostel.service.mail.ContactMailMessageFormatter;
import de.churchhostel.service.mail.ContactMailService;
import de.churchhostel.util.mail.Mailer;
import de.churchhostel.util.mail.MailerException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST entry point for sending mails.
 */
@Path("/mail/contact")
public class MailResource {

	private ContactMailService contactMailService;

	public MailResource() {
		contactMailService = new ContactMailService(new Mailer(), new ContactMailMessageFormatter());
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response sendContactMail(ContactMailPOST postDTO) throws MailerException {
		try {
			contactMailService.sendContactMail(postDTO);
		} catch (InvalidDataException e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
		}
		return Response.ok().build();
	}
}
