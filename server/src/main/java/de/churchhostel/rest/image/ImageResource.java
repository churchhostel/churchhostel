package de.churchhostel.rest.image;

import de.churchhostel.model.Image;
import de.churchhostel.repository.ChurchhostelRepository;
import de.churchhostel.repository.ImageRepository;
import de.churchhostel.rest.image.dto.CreateImage;
import de.churchhostel.rest.jwt.Secured;
import de.churchhostel.service.ImageService;
import de.churchhostel.service.TransactionManagedImageService;
import de.churchhostel.service.exception.HostelNotFoundException;
import de.churchhostel.service.exception.ImageNotFoundException;
import de.churchhostel.service.exception.InvalidDataException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST entry point for the image resource.
 * Provides Methods for accessing images.
 */
@Path("/image")
public class ImageResource {

	private ImageService imageService;

	public ImageResource() {
		imageService = new TransactionManagedImageService(new ChurchhostelRepository(), new ImageRepository());
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
    @Secured
	public Response createImage(@Valid @NotNull CreateImage createData) throws InvalidDataException, HostelNotFoundException {
        Image image = imageService.addImage(createModel(createData));
		return Response.status(Response.Status.CREATED).entity(image).build();
	}

	@DELETE
	@Path("{id}")
    @Secured
	public Response deleteImage(@PathParam("id") long id) throws ImageNotFoundException {
        imageService.deleteImage(id);
		return Response.ok().build();
	}


	private Image createModel(CreateImage c) {
        Image i = new Image(c.getFileUrl(), c.getChurchhostelId());
        return i;
    }
}
