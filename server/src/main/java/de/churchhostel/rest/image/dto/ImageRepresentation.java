package de.churchhostel.rest.image.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class ImageRepresentation {

    private Long id;
    private String fileUrl;
    private Long churchhostelId;

    public ImageRepresentation(Long id, String fileUrl, Long churchhostelId) {
        this.id = id;
        this.fileUrl = fileUrl;
        this.churchhostelId = churchhostelId;
    }

    public Long getId() {
        return id;
    }
    public String getFileUrl() {
        return fileUrl;
    }
    public Long getChurchhostelId() {
        return churchhostelId;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }
    public void setChurchhostelId(Long churchhostelId) {
        this.churchhostelId = churchhostelId;
    }
}
