package de.churchhostel.rest.image.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class CreateImage {

	@NotNull
	@Pattern(regexp = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]", message = "Invalid file URL number")
	private String fileUrl;
	@NotNull
	private Long churchhostelId;

	public CreateImage() {
	}

	public CreateImage(String fileUrl, Long churchhostelId) {
		this.fileUrl = fileUrl;
		this.churchhostelId = churchhostelId;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	public Long getChurchhostelId() {
		return churchhostelId;
	}

	public void setChurchhostelId(Long churchhostelId) {
		this.churchhostelId = churchhostelId;
	}

}
