package de.churchhostel.rest.exceptions;

import de.churchhostel.service.exception.InvalidDataException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author Simon Arnold <simonh.arnold@gmail.com>
 */
@Provider
public class InvalidDataMapper implements ExceptionMapper<InvalidDataException> {

    @Override
    public Response toResponse(InvalidDataException exception) {
        return Response.status(Response.Status.BAD_REQUEST)
            .entity(ErrorMessages.createJsonError(ErrorMessages.INVALID_DATA, Response.Status.BAD_REQUEST.getStatusCode()))
        .build();

    }
}
