package de.churchhostel.rest.exceptions;

import javax.persistence.EntityNotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.io.FileNotFoundException;

/**
 * @author Simon Arnold <simonh.arnold@gmail.com>
 */
@Provider
public class FileNotFoundMapper implements ExceptionMapper<FileNotFoundException> {

    @Override
    public Response toResponse(FileNotFoundException exception) {
        return Response.status(Response.Status.NOT_FOUND).entity(
            ErrorMessages.createJsonError(ErrorMessages.FILE_NOT_FOUND, Response.Status.NOT_FOUND.getStatusCode())
        ).build();
    }
}
