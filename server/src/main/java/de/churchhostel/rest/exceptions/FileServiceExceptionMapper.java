package de.churchhostel.rest.exceptions;

import de.churchhostel.service.exception.FileServiceException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author Simon Arnold <simonh.arnold@gmail.com>
 */
@Provider
public class FileServiceExceptionMapper implements ExceptionMapper<FileServiceException> {

    @Override
    public Response toResponse(FileServiceException exception) {
        return Response.status(Response.Status.CONFLICT).build();
    }
}
