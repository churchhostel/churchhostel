package de.churchhostel.rest.exceptions;

import javax.persistence.EntityNotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author Simon Arnold <simonh.arnold@gmail.com>
 */
@Provider
public class EntityNotFoundMapper implements ExceptionMapper<EntityNotFoundException> {

    @Override
    public Response toResponse(EntityNotFoundException exception) {
        return Response.status(Response.Status.NOT_FOUND).entity(
            ErrorMessages.createJsonError(ErrorMessages.NOT_FOUND, Response.Status.NOT_FOUND.getStatusCode())
        ).build();
    }
}
