package de.churchhostel.rest.exceptions;

import javax.json.Json;

/**
 * @author Simon Arnold <simonh.arnold@gmail.com>
 */
public class ErrorMessages {

    public static final String FILE_NOT_FOUND = "Entity not found" ;
    public static final String NOT_FOUND = "Entity not found" ;
    public static final String INVALID_DATA = "Invalid data";

    public static final String createJsonError(String message, int statusCode) {
        return Json.createObjectBuilder()
            .add("error", Json.createObjectBuilder()
                .add("code", statusCode)
                .add("message", message))
            .build().toString();
    }


}
