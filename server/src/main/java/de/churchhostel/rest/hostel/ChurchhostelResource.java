package de.churchhostel.rest.hostel;

import de.churchhostel.ServerConfiguration;
import de.churchhostel.model.Churchhostel;
import de.churchhostel.model.Contact;
import de.churchhostel.model.Image;
import de.churchhostel.repository.ChurchhostelRepository;
import de.churchhostel.repository.ContactRepository;
import de.churchhostel.repository.ImageRepository;
import de.churchhostel.rest.hostel.dto.ChurchhostelRepresentation;
import de.churchhostel.rest.hostel.dto.CreateHostel;
import de.churchhostel.rest.hostel.dto.UpdateHostel;
import de.churchhostel.rest.image.dto.ImageRepresentation;
import de.churchhostel.rest.jwt.Secured;
import de.churchhostel.service.*;
import de.churchhostel.service.exception.HostelNotFoundException;
import de.churchhostel.service.exception.InvalidDataException;
import io.jenetics.jpx.GPX;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static de.churchhostel.util.Utils.nullOrEmpty;

/**
 * REST entry point for the Churchhostel Resource. Provides Methods for
 * accessing Churchhostels.
 */
@Path("/hostel")
public class ChurchhostelResource {

    @Context
    private UriInfo uriInfo;

    private static final Logger log = LogManager.getLogger(ChurchhostelResource.class);

    private ChurchhostelService churchhostelService;
    private ContactService contactService;
    private ImageService imageService;

    public ChurchhostelResource() {
        ChurchhostelRepository chRepo = new ChurchhostelRepository();
        churchhostelService = new TransactionManagedChurchhostelService(chRepo);
        contactService = new TransactionManagedContactService(chRepo, new ContactRepository());
        imageService = new TransactionManagedImageService(chRepo, new ImageRepository());
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOne(@PathParam("id") long id) throws HostelNotFoundException {
        Churchhostel entity = churchhostelService.getHostel(id);
        ChurchhostelRepresentation representation = createRepresentation(entity);
        representation.setContacts(contactService.getHostelContacts(representation.getId()).stream().map(Contact::getId)
                .collect(Collectors.toSet()));
        representation.setImages(imageService.getHostelImages(representation.getId()).stream().map(image -> {
            return new ImageRepresentation(image.getId(), image.getFileUrl(), entity.getId());
        }).collect(Collectors.toSet()));
        return Response.status(Response.Status.OK).entity(representation).build();
    }

    @GET
    @Path("{id}/gpx")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGpxFile(@PathParam("id") long id) throws HostelNotFoundException {
        Churchhostel hostel = churchhostelService.getHostel(id);

        String name = "Unterkunft Gemeinde " + hostel.getName();
        String description = String.format("Adresse: %s\n\nHinweise: %s", hostel.getAddress(), String.join("\n", hostel.getAdditionalHints()));
        String link = ServerConfiguration.PAGE_URL + "/hostel/" + hostel.getId();
        GPX gpxWithHostel = GPX.builder("churchhostel.de")
                .addWayPoint(wp -> wp.name(name)
                    .time(Instant.now())
                    .lat(hostel.getLatitude())
                    .lon(hostel.getLongitude())
                    .sym("residence")
                    .type("Accomodation")
                    .addLink(link)
                    .desc(description))
                .build();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            GPX.write(gpxWithHostel, out);
        } catch (IOException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        String filename = "Unterkunft " + hostel.getName() + ".gpx";
        return Response.ok(out.toString(), "application/gpx+xml")
                .header("Content-Disposition", "attachment; filename=\"" + filename + "\"").build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        List<ChurchhostelRepresentation> hostels = churchhostelService.getAllHostels().stream()
                .map(this::createRepresentation).collect(Collectors.toList());
        for (ChurchhostelRepresentation representation : hostels) {
            representation.setContacts(contactService.getHostelContacts(representation.getId()).stream()
                    .map(Contact::getId).collect(Collectors.toSet()));
            representation.setImages(imageService.getHostelImages(representation.getId()).stream().map(image -> {
                return new ImageRepresentation(image.getId(), image.getFileUrl(), representation.getId());
            }).collect(Collectors.toSet()));
        }
        return Response.status(Response.Status.OK).entity(hostels).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Secured
    public Response createChurchhostel(@Valid @NotNull CreateHostel hostelDTO) throws InvalidDataException {
        Churchhostel hostelModel = createModel(hostelDTO);
        hostelModel = churchhostelService.createHostel(hostelModel);
        ChurchhostelRepresentation created = createRepresentation(hostelModel);
        // Freshly created hostels can't have any contacts or images yet, so the
        // representation returned should have an empty set for both
        created.setContacts(new HashSet<>());
        created.setImages(new HashSet<>());
        log.info("Created new hostel " + created.getId());
        return Response.status(Response.Status.CREATED).entity(created).build();
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Secured
    public Response updateChurchhostel(@PathParam("id") long id, @Valid @NotNull UpdateHostel updateData)
            throws InvalidDataException {
        Churchhostel model = createModel(id, updateData);
        churchhostelService.updateHostel(id, model);
        return Response.ok().build();
    }

    @DELETE
    @Path("{id}")
    @Secured
    public Response delete(@PathParam("id") long id) throws HostelNotFoundException {
        churchhostelService.deleteHostel(id);
        return Response.ok().build();
    }

    private Churchhostel createModel(CreateHostel dto) {
        Churchhostel hostel = new Churchhostel(dto.getLatitude(), dto.getLongitude(), dto.getName(), dto.getAddress(),
                dto.getBeds(), dto.getEmail(), dto.getFacilities(), dto.getServices(), dto.getAdditionalHints(),
                dto.getPrice());
        return hostel;
    }

    private Churchhostel createModel(long id, UpdateHostel updateHostelData) {
        Churchhostel model = new Churchhostel();
        model.setId(id);
        if (!nullOrEmpty(updateHostelData.getName())) {
            model.setName(updateHostelData.getName());
        }
        if (updateHostelData.getBeds() != null) {
            model.setBeds(updateHostelData.getBeds());
        }
        if (!nullOrEmpty(updateHostelData.getAddress())) {
            model.setAddress(updateHostelData.getAddress());
        }
        if (!nullOrEmpty(updateHostelData.getEmail())) {
            model.setEmail(updateHostelData.getEmail());
        }
        if (updateHostelData.getLatitude() != null) {
            model.setLatitude(updateHostelData.getLatitude());
        }
        if (updateHostelData.getLongitude() != null) {
            model.setLongitude(updateHostelData.getLongitude());
        }
        if (updateHostelData.getPrice() != null) {
            model.setPrice(updateHostelData.getPrice());
        }
        if (!nullOrEmpty(updateHostelData.getServices())) {
            model.setServices(updateHostelData.getServices());
        }
        if (!nullOrEmpty(updateHostelData.getFacilities())) {
            model.setFacilities(updateHostelData.getFacilities());
        }
        if (!nullOrEmpty(updateHostelData.getAdditionalHints())) {
            model.setAdditionalHints(updateHostelData.getAdditionalHints());
        }
        return model;
    }

    private ChurchhostelRepresentation createRepresentation(Churchhostel ch) {
        ChurchhostelRepresentation rep = new ChurchhostelRepresentation();
        rep.setId(ch.getId());
        rep.setLatitude(ch.getLatitude());
        rep.setLongitude(ch.getLongitude());
        rep.setName(ch.getName());
        rep.setBeds(ch.getBeds());
        rep.setEmail(ch.getEmail());
        rep.setAddress(ch.getAddress());
        rep.setServices(ch.getServices());
        rep.setFacilities(ch.getFacilities());
        rep.setAdditionalHints(ch.getAdditionalHints());
        rep.setPrice(ch.getPrice());
        return rep;
    }

}
