package de.churchhostel.rest.hostel.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class CreateHostel {

	@NotNull
	private Double latitude;
	@NotNull
	private Double longitude;
	@NotNull
	private String name;
	@NotNull
	private String address;
	@NotNull
	private Integer beds;
	@Pattern(regexp = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", flags = { Pattern.Flag.CASE_INSENSITIVE }, message = "Invalid email format")
	private String email;
	private List<String> facilities;
	private List<String> services;
	private List<String> additionalHints;
	@NotNull
	private BigDecimal price;

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getBeds() {
		return beds;
	}

	public void setBeds(Integer beds) {
		this.beds = beds;
	}

	public List<String> getFacilities() {
		return facilities;
	}

	public void setFacilities(List<String> facilities) {
		this.facilities = facilities;
	}

	public List<String> getServices() {
		return services;
	}

	public void setServices(List<String> services) {
		this.services = services;
	}

	public List<String> getAdditionalHints() {
		return additionalHints;
	}

	public void setAdditionalHints(List<String> additionalHints) {
		this.additionalHints = additionalHints;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public static class Builder {
		private CreateHostel target = new CreateHostel();

		public Builder(Double latitude, Double longitude, String name, String address, Integer beds, BigDecimal price) {
			target.latitude = latitude;
			target.longitude = longitude;
			target.name = name;
			target.address = address;
			target.beds = beds;
			target.price = price;
			target.facilities = new ArrayList<>();
			target.services = new ArrayList<>();
			target.additionalHints = new ArrayList<>();
		}

		public Builder withEmail(String email) {
			target.email = email;
			return this;
		}

		public Builder withFacility(String facility) {
			target.getFacilities().add(facility);
			return this;
		}

		public Builder withService(String service) {
			target.getServices().add(service);
			return this;
		}

		public Builder withAdditionalHint(String additionalHint) {
			target.getAdditionalHints().add(additionalHint);
			return this;
		}

		public CreateHostel build() {
			return target;
		}
	}
}
