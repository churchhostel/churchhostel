package de.churchhostel.rest.hostel.dto;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import de.churchhostel.rest.image.dto.ImageRepresentation;

public class ChurchhostelRepresentation {

    private Long id;
    private Double latitude;
    private Double longitude;
    private String name;
    private String address;
    private Integer beds;
    private String email;
    private List<String> facilities;
    private List<String> services;
    private List<String> additionalHints;
    private BigDecimal price;
    private Set<ImageRepresentation> images;
    private Set<Long> contacts;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getBeds() {
        return beds;
    }

    public void setBeds(Integer beds) {
        this.beds = beds;
    }

    public List<String> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<String> facilities) {
        this.facilities = facilities;
    }

    public List<String> getServices() {
        return services;
    }

    public void setServices(List<String> services) {
        this.services = services;
    }

    public List<String> getAdditionalHints() {
        return additionalHints;
    }

    public void setAdditionalHints(List<String> additionalHints) {
        this.additionalHints = additionalHints;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Set<ImageRepresentation> getImages() {
        return images;
    }

    public void setImages(Set<ImageRepresentation> images) {
        this.images = images;
    }

    public Set<Long> getContacts() {
        return contacts;
    }

    public void setContacts(Set<Long> contacts) {
        this.contacts = contacts;
    }
}
