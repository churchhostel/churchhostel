package de.churchhostel.rest.hostel.dto;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.constraints.Pattern;

public class UpdateHostel {

    private Double latitude;
    private Double longitude;
    private String name;
    private String address;
    private Integer beds;
    @Pattern(regexp = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", flags = { Pattern.Flag.CASE_INSENSITIVE }, message = "Invalid email format")
    private String email;
    private List<String> facilities;
    private List<String> services;
    private List<String> additionalHints;
    private BigDecimal price;

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getBeds() {
        return beds;
    }

    public void setBeds(Integer beds) {
        this.beds = beds;
    }

    public List<String> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<String> facilities) {
        this.facilities = facilities;
    }

    public List<String> getServices() {
        return services;
    }

    public void setServices(List<String> services) {
        this.services = services;
    }

    public List<String> getAdditionalHints() {
        return additionalHints;
    }

    public void setAdditionalHints(List<String> additionalHints) {
        this.additionalHints = additionalHints;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
