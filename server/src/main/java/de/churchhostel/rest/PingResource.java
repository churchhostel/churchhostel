package de.churchhostel.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;

@Path("/ping")
public class PingResource {

	private static final Logger log = LogManager.getLogger(PingResource.class);

	@Context
	UriInfo uriInfo;
	@Context
	Request request;

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response get() {
		log.info("Ping received");
		return Response.status(Status.OK).entity("Pong").build();
	}
}
