package de.churchhostel.rest.contact.dto;

import javax.validation.constraints.Pattern;

public class UpdateContact {

    private Long id;
    private String name;
    @Pattern(regexp = "^\\+?[\\d ]+$", message = "Invalid phone number")
    private String phone;
    @Pattern(regexp = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", flags = { Pattern.Flag.CASE_INSENSITIVE }, message = "Invalid email format")
    private String email;
    private Long churchhostelId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setName(String name) {
		this.name = name;
    }

    public void setEmail(String email) {
		this.email = email;
	}

    public String getEmail() {
        return email;
	}

	public Long getChurchhostelId() {
		return churchhostelId;
	}

	public void setChurchhostelId(Long churchhostelId) {
		this.churchhostelId = churchhostelId;
	}

}
