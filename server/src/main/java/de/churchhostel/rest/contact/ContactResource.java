package de.churchhostel.rest.contact;

import de.churchhostel.model.Contact;
import de.churchhostel.repository.ChurchhostelRepository;
import de.churchhostel.repository.ContactRepository;
import de.churchhostel.rest.contact.dto.CreateContact;
import de.churchhostel.rest.contact.dto.UpdateContact;
import de.churchhostel.rest.jwt.Secured;
import de.churchhostel.service.ContactService;
import de.churchhostel.service.TransactionManagedContactService;
import de.churchhostel.service.exception.ContactNotFoundException;
import de.churchhostel.service.exception.InvalidDataException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import static de.churchhostel.util.Utils.nullOrEmpty;

/**
 * REST entry point for the Contact Resource.
 * Provides Methods for accessing Contacts.
 */
@Path("/contact")
public class ContactResource {

	private ContactService contactService;

	@Context
	UriInfo uriInfo;
	
	public ContactResource() {
		contactService = new TransactionManagedContactService(new ChurchhostelRepository(), new ContactRepository());
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOne(@PathParam("id") long id) throws ContactNotFoundException {
        return Response.status(Status.OK).entity(contactService.getContact(id)).build();
	}

	@POST
    @Secured
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public Response createContact(@Valid @NotNull CreateContact createContactRequest) throws InvalidDataException {
        Contact createdContact = contactService.addContact(createContactModel(createContactRequest));
        return Response.status(Status.CREATED).entity(createdContact).build();
	}

	@PUT
    @Secured
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateContact(@PathParam("id") long id, @Valid @NotNull UpdateContact updatedContactRequest) throws InvalidDataException {
        contactService.updateContact(id, createContactModel(id, updatedContactRequest));
		return Response.ok().build();
	}

	@DELETE
    @Secured
	@Path("{id}")
	public Response delete(@PathParam("id") long id) {
        contactService.deleteContact(id);
		return Response.ok().build();
	}

    private Contact createContactModel(CreateContact createContactRequest) {
        Contact modelObject = new Contact(createContactRequest.getName(), createContactRequest.getPhone(), createContactRequest.getEmail());
        modelObject.setChurchhostelId(createContactRequest.getChurchhostelId());
        return modelObject;
    }

    private Contact createContactModel(long id, UpdateContact updateContactData) {
	    Contact model = new Contact();
	    model.setId(id);
        if (!nullOrEmpty(updateContactData.getName())) {
            model.setName(updateContactData.getName());
        }
        if (!nullOrEmpty(updateContactData.getPhone())) {
            model.setPhone(updateContactData.getPhone());
		}
		model.setEmail(updateContactData.getEmail());
        if (!nullOrEmpty(updateContactData.getChurchhostelId())) {
            model.setChurchhostelId(updateContactData.getChurchhostelId());
        }
        return model;
    }
}

