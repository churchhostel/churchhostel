package de.churchhostel.rest.contact.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class CreateContact {

	@NotNull
	private String name;
	@NotNull
	@Pattern(regexp = "^\\+?[\\d ]+$", message = "Invalid phone number")
	private String phone;
	@Pattern(regexp = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", flags = { Pattern.Flag.CASE_INSENSITIVE }, message = "Invalid email format")
	private String email;
	@NotNull
	private Long churchhostelId;

	public CreateContact() {
	}

	public String getName() {
		return name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public Long getChurchhostelId() {
		return churchhostelId;
	}

	public void setChurchhostelId(Long churchhostelId) {
		this.churchhostelId = churchhostelId;
	}

	public static class Builder {
		private CreateContact target = new CreateContact();

		public Builder(String name, String phone, Long churchhostelId) {
			target.name = name;
			target.phone = phone;
			target.churchhostelId = churchhostelId;
		}

		public Builder withEmail(String email) {
			target.email = email;
			return this;
		}

		public CreateContact build() {
			return target;
		}

	}

}
