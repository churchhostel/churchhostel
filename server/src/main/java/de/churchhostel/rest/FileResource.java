package de.churchhostel.rest;

import javax.json.Json;
import javax.servlet.annotation.MultipartConfig;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.churchhostel.service.ImageFileService;
import de.churchhostel.service.SimpleFileService;
import de.churchhostel.service.ThumbnailService;
import de.churchhostel.service.TrailFileService;
import de.churchhostel.service.exception.FileServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.churchhostel.service.FileService;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * REST entry point for files. Provides Methods for accessing files.
 */
@Path("/files")
@MultipartConfig
public class FileResource {

    private static final Logger log = LogManager.getLogger(FileResource.class);

    private FileService trailFileService = new TrailFileService();
    private FileService imageFileService = new ImageFileService();
    private ThumbnailService thumbnailFileService = new ThumbnailService(imageFileService);

    @POST
    @Path("/image")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postImage(@DefaultValue("true") @FormDataParam("enabled") boolean enabled,
            @FormDataParam("file") InputStream fileContent,
            @FormDataParam("file") FormDataContentDisposition fileDetail) throws FileServiceException {
        log.info("Received request to persist image as " + fileDetail.getFileName());
        String actualFileName;
        actualFileName = imageFileService.saveFile(fileDetail.getFileName(), fileContent);

        String responseBody = Json.createObjectBuilder().add("fileName", actualFileName).build().toString();
        return Response.status(Response.Status.CREATED).entity(responseBody).build();
    }

    @POST
    @Path("/trail")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postTrail(@DefaultValue("true") @FormDataParam("enabled") boolean enabled,
            @FormDataParam("file") InputStream fileContent,
            @FormDataParam("file") FormDataContentDisposition fileDetail) throws FileServiceException {
        log.info("Received request to persist trail as " + fileDetail.getFileName());
        String actualFileName;
        actualFileName = trailFileService.saveFile(fileDetail.getFileName(), fileContent);

        String responseBody = Json.createObjectBuilder().add("fileName", actualFileName).build().toString();
        return Response.status(Response.Status.CREATED).entity(responseBody).build();
    }

    @GET
    @Path("/trail/{name}")
    @Produces("application/vnd.google-earth.kml+xml")
    public Response getTrail(@PathParam("name") String filename) throws FileNotFoundException {
        File retrievedFile = trailFileService.getFile(filename);
        return Response.status(Response.Status.OK)
                .header("Content-Disposition", "attachment; filename=\"" + filename + "\"").entity(retrievedFile)
                .build();
    }

    @GET
    @Path("/image/{name}")
    @Produces({ "image/jpeg", "image/png" })
    public Response getImage(@PathParam("name") String filename, @QueryParam("thumb") boolean isThumbnail)
            throws Exception {
        File retrievedFile = isThumbnail ? thumbnailFileService.getThumbnail(filename)
                : thumbnailFileService.getMediumQualityImage(filename);
        return Response.status(Response.Status.OK)
                .header("Content-Disposition", "attachment; filename=\"" + retrievedFile.getName() + "\"")
                .entity(retrievedFile).build();
    }

    @DELETE
    @Path("/image/{name}")
    public Response deleteImage(@PathParam("name") String filename) throws FileNotFoundException, FileServiceException {
        imageFileService.deleteFile(filename);
        return Response.ok().build();
    }

    @DELETE
    @Path("/trail/{name}")
    public Response deleteTrail(@PathParam("name") String filename) throws FileNotFoundException, FileServiceException {
        trailFileService.deleteFile(filename);
        return Response.ok().build();
    }
}
