package de.churchhostel.service;

import de.churchhostel.model.User;
import de.churchhostel.repository.UserRepository;
import de.churchhostel.util.Encryption;
import de.churchhostel.util.EncryptionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

public class AuthenticationService {

    private final static Logger log = LogManager.getLogger(AuthenticationService.class);

    private UserRepository userRepository;

    public AuthenticationService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean isAuthenticated(String username, String password) {
        Optional<User> user = userRepository.findByName(username);
        try {
            return user.isPresent() && user.get().getPwHash().equals(Encryption.sha256base64(password));
        } catch (EncryptionException e) {
            log.error("Error generating password hash when authenticating user", e);
            return false;
        }
    }
}
