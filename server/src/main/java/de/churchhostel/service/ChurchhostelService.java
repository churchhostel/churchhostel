package de.churchhostel.service;

import de.churchhostel.model.Churchhostel;
import de.churchhostel.service.exception.HostelNotFoundException;
import de.churchhostel.service.exception.InvalidDataException;

import java.util.List;

/**
 * Service used to access, modify or save Churchhostel objects in the system.
 */
public interface ChurchhostelService {

	/**
	 * Returns the hostel with id as an object that can be serialized into a response.
	 * @param id ID of the hostel to be retrieved.
	 * @return Serializable hostel object.
	 * @throws HostelNotFoundException If no hostel with this id could be found.
	 */
	Churchhostel getHostel(long id) throws HostelNotFoundException;
	
	/**
	 * Retrieves all hostels currently in the system.
	 * @return A list of serializable hostel objects.
	 */
	List<Churchhostel> getAllHostels();
	
	/**
	 * Creates a new hostel from the given information
	 * @throws InvalidDataException
	 */
	Churchhostel createHostel(Churchhostel newHostel) throws InvalidDataException;
	/**
	 * Updates the hostel with the given id and information. 
	 * @param id id of the hostel that should be updated.
	 * @throws InvalidDataException If one or more fields of the updated information are invalid.
	 */
	void updateHostel(long id, Churchhostel updatedHostel) throws HostelNotFoundException, InvalidDataException;
	
	/**
	 * Deletes a hostel.
	 * @param id Hostel that should be deleted.
	 * @throws HostelNotFoundException If no such hostel could be found.
	 */
	void deleteHostel(long id) throws HostelNotFoundException;
}
