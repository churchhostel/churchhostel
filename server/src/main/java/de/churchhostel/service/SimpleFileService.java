package de.churchhostel.service;

import de.churchhostel.ServerConfiguration;
import de.churchhostel.service.exception.FileServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;

public abstract class SimpleFileService implements FileService {

	private static final Logger log = LogManager.getLogger(SimpleFileService.class);

	private final Path trailDirectory;

	protected abstract String getSubDir();

	public SimpleFileService() {
	    this(ServerConfiguration.getFilesDir());
	}

    public SimpleFileService(Path filesDir) {
        trailDirectory = filesDir.resolve(getSubDir());
        if (!Files.exists(trailDirectory)) {
            try {
                Files.createDirectory(trailDirectory);
            } catch (IOException e) {
                log.error("Could not create trail files directory " + trailDirectory.toString());
            }
        }
    }

	@Override
	public String saveFile(String preferredFileName, InputStream fileContent) throws FileServiceException {
	    String validFileName = findValidFilename(preferredFileName);
		Path destinationFile = trailDirectory.resolve(validFileName);

		try {
			Files.createDirectories(destinationFile.getParent());
			Files.copy(fileContent, destinationFile, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			log.error("Failed to save input", e);
			throw new FileServiceException(e);
		}
		return validFileName;
	}

	@Override
	public File getFile(String name) throws FileNotFoundException {
		File file = trailDirectory.resolve(name).toFile();
		if (! file.exists()) {
			throw new FileNotFoundException("File " + name + " could not be found in " + trailDirectory);
		}
		return file;
	}

	@Override
	public void deleteFile(String name) throws FileServiceException {
		try {
			Files.delete(trailDirectory.resolve(name));
		}
		catch (NoSuchFileException e) {
			log.info("Trying to delete nonexisting file " + name + " in " + trailDirectory);
		}
		catch (IOException e) {
			log.info("Error trying to delete file " + name + " at " + trailDirectory);
			throw new FileServiceException(e);
		}
	}

	private String findValidFilename(String name) throws FileServiceException {
	    String validName = name;
		int prefixCounter = 1;
		while (Files.exists(trailDirectory.resolve(validName)) && prefixCounter <= 100) {
			validName = String.valueOf(prefixCounter) + name;
			prefixCounter += 1;
		}

		if (prefixCounter > 100) {
			throw new FileServiceException("Could not find a valid filename for " + name + " in " + trailDirectory + " after 100 attempts.");
		}

		return validName;
	}
	
}
