package de.churchhostel.service;

import java.nio.file.Path;

public class ImageFileService extends SimpleFileService {

    public ImageFileService(Path filesDir) {
        super(filesDir);
    }

    public ImageFileService() {
        super();
    }

    @Override
    protected String getSubDir() {
        return "images/";
    }
}
