package de.churchhostel.service;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Path;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;

import org.apache.commons.io.FilenameUtils;

import de.churchhostel.ServerConfiguration;

public class ThumbnailService {

    private FileService imageFileService;

    public ThumbnailService(FileService imageFileService) {
        this.imageFileService = imageFileService;
    }

    public File getThumbnail(String originalImageName) throws Exception {
        final String thumbName = FilenameUtils.getBaseName(originalImageName) + "_thumb."
                + FilenameUtils.getExtension(originalImageName);
        try {
            return imageFileService.getFile(thumbName);
        } catch (FileNotFoundException e) {
            // Attempt to create the thumbnail
            File existingFile = imageFileService.getFile(originalImageName);
            createThumbnail(existingFile, thumbName);
        }
        return imageFileService.getFile(thumbName);
    }

    public File getMediumQualityImage(String originalImageName) throws Exception {
        final String thumbName = FilenameUtils.getBaseName(originalImageName) + "_mq."
                + FilenameUtils.getExtension(originalImageName);
        try {
            return imageFileService.getFile(thumbName);
        } catch (FileNotFoundException e) {
            // Attempt to create the thumbnail
            File existingFile = imageFileService.getFile(originalImageName);
            createMediumQualityImage(existingFile, thumbName);
        }
        return imageFileService.getFile(thumbName);
    }

    private void createMediumQualityImage(File f, String thumbnailFilename) throws Exception {
        createResizedImage(f, thumbnailFilename, ServerConfiguration.getMediumQualityImageSize(), 0.9f);
    }

    private void createThumbnail(File f, String thumbnailFilename) throws Exception {
        createResizedImage(f, thumbnailFilename, ServerConfiguration.getThumbnailSize(), 0.7f);
    }

    private void createResizedImage(File f, String thumbnailFilename, int maxImageSize, float quality)
            throws Exception {
        BufferedImage sourceImage = ImageIO.read(f);
        float width = sourceImage.getWidth();
        float height = sourceImage.getHeight();

        // Just copy the image if the longer side is below the wanted image size
        BufferedImage img2;
        if ((width > height && width < maxImageSize) || (width < height && height < maxImageSize)
                || (width == height && width < maxImageSize)) {
            img2 = new BufferedImage(sourceImage.getWidth(), sourceImage.getHeight(), sourceImage.getType());
            Graphics g = img2.createGraphics();
            g.drawImage(sourceImage, 0, 0, null);
            g.dispose();
        } else if (width > height) {
            float scaledWidth = (width / height) * (float) maxImageSize;
            float scaledHeight = maxImageSize;

            BufferedImage img = new BufferedImage((int) scaledWidth, (int) scaledHeight, sourceImage.getType());
            Image scaledImage = sourceImage.getScaledInstance((int) scaledWidth, (int) scaledHeight,
                    Image.SCALE_SMOOTH);
            img.createGraphics().drawImage(scaledImage, 0, 0, null);

            int offset = (int) ((scaledWidth - scaledHeight) / 2f);
            img2 = img.getSubimage(offset, 0, maxImageSize, maxImageSize);
        } else if (width < height) {
            float scaledWidth = maxImageSize;
            float scaledHeight = (height / width) * (float) maxImageSize;

            BufferedImage img = new BufferedImage((int) scaledWidth, (int) scaledHeight, sourceImage.getType());
            Image scaledImage = sourceImage.getScaledInstance((int) scaledWidth, (int) scaledHeight,
                    Image.SCALE_SMOOTH);
            img.createGraphics().drawImage(scaledImage, 0, 0, null);

            int offset = (int) ((scaledHeight - scaledWidth) / 2f);
            img2 = img.getSubimage(0, offset, maxImageSize, maxImageSize);
        } else {
            img2 = new BufferedImage(maxImageSize, maxImageSize, sourceImage.getType());
            Image scaledImage = sourceImage.getScaledInstance(maxImageSize, maxImageSize, Image.SCALE_SMOOTH);
            img2.createGraphics().drawImage(scaledImage, 0, 0, null);
        }
        String fileExtension = FilenameUtils.getExtension(f.getName());
        ImageWriter imageWriter = ImageIO.getImageWritersByFormatName(fileExtension).next();
        ImageWriteParam iwp = imageWriter.getDefaultWriteParam();
        iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        iwp.setCompressionQuality(quality);

        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        ImageOutputStream imageOutStream = ImageIO.createImageOutputStream(outStream);
        imageWriter.setOutput(imageOutStream);
        IIOImage image = new IIOImage(img2, null, null);
        imageWriter.write(null, image, iwp);

        ByteArrayInputStream inStream = new ByteArrayInputStream(outStream.toByteArray());
        imageFileService.saveFile(thumbnailFilename, inStream);
    }
}
