package de.churchhostel.service;

import static de.churchhostel.util.Utils.nullOrEmpty;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.churchhostel.model.Churchhostel;
import de.churchhostel.repository.ChurchhostelRepository;
import de.churchhostel.service.exception.HostelNotFoundException;
import de.churchhostel.service.exception.InvalidDataException;

public class TransactionManagedChurchhostelService implements ChurchhostelService {

	private static final Logger log = LogManager.getLogger(TransactionManagedChurchhostelService.class);
	
	private ChurchhostelRepository chRepo;
	
	public TransactionManagedChurchhostelService(ChurchhostelRepository chRepo) {
		this.chRepo = chRepo;
	}

	@Override
	public Churchhostel getHostel(long id) throws HostelNotFoundException {
		Optional<Churchhostel> ch = chRepo.find(id);
		if (!ch.isPresent()) {
			log.warn("A hostel object was requested on the service layer, but could not be found.");
			throw new HostelNotFoundException();
		}
		return ch.get();
	}

	@Override
	public List<Churchhostel> getAllHostels() {
		return chRepo.findActive();
	}

	@Override
	public Churchhostel createHostel(Churchhostel newHostel) throws InvalidDataException {
		if (nullOrEmpty(newHostel.getName()) || nullOrEmpty(newHostel.getAddress()) || newHostel.getPrice() == null) {
			log.warn("Attempted creation of a churchhostel object with insufficient data: " + newHostel);
			throw new InvalidDataException("Insufficient data to add churchhostel: " + newHostel);
		}

		Churchhostel created = chRepo.create(newHostel);
		return created;
	}

	@Override
	public void updateHostel(long id, Churchhostel updatedHostel) throws HostelNotFoundException, InvalidDataException {
		Optional<Churchhostel> originalHostel = chRepo.find(id);
		if (!originalHostel.isPresent()) {
			log.error("Cannot update hostel, hostel with ID " + updatedHostel.getId() + " does not exists.");
			throw new HostelNotFoundException();
		}

		Churchhostel merged = originalHostel.get();
		
		if (updatedHostel.getLatitude() != null) {
			merged.setLatitude(updatedHostel.getLatitude());
		}
		if (updatedHostel.getLongitude() != null) {
			merged.setLongitude(updatedHostel.getLongitude());
		}
		if (!nullOrEmpty(updatedHostel.getName())) {
			merged.setName(updatedHostel.getName());
		}
		if (!nullOrEmpty(updatedHostel.getAddress())) {
			merged.setAddress(updatedHostel.getAddress());
		}
		if (!nullOrEmpty(updatedHostel.getEmail())) {
			merged.setEmail(updatedHostel.getEmail());
		}
		if (updatedHostel.getBeds() != null) {
			merged.setBeds(updatedHostel.getBeds());
		}
		if (!nullOrEmpty(updatedHostel.getFacilities())) {
			merged.setFacilities(updatedHostel.getFacilities());
		}
		if (!nullOrEmpty(updatedHostel.getServices())) {
			merged.setServices(updatedHostel.getServices());
		}
		if (!nullOrEmpty(updatedHostel.getAdditionalHints())) {
			merged.setAdditionalHints(updatedHostel.getAdditionalHints());
		}
		if (updatedHostel.getPrice() != null) {
			merged.setPrice(updatedHostel.getPrice());
		}
		
		chRepo.update(merged);
	}

	@Override
	public void deleteHostel(long id) throws HostelNotFoundException {
		if (!chRepo.exists(id)) {
			log.error("Deletion of Hostel with id " + id + " was requested but no hostel with that ID could be found.");
			throw new HostelNotFoundException();
		}
		
		chRepo.delete(id);
	}
	
}