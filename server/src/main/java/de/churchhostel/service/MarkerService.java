package de.churchhostel.service;


import de.churchhostel.model.Marker;

import java.util.List;

/**
 * Service used to access, modify or save Marker objects in the system.
 */
public interface MarkerService {

	/**
	 * Retrieves all markers in a serializable form that can be used in a response.
	 * @return List of markers in the system
	 */
	List<Marker> getAllMarker();
	
}