package de.churchhostel.service;

import de.churchhostel.model.Trail;
import de.churchhostel.service.exception.InvalidDataException;
import de.churchhostel.service.exception.TrailNotFoundException;

import java.util.List;

/**
 * Service used to access, modify or save Trail objects in the system.
 */
public interface TrailService {

	/**
	 * Retrieves a trail
	 * @param id Id of the trail object to be retrieved
	 * @return Serializable trail object
	 * @throws TrailNotFoundException if no such trail could be found
	 */
	Trail getTrail(long id) throws TrailNotFoundException;
	
	/**
	 * Retrieves a list of trails as serializable objects
	 * @return List of all available trails
	 */
	List<Trail> getAll();
	
	/**
	 * Adds a new trail
	 * @return the created trail object
	 * @throws InvalidDataException if one or more fields of the trail information were invalid
	 */
	Trail addTrail(Trail newTrail) throws InvalidDataException;
	
	/**
	 * Updates a trail
	 * @param id trail to be updated
	 * @throws TrailNotFoundException if no such trail could be found
	 * @throws InvalidDataException if one or more fields of the trail information were invalid
	 */
	void updateTrail(long id, Trail updatedTrail) throws TrailNotFoundException, InvalidDataException;
	
	/**
	 * Removes a trail
	 * @param id Trail to be removed
	 * @throws TrailNotFoundException If no such trail could be found.
	 */
	void deleteTrail(long id) throws TrailNotFoundException;
}
