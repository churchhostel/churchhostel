package de.churchhostel.service;

import de.churchhostel.model.Contact;
import de.churchhostel.repository.ChurchhostelRepository;
import de.churchhostel.repository.ContactRepository;
import de.churchhostel.service.exception.ContactNotFoundException;
import de.churchhostel.service.exception.HostelNotFoundException;
import de.churchhostel.service.exception.InvalidDataException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;
import static de.churchhostel.util.Utils.nullOrEmpty;

public class TransactionManagedContactService implements ContactService {

	private static final Logger log = LogManager.getLogger(TransactionManagedContactService.class);

	private ChurchhostelRepository chRepo;
	private ContactRepository contactRepo;
	
	public TransactionManagedContactService(ChurchhostelRepository chRepo, ContactRepository contactRepo) {
		this.chRepo = chRepo;
		this.contactRepo = contactRepo;
	}

	@Override
	public Contact getContact(long id) throws ContactNotFoundException {
	    Optional<Contact> findResult =  contactRepo.find(id);
	    if (!findResult.isPresent()) {
			log.error("Contact with id " + id + " requested but could not be found.");
			throw new ContactNotFoundException();
		}

		return findResult.get();
	}

    @Override
    public List<Contact> getHostelContacts(long id) {
        return contactRepo.findHostelContacts(id);
    }

    @Override
	public Contact addContact(Contact newContact) throws InvalidDataException, HostelNotFoundException {
		if (nullOrEmpty(newContact.getName()) || nullOrEmpty(newContact.getPhone())) {
			log.warn("Attempted creation of a contact object with insufficient data: " + newContact);
			throw new InvalidDataException("Insufficient data to add contact: " + newContact);
		}

		if (!chRepo.exists(newContact.getChurchhostelId())) {
			log.error("Creation of contact for churchhostel with id " + newContact.getChurchhostelId() + " requested, but the hostel could not be found.");
			throw new HostelNotFoundException("Hostel " + newContact.getChurchhostelId() + " could not be found.");
		}

		newContact = contactRepo.create(newContact);
		return newContact;
	}

	@Override
	public void updateContact(long id, Contact updatedContact) throws InvalidDataException, ContactNotFoundException, HostelNotFoundException {
		Optional<Contact> originalContact = contactRepo.find(id);
		if (!originalContact.isPresent()) {
			log.error("Update of contact with id " + id + " requested, but the contact could not be found.");
			throw new ContactNotFoundException();
		}

		Contact mergedContact = originalContact.get();
		if (!nullOrEmpty(updatedContact.getName())) {
			mergedContact.setName(updatedContact.getName());
		}
		if (!nullOrEmpty(updatedContact.getPhone())) {
			mergedContact.setPhone(updatedContact.getPhone());
		}
		mergedContact.setEmail(updatedContact.getEmail());

		// Update the associated churchhostel only if the churchhostelId attribute is set
		if (updatedContact.getChurchhostelId() != null) {
			if (!chRepo.exists(updatedContact.getChurchhostelId())) {
				log.error("A contact update tried to update hostel relation to a non existent hostel. " +
                    "Contact ID: " + updatedContact.getId() + " Hostel ID: " + updatedContact.getChurchhostelId());
				throw new HostelNotFoundException();
			}

			mergedContact.setChurchhostelId(updatedContact.getChurchhostelId());
		}

		contactRepo.update(mergedContact);
	}

	@Override
	public void deleteContact(long id) throws ContactNotFoundException {
		if (!contactRepo.exists(id)) {
			log.error("Contact delete requested, but contact " + id + " couldn't be found!");
			throw new ContactNotFoundException();
		}

		contactRepo.delete(id);
	}
	
	

}