package de.churchhostel.service.exception;

import java.io.IOException;

public class FileServiceException extends IOException {

	public FileServiceException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FileServiceException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public FileServiceException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public FileServiceException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	
}