package de.churchhostel.service.exception;

import javax.persistence.EntityNotFoundException;

public class HostelNotFoundException extends EntityNotFoundException {

	public HostelNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

    public HostelNotFoundException(String message) {
        super(message);
    }

}
