package de.churchhostel.service.exception;

import javax.persistence.EntityNotFoundException;

public class ContactNotFoundException extends EntityNotFoundException {

    public ContactNotFoundException() {
    }

    public ContactNotFoundException(String message) {
        super(message);
    }
}
