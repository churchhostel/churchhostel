package de.churchhostel.service;

import de.churchhostel.model.Contact;
import de.churchhostel.service.exception.ContactNotFoundException;
import de.churchhostel.service.exception.HostelNotFoundException;
import de.churchhostel.service.exception.InvalidDataException;

import java.util.List;

/**
 * Service used to access, modify or save Contact objects in the system.
 */
public interface ContactService {

	/**
	 * Returns the contact with id as an object that can be serialized into a response.
	 * @param id ID of the contact to be retrieved.
	 * @return Serializable contact object.
	 * @throws ContactNotFoundException If no such contact could be found.
	 */
	Contact getContact(long id) throws ContactNotFoundException;

	List<Contact> getHostelContacts(long id);

	/**
	 * Adds a contact.
	 * @return The newly created contact object.
	 * @throws HostelNotFoundException If the hoste the contact should be added to could not be found.
	 */
	Contact addContact(Contact newContact) throws HostelNotFoundException, InvalidDataException;

	/**
	 * Updates a contact. 
	 * If the attribute churchhostelId on the post data transfer object is not set, 
	 * the churchhostel the contact is associated to is not changed.
	 * @param id id of the contact to be updated.
	 * @throws ContactNotFoundException If no such contact could be found.
	 */
	void updateContact(long id, Contact updatedContact) throws InvalidDataException, ContactNotFoundException, HostelNotFoundException;
	
	/**
	 * Deletes a contact.
	 * @param id Contact to be deleted
	 * @throws ContactNotFoundException If no such contact could be found.
	 */
	void deleteContact(long id) throws ContactNotFoundException;
}
