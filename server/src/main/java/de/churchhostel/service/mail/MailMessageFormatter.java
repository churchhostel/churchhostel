package de.churchhostel.service.mail;

import de.churchhostel.model.mail.MailMessage;

/**
 * A MailMessageFormatter object can take any object and format it into a mail that may be
 * send using the MailService
 *
 * @param <T> Type of the source object that will be formatted into a mail message
 */
public interface MailMessageFormatter<T extends Object> {

	/**
	 * Formats given source object into a mail message.
	 * @param sourceObject Object that should be turned into a MailMessage object
	 * @return The MailMEssage object
	 * @throws FormatterException When the given object cannot be formatted into a mail message due to missing or invalid fields
	 */
	MailMessage format(T sourceObject) throws FormatterException;

}
