package de.churchhostel.service.mail;

import de.churchhostel.rest.mail.dto.ContactMailPOST;
import de.churchhostel.service.exception.InvalidDataException;
import de.churchhostel.util.mail.Mailer;
import de.churchhostel.util.mail.MailerException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * A ContactMailService object is used to send mails from mail requests posted to the API. 
 * It uses a MailMessageFormatter to format the post request into a sendable message and sends
 * it using a Mailer.
 */
public class ContactMailService {

	private static final Logger log = LogManager.getLogger(ContactMailService.class);
	
	private Mailer mailer;
	private MailMessageFormatter<ContactMailPOST> messageFormatter;

	public ContactMailService(Mailer mailer, MailMessageFormatter<ContactMailPOST> messageFormatter) {
		this.mailer = mailer;
		this.messageFormatter = messageFormatter;
	}

	/**
	 * Sends a mail from a request posted to the API.
	 * @param mailInformation POST-Object that was sent to the API.
	 * @throws InvalidDataException If one or more fields of the post request are invalid or the post request body was empty.
	 * @throws MailerException If the service was unable to send a mail with the given mailer.
	 */
	public void sendContactMail(ContactMailPOST mailInformation) throws InvalidDataException, MailerException {
		if (mailInformation == null) {
			throw new InvalidDataException();
		}

		try {
			mailer.sendMail(messageFormatter.format(mailInformation));
		} catch (FormatterException e) {
			log.error("Error formatting message", e);
			throw new InvalidDataException("The data provided contained empty fields or was malformatted", e);
		} catch (MailerException e) {
			log.error("Could not send mail", e);
			throw e;
		}

	}
}
