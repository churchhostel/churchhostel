package de.churchhostel.service.mail;

import de.churchhostel.model.mail.MailMessage;
import de.churchhostel.rest.mail.dto.ContactMailPOST;

/**
 * The ContactMailMessageFormatter formats ContactMailPOST objects sent to the API into sendable mail messages.
 */
public class ContactMailMessageFormatter implements MailMessageFormatter<ContactMailPOST> {

	private static final String CH_CONTACT_ADDRESS = "kontakt@churchhostel.de";

	@Override
	public MailMessage format(ContactMailPOST sourceObject) throws FormatterException {
		if (sourceObject.getContactName() == null) {
			throw new FormatterException("Cannot send contact email without contact name");
		}
		if (sourceObject.getEmail() == null) {
			throw new FormatterException("Cannot send contact email without email address");
		}

		MailMessage message = new MailMessage();
		message.setFrom(sourceObject.getEmail());
		message.setTo(new String[] { CH_CONTACT_ADDRESS });
		String body = String.format("<h2>%s möchte Kontakt mit uns aufnehmen.</h2>", sourceObject.getContactName());
		body += "Es wurden folgende Daten angegeben:";
		body += "<ul>";
		body += String.format("<li>Email: %s</li>", sourceObject.getEmail());
		if (sourceObject.getCommunityName() != null) {
			body += String.format("<li>Name der Gemeinde: %s</li>", sourceObject.getCommunityName());
		}
		if (sourceObject.getCommunityAddress() != null) {
			body += String.format("<li>Anschrift der Gemeinde: %s</li>", sourceObject.getCommunityAddress());
		}
		if (sourceObject.getPhoneNumber() != null) {
			body += String.format("<li>Telefonnummer: %s</li>", sourceObject.getPhoneNumber());
		}
		body += "</ul>";
		body += "<h3>Nachricht:</h3>";
		body += "<p>"+ sourceObject.getMessage() + "</p>";
		message.setBody(body);
		message.setSubject("Kontaktanfrage");

		return message;
	}

}
