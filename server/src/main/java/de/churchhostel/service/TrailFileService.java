package de.churchhostel.service;

import java.nio.file.Path;

public class TrailFileService extends SimpleFileService {

    public TrailFileService(Path filesDir) {
        super(filesDir);
    }

    public TrailFileService() {
        super();
    }

    @Override
    protected String getSubDir() {
        return "trails/";
    }
}
