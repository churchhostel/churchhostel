package de.churchhostel.service;

import de.churchhostel.model.Trail;
import de.churchhostel.repository.TrailRepository;
import de.churchhostel.service.exception.InvalidDataException;
import de.churchhostel.service.exception.TrailNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;

import static de.churchhostel.util.Utils.nullOrEmpty;

public class TransactionManagedTrailService implements TrailService {
	
	private static final Logger log = LogManager.getLogger(TransactionManagedTrailService.class);

	private TrailRepository trailRepo;

	public TransactionManagedTrailService(TrailRepository trailRepo) {
		this.trailRepo = trailRepo;
	}

	@Override
	public Trail getTrail(long id) throws TrailNotFoundException {
		Optional<Trail> t = trailRepo.find(id);
		if (!t.isPresent()) {
			throw new TrailNotFoundException();
		}

		return t.get();
	}

	@Override
	public Trail addTrail(Trail newTrail) throws InvalidDataException {
	    if (nullOrEmpty(newTrail.getName()) || nullOrEmpty(newTrail.getFileUrl())
                || nullOrEmpty(newTrail.getStartLocation()) || nullOrEmpty(newTrail.getEndLocation()) || newTrail.getLength() == null) {
            throw new InvalidDataException("One or more required fields missing to add trail");
        }
		// Find a file name for the trail first
//		String imageFilename;
//		try {
//			imageFilename = fileService.findValidFilename("trails", newTrail.getName());
//		} catch (FileServiceException e) {
//			log.error("Could not find a valid file name to persist the trail with", e.getMessage());
//			throw new InvalidDataException("Filename already exists and an alternate filename could not be found: " + e.getMessage());
//		}
		
		// Always use unix style forward slash for image name, as it is separated by this in the file resource 
//		String finalFileName = "trails/" + imageFilename;
		// FIXME für den neuen Trail muss noch ein Dateipfad ermittelt werden
		// Trail trail = new Trail(newTrail.getName(), finalFileName, newTrail.getStartLocation(), newTrail.getEndLocation(), newTrail.getLength());
		
		return trailRepo.create(newTrail);
	}

	@Override
	public List<Trail> getAll() {
		return trailRepo.findAll();
	}

	@Override
	public void updateTrail(long id, Trail updatedTrail) throws TrailNotFoundException {
		Optional<Trail> originalTrail = trailRepo.find(id);
		if (!originalTrail.isPresent()) {
			throw new TrailNotFoundException();
		}

		Trail mergedTrail = originalTrail.get();

		if (!nullOrEmpty(updatedTrail.getName())) {
			mergedTrail.setName(updatedTrail.getName());
		}
        if (!nullOrEmpty(updatedTrail.getFileUrl())) {
            mergedTrail.setFileUrl(updatedTrail.getFileUrl());
        }
		if (updatedTrail.getLength() != null) {
			mergedTrail.setLength(updatedTrail.getLength());
		}
		if (!nullOrEmpty(updatedTrail.getStartLocation())) {
			mergedTrail.setStartLocation(updatedTrail.getStartLocation());
		}
		if (!nullOrEmpty(updatedTrail.getEndLocation())) {
			mergedTrail.setEndLocation(updatedTrail.getEndLocation());
		}

		trailRepo.update(mergedTrail);
	}

	@Override
	public void deleteTrail(long id) throws TrailNotFoundException {
		if (!trailRepo.exists(id)) {
			throw new TrailNotFoundException();
		}
		
		trailRepo.delete(id);
	}
	
}