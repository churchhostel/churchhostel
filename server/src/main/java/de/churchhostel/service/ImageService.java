package de.churchhostel.service;

import de.churchhostel.model.Image;
import de.churchhostel.service.exception.HostelNotFoundException;
import de.churchhostel.service.exception.ImageNotFoundException;
import de.churchhostel.service.exception.InvalidDataException;

import java.util.List;

/**
 * Service used to access, modify or save Image objects in the system.
 */
public interface ImageService {

	/**
	 * Retrieves an image as a serializable object.
	 * @param id Id of the image
	 * @return The serializable object that can be returned in a response.
	 * @throws ImageNotFoundException If no such image could be found.
	 */
	Image getImage(long id) throws ImageNotFoundException;

	List<Image> getHostelImages(long id);

	/**
	 * Adds an image.
	 * @return The created image object.
	 * @throws HostelNotFoundException If the hostel the image should be associated to could not be found.
	 * @throws InvalidDataException If one or more fields in the image information was invalid
	 */
	Image addImage(Image newImage) throws HostelNotFoundException, InvalidDataException;

	/**
	 * Updates image information. If the churchostelId attribute is not set, it remains unchanged.
	 * @param id the image that should be updated.
	 * @throws ImageNotFoundException If no such image could be found.
	 */
	void updateImage(long id, Image updatedImage) throws ImageNotFoundException, InvalidDataException, HostelNotFoundException;
	
	/**
	 * Deletes an image and the associated file on the file system.
	 * @param id Image to be deleted
	 * @throws ImageNotFoundException If no such image could be found.
	 */
	void deleteImage(long id) throws ImageNotFoundException;
}
