package de.churchhostel.service;

import de.churchhostel.model.Churchhostel;
import de.churchhostel.model.Marker;
import de.churchhostel.repository.ChurchhostelRepository;

import java.util.List;
import java.util.stream.Collectors;

public class TransactionManagedMarkerService implements MarkerService {

	private ChurchhostelRepository chRepo;
	
	public TransactionManagedMarkerService(ChurchhostelRepository chRepo) {
		this.chRepo = chRepo;
	}

	@Override
	public List<Marker> getAllMarker() {
		List<Churchhostel> chs = chRepo.findActive();
		return chs.stream().map(this::createMarker).collect(Collectors.toList());
	}


	private Marker createMarker(Churchhostel ch) {
		Marker m = new Marker();
		m.setLongitude(ch.getLongitude());
		m.setLatitude(ch.getLatitude());
		m.setBeds(ch.getBeds());
		m.setChurchhostelId(ch.getId());
		return m;
	}
}