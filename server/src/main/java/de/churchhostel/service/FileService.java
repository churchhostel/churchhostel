package de.churchhostel.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

import de.churchhostel.service.exception.FileServiceException;

/**
 * Service to store and retrieve files.
 */
public interface FileService {

	/**
	 * Stores a  file.
	 * @param preferredName Name of the file where the file content should be stored.
	 * @param fileContent Content of the file.
     * @return The actual file name which can be different if a file with the preferred name already existed
	 * @throws FileServiceException If the file could not be stored.
	 */
	String saveFile(String preferredName, InputStream fileContent) throws FileServiceException;

	/**
	 * Retrieves a trail file.
	 * @param name Name of the file
	 * @return The file
	 * @throws FileNotFoundException If no file with given name could be found within the subdirectory.
	 */
	File getFile(String name) throws FileNotFoundException;

	/**
	 * Removes a file.
	 * @param name Name of the file
	 * @throws FileServiceException If the file could not be deleted.
	 */
	void deleteFile(String name) throws FileServiceException;

}
