package de.churchhostel.service;

import de.churchhostel.model.Image;
import de.churchhostel.repository.ChurchhostelRepository;
import de.churchhostel.repository.ImageRepository;
import de.churchhostel.service.exception.HostelNotFoundException;
import de.churchhostel.service.exception.ImageNotFoundException;
import de.churchhostel.service.exception.InvalidDataException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;

public class TransactionManagedImageService implements ImageService {

	private static final Logger log = LogManager.getLogger(TransactionManagedImageService.class);

	private ChurchhostelRepository chRepo;
	private ImageRepository imageRepo;

	public TransactionManagedImageService(ChurchhostelRepository chRepo, ImageRepository imageRepo) {
		this.chRepo = chRepo;
		this.imageRepo = imageRepo;
	}

	@Override
	public Image getImage(long id) throws ImageNotFoundException {
		Optional<Image> image = imageRepo.find(id);
		if (!image.isPresent()) {
			log.warn("Image with id " + id + " requested but could not be found");
			throw new ImageNotFoundException();
		}

		return image.get();
	}

    @Override
    public List<Image> getHostelImages(long id) {
        return imageRepo.findHostelImages(id);
    }

    @Override
	public Image addImage(Image newImage) throws HostelNotFoundException, InvalidDataException {
		if (newImage.getChurchhostelId() == null) {
			log.error("Cannot add image without a churchhostel ID relation set: " + newImage);
			throw new InvalidDataException("Insufficient data to add image.");
		}
		
		if (!chRepo.exists(newImage.getChurchhostelId())) {
			log.error("Could not add image for unknown hostel with id " + newImage.getChurchhostelId());
			throw new HostelNotFoundException();
		}

		return imageRepo.create(newImage);

		// Find a file name for the image in our file service first, because if we don't find one,
		// we have to abort persisting the image
//		String subDir = String.valueOf(newImage.getChurchhostelId());
//		String imageFilename;
//		try {
//			imageFilename = fileService.findValidFilename(subDir, "bla");
//		} catch (FileServiceException e) {
//			log.error("Could not find a valid file name to persist the image with", e.getMessage());
//			throw new InvalidDataException("Filename already exists and an alternate filename could not be found: " + e.getMessage());
//		}
//
//		// Always use unix style forward slash for image name, as it is separated by this in the file resource
//		String finalFileName = subDir + "/" + imageFilename;
//
//		imageRepo.persist(newImage);
	}

	@Override
	public void updateImage(long id, Image updatedImage) throws ImageNotFoundException, InvalidDataException, HostelNotFoundException {
		Optional<Image> originalImage = imageRepo.find(id);
		if (!originalImage.isPresent()) {
			throw new ImageNotFoundException();
		}

		Image merged = originalImage.get();
		
		if (updatedImage.getChurchhostelId() != null) {
			if (!chRepo.exists(updatedImage.getChurchhostelId())) {
				log.error("Could not update image hostel relation. Hostel " + updatedImage.getChurchhostelId() + " not found.");
				throw new HostelNotFoundException();
			}
			merged.setChurchhostelId(updatedImage.getChurchhostelId());
		}
		
		imageRepo.update(merged);
	}

	@Override
	public void deleteImage(long id) throws ImageNotFoundException {
		if (!imageRepo.exists(id)) {
			throw new ImageNotFoundException();
		}
		
		// Delete the associated file
//		try {
//			fileService.deleteFile(image.getFilePath());
//		} catch (FileServiceException e) {
//			log.error("Could not delete file " + image.getFilePath() + " on disk", e);
//		}
		
		imageRepo.delete(id);
	}
	
}