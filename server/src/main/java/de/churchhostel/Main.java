package de.churchhostel;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.EnumSet;

import javax.servlet.DispatcherType;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.logging.log4j.LogManager;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.eclipse.jetty.util.log.Log;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

import de.churchhostel.util.Log4j2Bridge;

public class Main {

	private static final Options OPTIONS = new Options();
	static {
		OPTIONS.addRequiredOption(null, "config", true, "Config file containing database connection parameters");
	}

	public static void main(String[] args) throws Exception {
		System.setProperty("java.util.logging.manager", "org.apache.logging.log4j.jul.LogManager");

		CommandLine cmd = new DefaultParser().parse(OPTIONS, args);
		Path configFilePath = Paths.get(cmd.getOptionValue("config"));

		ServerConfiguration.readConfig(configFilePath);

		// Redirect jetty logging to our log4j2 bridge
		Log.setLog(new Log4j2Bridge("Jetty-Server"));

		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
		context.setContextPath("/");

		FilterHolder filter = new FilterHolder();
		filter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM,
				"http://localhost:8081,https://localhost:8081,http://server:8080,https://churchhostel.de,https://www.churchhostel.de,https://api.churchhostel.de");
		filter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
		filter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_HEADERS_HEADER, "X-Requested-With,Content-Type,Accept,Origin,Cache-Control,Authorization");
		filter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_METHODS_HEADER, "POST,GET,PUT,DELETE,OPTIONS,HEAD");
		filter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
		filter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "POST,GET,PUT,DELETE,OPTIONS,HEAD");
		filter.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Accept,Origin,Cache-Control,Authorization");
		// filter.setInitParameter(CrossOriginFilter.CHAIN_PREFLIGHT_PARAM, "false");
		filter.setInitParameter("exposedHeaders", "Authorization");
		// filter.setInitParameter("preflightMaxAge", "728000");
		filter.setInitParameter("allowCredentials", "true");
		CrossOriginFilter corsFilter = new CrossOriginFilter();
		filter.setFilter(corsFilter);

		Server server = new Server(8080);
		server.setHandler(context);

		final ResourceConfig resourceConfig = new ResourceConfig();
		resourceConfig.packages("de.churchhostel.rest", "com.fasterxml.jackson.jaxrs.json");
		resourceConfig.register(MultiPartFeature.class);

		ServletHolder jerseyServlet = new ServletHolder(new ServletContainer(resourceConfig));
		jerseyServlet.setInitOrder(1);
		context.addServlet(jerseyServlet, "/api/*");
		context.addFilter(filter, "/*", EnumSet.of(DispatcherType.REQUEST));

		try {
			server.start();
			server.join();
		} catch (Exception e) {
			LogManager.getLogger(Main.class.getName()).error(e);
		} finally {
			server.destroy();
		}
	}
}
