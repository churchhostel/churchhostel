package de.churchhostel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.churchhostel.repository.database.ConnectionPool;

public class ServerConfiguration {

    private static final Logger log = LogManager.getLogger(ServerConfiguration.class);

    private static Path filesDir;
    private static byte[] jwtSigningKey;
    private static int thumbnailSize;
    private static int mediumQualityImageSize;
    public static String SMTP_HOST, SMTP_PORT, SMTP_USER, SMTP_PASSWORD;
    public static String PAGE_URL;

    public static void readConfig(Path configFile) throws Exception {
        if (!Files.exists(configFile)) {
            throw new FileNotFoundException("Config file " + configFile + " does not exist.");
        }
        if (!Files.isReadable(configFile)) {
            throw new FileNotFoundException("Cannot read config file " + configFile);
        }

        log.info("Configuration file found. Initializing system.");

        JsonObject configJson;
        try (InputStream configFis = new FileInputStream(configFile.toFile());
                JsonReader configReader = Json.createReader(configFis)) {
            configJson = configReader.readObject();
        }

        JsonObject dbConfig = configJson.getJsonObject("chDB");
        Map<String, String> persistenceConfig = new HashMap<>();
        persistenceConfig.put("url", dbConfig.getString("url"));
        persistenceConfig.put("user", dbConfig.getString("user"));
        persistenceConfig.put("password", dbConfig.getString("password"));
        ConnectionPool.getInstance().setDatasourceProperties(persistenceConfig);

        setFilesDir(configJson.getString("filesDir"));
        jwtSigningKey = configJson.getString("jwtKey").getBytes();
        JsonObject mailConfig = configJson.getJsonObject("mail");
        SMTP_HOST = mailConfig.getString("host");
        SMTP_PORT = mailConfig.getString("port");
        SMTP_USER = mailConfig.getString("user");
        SMTP_PASSWORD = mailConfig.getString("password");

        thumbnailSize = configJson.getInt("thumbnailSize", 400);
        mediumQualityImageSize = configJson.getInt("mediumQualityImageSize", 1024);
        PAGE_URL = configJson.getString("pageUrl", "https://www.churchhostel.de");
    }

    public static Path getFilesDir() {
        return filesDir;
    }

    public static int getThumbnailSize() {
        return thumbnailSize;
    }

    public static int getMediumQualityImageSize() {
        return mediumQualityImageSize;
    }

    public static void setFilesDir(String filesDir) throws Exception {
        Path filesDirPath = Paths.get(filesDir);
        if (!Files.exists(filesDirPath)) {
            try {
                Files.createDirectory(filesDirPath);
            } catch (IOException e) {
                throw new Exception(
                        "Files base directory " + filesDirPath + " does not exist and could not be created.");
            }
        }
        if (!Files.isReadable(filesDirPath) || !Files.isWritable(filesDirPath)) {
            throw new Exception("Files base directory " + filesDirPath + " is not read- or writable.");
        }
        ServerConfiguration.filesDir = filesDirPath;
    }

    public static void setJWTSigningKey(String key) {
        jwtSigningKey = key.getBytes();
    }

    public static byte[] getJWTSigningKey() {
        return jwtSigningKey;
    }
}
