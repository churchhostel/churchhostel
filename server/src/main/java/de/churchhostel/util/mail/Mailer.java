package de.churchhostel.util.mail;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.churchhostel.ServerConfiguration;
import de.churchhostel.model.mail.MailMessage;

/**
 * This Mailer sends mails with tls and authentication enabled and expects the variables mail.smtp.host, mail.smtp.port, mail.smtp.user and mail.smtp.password to be set
 * in the environment.
 *
 */
public class Mailer {
	
	private static final Logger log = LogManager.getLogger(Mailer.class);
	
	public void sendMail(MailMessage formattedMessage) throws MailerException {
		HtmlEmail mail = new HtmlEmail();
		mail.setHostName(ServerConfiguration.SMTP_HOST);
		mail.setSmtpPort(Integer.valueOf(ServerConfiguration.SMTP_PORT));
		mail.setAuthenticator(new DefaultAuthenticator(ServerConfiguration.SMTP_USER, ServerConfiguration.SMTP_PASSWORD));
		mail.setSSLOnConnect(true);
		try {
			mail.setFrom(formattedMessage.getFrom());
			mail.setSubject(formattedMessage.getSubject());
			mail.setHtmlMsg(formattedMessage.getBody());
			mail.setCharset("utf-8");
			mail.addTo(formattedMessage.getTo());
			mail.send();
		} catch (EmailException e1) {
			log.error("Failed to send contact mail", e1);
		}

	}
}
