package de.churchhostel.util;

import java.util.List;
import java.util.Set;

public class Utils {

	public static boolean nullOrEmpty(String x) {
		return x == null || x.isEmpty();
	}

    public static boolean nullOrEmpty(Long x) {
        return x == null;
    }

	public static boolean nullOrEmpty(List<?> x) {
		return x == null || x.isEmpty();
	}

	public static boolean nullOrEmpty(Set<?> x) {
		return x == null || x.isEmpty();
	}

}
