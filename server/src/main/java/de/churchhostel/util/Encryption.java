package de.churchhostel.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class Encryption {

    public static String sha256base64(String clearText) throws EncryptionException {
        MessageDigest msgDigest = null;
        try {
            msgDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new EncryptionException(e);
        }
        byte[] hash = msgDigest.digest(clearText.getBytes(StandardCharsets.UTF_8));
        return Base64.getEncoder().encodeToString(hash);
    }


}
