package de.churchhostel.util;

/**
 * @author Simon Arnold <simonh.arnold@gmail.com>
 */
@FunctionalInterface
public interface CheckedFunction<T, R> {
    R apply(T t) throws Exception;
}
