package de.churchhostel.repository;

import de.churchhostel.model.Image;
import de.churchhostel.repository.database.DatabaseQuery;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ImageRepository extends AbstractRepository<Image> {

    private static final Logger log = LogManager.getLogger(ImageRepository.class);

    public ImageRepository() {
        super("image");
    }

    public List<Image> findHostelImages(long hostelId) {
        DatabaseQuery query = database.createQuery("SELECT * From image where image.churchhostel_id = ?;", hostelId);
        List<Image> result = query.execute(resultSet -> {
            List<Image> c = new ArrayList<>();
            while (resultSet.next()) {
                c.add(mapSingleResult(resultSet));
            }
            return c;
        });
        return result;
    }

    @Override
    public Image create(Image entity) {
        DatabaseQuery insertQuery = database.createQuery("INSERT INTO image (filePath, churchhostel_id) VALUES (?, ?)", entity.getFileUrl(), entity.getChurchhostelId());
        List<Long> generatedIDs = insertQuery.executeUpdate();
        if (generatedIDs.isEmpty()) {
            log.warn("No ID was generated when creating image");
            return entity;
        }
        entity.setId(generatedIDs.get(0));
        return entity;
    }

    @Override
    public void update(Image entity) {
        DatabaseQuery updateQuery = database.createQuery("UPDATE image SET filePath = ?, churchhostel_id = ? WHERE id = ?",
                entity.getFileUrl(), entity.getChurchhostelId(), entity.getId());
        updateQuery.executeUpdate();
    }

    @Override
    protected Image mapSingleResult(ResultSet res) throws SQLException {
        Image i = new Image();
        i.setId(res.getLong("id"));
        i.setFileUrl(res.getString("filePath"));
        i.setChurchhostelId(res.getLong("churchhostel_id"));
        return i;
    }
}
