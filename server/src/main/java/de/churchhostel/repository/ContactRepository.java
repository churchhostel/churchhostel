package de.churchhostel.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.churchhostel.model.Contact;
import de.churchhostel.repository.database.DatabaseQuery;

public class ContactRepository extends AbstractRepository<Contact> {

    private static final Logger log = LogManager.getLogger(ContactRepository.class);

    public ContactRepository() {
        super("contact");
    }

    public List<Contact> findHostelContacts(long hostelId) {
         DatabaseQuery query = database.createQuery("SELECT * From contact where contact.churchhostel_id = ?;", hostelId);
         List<Contact> result = query.execute(resultSet -> {
            List<Contact> c = new ArrayList<>();
            while (resultSet.next()) {
                c.add(mapSingleResult(resultSet));
            }
            return c;
         });
         return result;

    }

    @Override
    public Contact create(Contact entity) {
        DatabaseQuery updateQuery = database.createQuery("INSERT INTO contact (name, phone, email, churchhostel_id) VALUES (?, ?, ?, ?)", entity.getName(), entity.getPhone(), entity.getEmail(), entity.getChurchhostelId());
        List<Long> generatedIDs = updateQuery.executeUpdate();
        if (generatedIDs.isEmpty()) {
            log.warn("Insert Statement for new Contact did not return an ID for inserted row.");
        }
        long generatedID = generatedIDs.get(0);
        log.info("Created entity " + generatedID);
        entity.setId(generatedID);
        return entity;
    }

    @Override
    public void update(Contact entity) {
        DatabaseQuery updateQuery = database.createQuery("UPDATE contact SET name = ?, phone = ?, email = ?, churchhostel_id = ? WHERE id = ?",
                entity.getName(), entity.getPhone(), entity.getEmail(), entity.getChurchhostelId(), entity.getId());
        updateQuery.executeUpdate();
    }

    @Override
	protected Contact mapSingleResult(ResultSet res) throws SQLException {
        final Contact c = new Contact();
        c.setId(res.getLong("id"));
        c.setName(res.getString("name"));
        c.setPhone(res.getString("phone"));
        c.setEmail(res.getString("email"));
        c.setChurchhostelId(res.getLong("churchhostel_id"));
        return c;
    }

}
