package de.churchhostel.repository;

import de.churchhostel.model.Trail;
import de.churchhostel.repository.database.DatabaseQuery;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class TrailRepository extends AbstractRepository<Trail> {

    private static final Logger log = LogManager.getLogger(TrailRepository.class);

    public TrailRepository() {
        super("trail");
    }

    @Override
    public Trail create(Trail entity) {
        DatabaseQuery updateQuery = database.createQuery("INSERT INTO trail (fileUrl, `length`, name, startLocation, endLocation) VALUES (?, ?, ?, ?, ?)",
                entity.getFileUrl(), entity.getLength(), entity.getName(), entity.getStartLocation(), entity.getEndLocation());
        List<Long> generatedIDs = updateQuery.executeUpdate();
        if (generatedIDs.isEmpty()) {
            log.warn("Insert statement for new Trail did not return an ID for inserted row.");
        }
        long generatedID = generatedIDs.get(0);
        log.info("Created trail entity " + generatedID);
        entity.setId(Long.valueOf(generatedID));
        return entity;
    }

    @Override
    public void update(Trail entity) {
        DatabaseQuery updateQuery = database.createQuery("UPDATE trail SET fileUrl = ?, `length` = ?, `name` = ?, startLocation = ?, endLocation = ? WHERE id = ?",
                entity.getFileUrl(), entity.getLength(), entity.getName(), entity.getStartLocation(), entity.getEndLocation(), entity.getId());
        updateQuery.executeUpdate();
    }

    @Override
    protected Trail mapSingleResult(ResultSet res) throws SQLException {
        final Trail t = new Trail();
        t.setId(res.getLong("id"));
        t.setName(res.getString("name"));
        t.setFileUrl(res.getString("fileUrl"));
        t.setStartLocation(res.getString("startLocation"));
        t.setEndLocation(res.getString("endLocation"));
        t.setLength(res.getInt("length"));
        return t;
    }
}
