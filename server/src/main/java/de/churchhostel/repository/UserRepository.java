package de.churchhostel.repository;

import de.churchhostel.model.User;
import de.churchhostel.repository.database.DatabaseQuery;
import de.churchhostel.util.NotImplementedException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class UserRepository extends AbstractRepository<User> {

    private static final Logger log = LogManager.getLogger(UserRepository.class);

    public UserRepository() {
        super("ch_user");
    }

    public Optional<User> findByName(final String username) {
        DatabaseQuery query = database.createQuery("SELECT * FROM " + TABLE_NAME +" WHERE name = ?", username);
        User result = query.execute(resultSet -> {
            if (!resultSet.next())  {
                return null;
            }
            return mapSingleResult(resultSet);
        });
        return Optional.ofNullable(result);
    }


    @Override
    public User create(User entity) {
        throw new NotImplementedException();
    }

    @Override
    public void update(User entity) {
        throw new NotImplementedException();
    }

    @Override
    protected User mapSingleResult(ResultSet res) throws SQLException {
        return new User(res.getLong("id"), res.getString("name"), res.getString("pw_hash"));
    }

}
