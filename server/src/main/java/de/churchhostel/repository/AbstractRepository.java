package de.churchhostel.repository;

import de.churchhostel.repository.database.ConnectionPool;
import de.churchhostel.repository.database.DataBase;
import de.churchhostel.repository.database.DatabaseImpl;
import de.churchhostel.repository.database.DatabaseQuery;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Abstract repository class, base class for other repositories
 *
 * @param <T> Class of the entity the repository retrieves/stores
 */
public abstract class AbstractRepository<T> {
	
	private static final Logger log = LogManager.getLogger(AbstractRepository.class);

	protected final String TABLE_NAME;
	protected final DataBase database;

	public AbstractRepository(String tableName) {
		TABLE_NAME = tableName;
	    database = new DatabaseImpl(ConnectionPool.getInstance());
	}

	public Optional<T> find(final long id) {
	    DatabaseQuery query = database.createQuery("SELECT * FROM " + TABLE_NAME +" WHERE id = ?", id);
	    T result = query.execute(resultSet -> {
	        if (!resultSet.next())  {
	           return null;
            }
	        return mapSingleResult(resultSet);
        });
	    return Optional.ofNullable(result);
	}

	public List<T> findAll() {
		DatabaseQuery query = database.createQuery("SELECT * FROM " + TABLE_NAME + " WHERE 1;");
        List<T> result = query.execute( resultSet -> {
        	List<T> all = new ArrayList<>();
        	while (resultSet.next()) {
        		all.add(mapSingleResult(resultSet));
			}
			return all;
		});
		return result;
	}

	public void deleteAll() {
		DatabaseQuery q = database.createQuery("DELETE FROM " + TABLE_NAME + " WHERE 1;");
		q.executeUpdate();
	}

	public void delete(final long id) {
		database.createQuery("DELETE FROM " + TABLE_NAME + " WHERE id = ?", id).executeUpdate();
	}

	/**
	 * Simply tests if a row exists, possibly doing so in an efficient way.
	 * @param id
	 * @return
	 */
	public boolean exists(final long id) {
		DatabaseQuery query = database.createQuery("SELECT * FROM " + TABLE_NAME +" WHERE id = ?", id);
        boolean result = query.execute(resultSet -> resultSet.next());
       	return result;
	}

	/**
	 * Creates an entity and returns the generated ID
	 * @param entity
	 * @return
	 */
	public abstract T create(T entity);

	/**
	 * Updates an entity with new values
	 */
	public abstract void update(T entity);

	protected abstract T mapSingleResult(ResultSet res) throws SQLException;

}
