package de.churchhostel.repository.database;

import de.churchhostel.util.CheckedFunction;

import java.sql.ResultSet;
import java.util.List;

/**
 * @author Simon Arnold <simonh.arnold@gmail.com>
 */
public interface DatabaseQuery {

    <T> T execute(CheckedFunction<ResultSet, T> mapper);

    List<Long> executeUpdate();

}
