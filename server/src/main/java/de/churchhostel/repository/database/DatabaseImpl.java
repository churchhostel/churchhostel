package de.churchhostel.repository.database;

import de.churchhostel.util.CheckedFunction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @author Simon Arnold <simonh.arnold@gmail.com>
 */
public class DatabaseImpl implements DataBase {

    private static final Logger log = LogManager.getLogger(DatabaseImpl.class);

    private final ConnectionPool dataSource;

    public DatabaseImpl(final ConnectionPool dataSource) {
        this.dataSource = dataSource;
    }

    public final Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new DatabaseConnectionFailureException(e);
        }
    }

    @Deprecated
    @Override
    public int update(String sql, Object[] parameters) throws DataAccessException {
        return 0;
    }

    @Override
    public <T> T select(String sql, CheckedFunction<ResultSet, T> responseMapper, Object... parameters) throws DataAccessException {
        return null;
    }

    @Override
    public DatabaseQuery createQuery(String sql, Object... parameters) {
        return new DatabaseQueryImpl(sql, parameters);
    }

    @Override
    public DatabaseQuery createQuery(String sql, List<Object> parameters) {
        return new DatabaseQueryImpl(sql, parameters.toArray());
    }

    class DatabaseQueryImpl implements DatabaseQuery {

        private final String sql;
        private final Object[] parameter;

        public DatabaseQueryImpl(String sql, Object[] parameter) {
            this.sql = sql;
            this.parameter = parameter;
        }

        @Override
        public <T> T execute(CheckedFunction<ResultSet, T> mapFunction) {
            T result = null;
            try (Connection con = getConnection();
                 PreparedStatement stmt = createStatement(con, sql, parameter);
                 ResultSet res = stmt.executeQuery()) {
                result = mapFunction.apply(res);
            } catch (SQLException e) {
                throw new DataAccessException(e);
            } catch (Exception e) {
                System.err.println("Mapping result row to contact failed. " + e.getMessage());
                e.printStackTrace();
            }
            return result;
        }

        @Override
        public List<Long> executeUpdate() {
            List<Long> generatedKeys = new ArrayList<>();
            Connection con = null;
            PreparedStatement stmt = null;
            ResultSet generatedKeysRs = null;
            try {
                con = getConnection();
                stmt = createStatement(con, sql, parameter);
                stmt.executeUpdate();
                generatedKeysRs = stmt.getGeneratedKeys();
                while (generatedKeysRs.next()) {
                    generatedKeys.add(generatedKeysRs.getLong(1));
                }
                return generatedKeys;
            } catch (SQLException | DataAccessException e) {
                log.fatal(e);
            } finally {
                try {
                    con.close();
                    stmt.close();
                    generatedKeysRs.close();
                } catch (SQLException | NullPointerException e) {
                    e.printStackTrace();
                }
            }
            return new ArrayList<>();
        }

    }

    private void rollback(final Connection con) {
        if (con != null) {
            try {
                con.rollback();
            } catch (SQLException e) {
               e.printStackTrace();
               log.error(e);
            }
        }
    }

    private PreparedStatement createStatement(Connection con, String sql, Object... parameters) {
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            setParameters(stmt, parameters);
        } catch (SQLException e) {
            e.printStackTrace();
            log.error(e);
        }
        return stmt;
    }

    private void setParameters(final PreparedStatement stmt, final Object... parameters) throws SQLException {
        for (int i = 0; i < parameters.length; i++) {
            final Object parameter = parameters[i];
            final int parameterIndex = i + 1;
            if (null == parameter)
            {
                stmt.setNull(parameterIndex, Types.NULL);
            }
            else if (parameter instanceof Boolean)
            {
                stmt.setBoolean(parameterIndex, (Boolean) parameter);
            }
            else if (parameter instanceof Character)
            {
                stmt.setString(parameterIndex, String.valueOf(parameter));
            }
            else if (parameter instanceof Byte)
            {
                stmt.setByte(parameterIndex, (Byte) parameter);
            }
            else if (parameter instanceof Short)
            {
                stmt.setShort(parameterIndex, (Short) parameter);
            }
            else if (parameter instanceof Integer)
            {
                stmt.setInt(parameterIndex, (Integer) parameter);
            }
            else if (parameter instanceof Long)
            {
                stmt.setLong(parameterIndex, (Long) parameter);
            }
            else if (parameter instanceof Float)
            {
                stmt.setFloat(parameterIndex, (Float) parameter);
            }
            else if (parameter instanceof Double)
            {
                stmt.setDouble(parameterIndex, (Double) parameter);
            }
            else if (parameter instanceof String)
            {
                stmt.setString(parameterIndex, (String) parameter);
            }
            else if (parameter instanceof Date)
            {
                stmt.setDate(parameterIndex, new Date(((Date) parameter)
                        .getTime()));
            }
            else if (parameter instanceof Calendar)
            {
                stmt.setDate(parameterIndex, new Date(((Calendar) parameter)
                        .getTimeInMillis()));
            }
            else if (parameter instanceof BigDecimal)
            {
                stmt.setBigDecimal(parameterIndex, (BigDecimal) parameter);
            }
            else
            {
                throw new IllegalArgumentException(
                    String.format("Unknown type of the parameter is found. [param: %s, paramIndex: %s]",parameter, parameterIndex));
            }
        }
    }

}
