package de.churchhostel.repository.database;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;


/**
 * @author Simon Arnold <simonh.arnold@gmail.com>
 */
public class ConnectionPool {

    private static final Logger log = LogManager.getLogger(ConnectionPool.class);

    private static final String MYSQL_DRIVER = "com.mysql.cj.jdbc.Driver";

    private static ConnectionPool instance;
    public static ConnectionPool getInstance() {
        if (instance == null) {
            instance = new ConnectionPool();
        }
        return instance;
    }

    private ComboPooledDataSource pooledDataSource;
    private ConnectionPool() {
        pooledDataSource = new ComboPooledDataSource();
    };

    public void setDatasourceProperties(Map<String, String> datasourceProperties) throws PropertyVetoException {
        log.info("Database connection is set to " + datasourceProperties.get("url"));
        pooledDataSource.setDriverClass(datasourceProperties.getOrDefault("driver", MYSQL_DRIVER));
        pooledDataSource.setJdbcUrl(datasourceProperties.get("url"));
        pooledDataSource.setUser(datasourceProperties.get("user"));
        pooledDataSource.setPassword(datasourceProperties.get("password"));
        pooledDataSource.setTestConnectionOnCheckout(true);
//        the settings below are optional
//        c3p0 can work with defaults
//        cpds.setMinPoolSize(5);
//        cpds.setAcquireIncrement(5);
//        cpds.setMaxPoolSize(20);
    }

    public Connection getConnection() throws SQLException {
        return pooledDataSource.getConnection();
    }
}
