package de.churchhostel.repository.database;

/**
 * @author Simon Arnold <simonh.arnold@gmail.com>
 */
public class DatabaseConnectionFailureException extends DataAccessException {
    public DatabaseConnectionFailureException() {
    }

    public DatabaseConnectionFailureException(String message) {
        super(message);
    }

    public DatabaseConnectionFailureException(String message, Throwable cause) {
        super(message, cause);
    }

    public DatabaseConnectionFailureException(Throwable cause) {
        super(cause);
    }

    public DatabaseConnectionFailureException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
