package de.churchhostel.repository.database;

import de.churchhostel.util.CheckedFunction;

import java.sql.ResultSet;
import java.util.List;

/**
 * @author Simon Arnold <simonh.arnold@gmail.com>
 */
public interface DataBase {

    DatabaseQuery createQuery(final String sql, Object... parameters);
    DatabaseQuery createQuery(final String sql, List<Object> parameters);

    <T> T select(String sql, CheckedFunction<ResultSet, T> responseMapper, Object... parameters) throws DataAccessException;
    int update(final String sql, final Object... parameters) throws DataAccessException;


}

