package de.churchhostel.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.churchhostel.model.Churchhostel;
import de.churchhostel.repository.database.DatabaseQuery;

public class ChurchhostelRepository extends AbstractRepository<Churchhostel> {

    private static final Logger log = LogManager.getLogger(ChurchhostelRepository.class);

    public ChurchhostelRepository() {
        super("churchhostel");
    }

    @Override
    public Optional<Churchhostel> find(long id) {
        Optional<Churchhostel> findResult = super.find(id);
        if (!findResult.isPresent()) {
            return findResult;
        }
        return Optional.of(loadRemainingData(findResult.get()));
    }

    @Override
    public List<Churchhostel> findAll() {
        List<Churchhostel> all = super.findAll();
        all.forEach(this::loadRemainingData);
        return all;
    }

    public List<Churchhostel> findActive() {
        DatabaseQuery query = database.createQuery("SELECT * FROM churchhostel WHERE active = 1");
        List<Churchhostel> all = query.execute( resultSet -> {
            List<Churchhostel> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(mapSingleResult(resultSet));
            }
            return result;
        });
        all.forEach(this::loadRemainingData);
        return all;
    }

    private Churchhostel loadRemainingData(Churchhostel ch) {
        DatabaseQuery serviceQuery = database.createQuery("SELECT s.description FROM service s WHERE s.churchhostel_id = ?", ch.getId());
        ch.setServices(serviceQuery.execute(res -> {
            List<String> services = new ArrayList<>();
            while (res.next()) {
                services.add(res.getString("description"));
            }
            return services;
        }));
        DatabaseQuery facilityQuery = database.createQuery("SELECT f.description FROM facility f WHERE f.churchhostel_id = ?", ch.getId());
        ch.setFacilities(facilityQuery.execute(res -> {
            List<String> facilities = new ArrayList<>();
            while (res.next()) {
                facilities.add(res.getString("description"));
            }
            return facilities;
        }));
        DatabaseQuery hintsQuery = database.createQuery("SELECT h.description FROM additional_hint h WHERE h.churchhostel_id = ?", ch.getId());
        ch.setAdditionalHints(hintsQuery.execute(res -> {
            List<String> hints = new ArrayList<>();
            while (res.next()) {
                hints.add(res.getString("description"));
            }
            return hints;
        }));
        return ch;
    }

    @Override
    public Churchhostel create(Churchhostel entity) {
        DatabaseQuery insertQuery = database.createQuery("INSERT INTO churchhostel (address, beds, email, latitude, longitude, `name`, price, `active`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                entity.getAddress(), entity.getBeds(), entity.getEmail(), entity.getLatitude(), entity.getLongitude(), entity.getName(), entity.getPrice(), 1);
        long id = insertQuery.executeUpdate().get(0);
        entity.setId(id);
        log.info("Persisted new hostel with ID " + id);

        if (!(entity.getServices() == null || entity.getServices().isEmpty())) {
            List<Object> serviceParams = entity.getServices().stream().flatMap(s -> Stream.of(id, s)).collect(Collectors.toList());
            DatabaseQuery insertServicesQuery = database.createQuery("INSERT INTO service (churchhostel_id, description) VALUES "
                    + String.join(",", Collections.nCopies(entity.getServices().size(), "(?,?)")), serviceParams);
            insertServicesQuery.executeUpdate();
        }

        if (!(entity.getFacilities() == null || entity.getFacilities().isEmpty())) {
            List<Object> facilityParams = entity.getFacilities().stream().flatMap(f -> Stream.of(id, f)).collect(Collectors.toList());
            DatabaseQuery insertFacilitiesQuery = database.createQuery("INSERT INTO facility (churchhostel_id, description) VALUES "
                    + String.join(",", Collections.nCopies(entity.getFacilities().size(), "(?,?)")), facilityParams);
            insertFacilitiesQuery.executeUpdate();
        }

        if (!(entity.getAdditionalHints() == null || entity.getAdditionalHints().isEmpty())) {
            List<Object> hintsParams = entity.getAdditionalHints().stream().flatMap(h -> Stream.of(id, h)).collect(Collectors.toList());
            DatabaseQuery insertHintsQuery = database.createQuery("INSERT INTO additional_hint (churchhostel_id, description) VALUES "
                    + String.join(",", Collections.nCopies(entity.getAdditionalHints().size(), "(?,?)")), hintsParams);
            insertHintsQuery.executeUpdate();
        }

        return entity;
    }

    @Override
    public void update(Churchhostel entity) {
        DatabaseQuery updateQuery = database.createQuery("UPDATE churchhostel SET address = ?, email = ?, beds = ?, latitude = ?, longitude = ?, `name` = ?, price = ? WHERE id = ?",
                entity.getAddress(), entity.getEmail(), entity.getBeds(), entity.getLatitude(), entity.getLongitude(), entity.getName(), entity.getPrice(), entity.getId());
        updateQuery.executeUpdate();

        // Replace services
        database.createQuery("DELETE FROM service WHERE churchhostel_id = ?", entity.getId()).executeUpdate();
        if (!entity.getServices().isEmpty()) {
            List<Object> serviceParams = entity.getServices().stream().flatMap(s -> Stream.of(entity.getId(), s)).collect(Collectors.toList());
            DatabaseQuery insertServicesQuery = database.createQuery("INSERT INTO service (churchhostel_id, description) VALUES "
                    + String.join(",", Collections.nCopies(entity.getServices().size(), "(?,?)")), serviceParams);
            insertServicesQuery.executeUpdate();
        }

        // Replace facilities
        database.createQuery("DELETE FROM facility WHERE churchhostel_id = ?", entity.getId()).executeUpdate();
        if (!entity.getFacilities().isEmpty()) {
            List<Object> facilityParams = entity.getFacilities().stream().flatMap(f -> Stream.of(entity.getId(), f)).collect(Collectors.toList());
            DatabaseQuery insertFacilitiesQuery = database.createQuery("INSERT INTO facility (churchhostel_id, description) VALUES "
                    + String.join(",", Collections.nCopies(entity.getFacilities().size(), "(?,?)")), facilityParams);
            insertFacilitiesQuery.executeUpdate();
        }

        // Replace additional hints
        database.createQuery("DELETE FROM additional_hint WHERE churchhostel_id = ?", entity.getId()).executeUpdate();
        if (!entity.getAdditionalHints().isEmpty()) {
            List<Object> hintsParams = entity.getAdditionalHints().stream().flatMap(h -> Stream.of(entity.getId(), h)).collect(Collectors.toList());
            DatabaseQuery insertHintsQuery = database.createQuery("INSERT INTO additional_hint (churchhostel_id, description) VALUES "
                    + String.join(",", Collections.nCopies(entity.getAdditionalHints().size(), "(?,?)")), hintsParams);
            insertHintsQuery.executeUpdate();
        }
    }

    @Override
    public void delete(long id) {
        DatabaseQuery deactivateQuery = database.createQuery("UPDATE churchhostel SET active = 0 WHERE id = ?", id);
        deactivateQuery.executeUpdate();
    }

    @Override
	protected Churchhostel mapSingleResult(ResultSet res) throws SQLException {
        final Churchhostel hostel = new Churchhostel();
        hostel.setId(res.getLong("id"));
        hostel.setName(res.getString("name"));
        hostel.setLatitude(res.getDouble("latitude"));
        hostel.setLongitude(res.getDouble("longitude"));
        hostel.setAddress(res.getString("address"));
        hostel.setEmail(res.getString("email"));
        hostel.setBeds(res.getInt("beds"));
        hostel.setPrice(res.getBigDecimal("price"));
        hostel.setActive(res.getBoolean("active"));
        return hostel;
    }


}
