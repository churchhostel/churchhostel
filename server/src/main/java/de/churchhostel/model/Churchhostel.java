package de.churchhostel.model;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.validator.internal.util.privilegedactions.NewSchema;

public class Churchhostel {

    private Long id;
    private Double latitude;
    private Double longitude;
    private String name;
    private String address;
    private Integer beds;
    private String email;
    private List<String> facilities;
    private List<String> services;
    private List<String> additionalHints;
    private BigDecimal price;
    private Boolean active;

    public Churchhostel() {
    }

    public Churchhostel(Double latitude, Double longitude, String name, String address, Integer beds, String email, List<String> facilities, List<String> services, List<String> additionalHints, BigDecimal price) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.address = address;
        this.beds = beds;
        this.email = email;
        this.facilities = facilities;
        this.services = services;
        this.additionalHints = additionalHints;
        this.price = price;
        this.active = true;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getBeds() {
        return beds;
    }

    public void setBeds(Integer beds) {
        this.beds = beds;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<String> facilities) {
        this.facilities = facilities;
    }

    public List<String> getServices() {
        return services;
    }

    public void setServices(List<String> services) {
        this.services = services;
    }

    public List<String> getAdditionalHints() {
        return additionalHints;
    }

    public void setAdditionalHints(List<String> additionalHints) {
        this.additionalHints = additionalHints;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Churchhostel that = (Churchhostel) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Churchhostel{" +
                "id=" + id +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", beds=" + beds +
                ", facilities=" + facilities +
                ", services=" + services +
                ", additionalHints=" + additionalHints +
                ", price=" + price +
                '}';
    }

    public static class Builder {
        private Churchhostel hostel = new Churchhostel();

        public Builder() {
            hostel.active = true;
        }
        public Builder(boolean isActive) {
            hostel.active = isActive;
        }
        public Builder(Long newId) {
            hostel.id = newId;
        }
        public Builder(Long newId, boolean isActive) {
            hostel.id = newId;
            hostel.active = isActive;
        }
        public Builder withLatitude(Double newLat) {
            hostel.latitude = newLat;
            return this;
        }
        public Builder withLongitude(Double newLong) {
            hostel.longitude = newLong;
            return this;
        }
        public Builder withName(String newName) {
            hostel.name = newName;
            return this;
        }
        public Builder withAddress(String newAddress) {
            hostel.address = newAddress;
            return this;
        }
        public Builder withBeds(int newBeds) {
            hostel.beds = newBeds;
            return this;
        }
        public Builder withEmail(String newEmail) {
            hostel.email = newEmail;
            return this;
        }
        public Builder withFacilities(List<String> newFacilities) {
            hostel.facilities = newFacilities;
            return this;
        }
        public Builder withServices(List<String> newServices) {
            hostel.services = newServices;
            return this;
        }
        public Builder withAdditionalHints(List<String> newAdditionalHints) {
            hostel.additionalHints = newAdditionalHints;
            return this;
        }
        public Builder withPrice(BigDecimal newPrice) {
            hostel.price = newPrice;
            return this;
        }
        public Churchhostel build() {
            return hostel;
        }
    }
}