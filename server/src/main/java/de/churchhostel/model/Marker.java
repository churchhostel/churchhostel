package de.churchhostel.model;

/**
 * Der Marker wird nicht persistiert, gehört aber sehr eng zum Churchhostel und gehört deshalb zum Teil mit zum Model.
 */
public class Marker {

    private double longitude;
    private double latitude;
    private int beds;
    private long churchhostelId;

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getBeds() {
        return beds;
    }

    public void setBeds(int beds) {
        this.beds = beds;
    }

    public long getChurchhostelId() {
        return churchhostelId;
    }

    public void setChurchhostelId(long churchhostelId) {
        this.churchhostelId = churchhostelId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Marker marker = (Marker) o;

        if (Double.compare(marker.longitude, longitude) != 0) return false;
        if (Double.compare(marker.latitude, latitude) != 0) return false;
        if (beds != marker.beds) return false;
        return churchhostelId == marker.churchhostelId;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(longitude);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + beds;
        result = 31 * result + (int) (churchhostelId ^ (churchhostelId >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Marker{" +
                "longitude=" + longitude +
                ", latitude=" + latitude +
                ", beds=" + beds +
                ", churchhostelId=" + churchhostelId +
                '}';
    }
}
