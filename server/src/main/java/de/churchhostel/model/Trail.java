package de.churchhostel.model;

public class Trail {

    private Long id;
    private String name;
    private String fileUrl;
    private String startLocation;
    private String endLocation;
    private Integer length;

    public Trail() {
    }

    public Trail(String name, String fileUrl, String start, String end, int length) {
        this.name = name;
        this.fileUrl = fileUrl;
        this.startLocation = start;
        this.endLocation = end;
        this.length = length;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(String start) {
        this.startLocation = start;
    }

    public String getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(String end) {
        this.endLocation = end;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endLocation == null) ? 0 : endLocation.hashCode());
		result = prime * result + ((fileUrl == null) ? 0 : fileUrl.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + length;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((startLocation == null) ? 0 : startLocation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trail other = (Trail) obj;
		if (endLocation == null) {
			if (other.endLocation != null)
				return false;
		} else if (!endLocation.equals(other.endLocation))
			return false;
		if (fileUrl == null) {
			if (other.fileUrl != null)
				return false;
		} else if (!fileUrl.equals(other.fileUrl))
			return false;
		if (id != other.id)
			return false;
		if (length != other.length)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (startLocation == null) {
			if (other.startLocation != null)
				return false;
		} else if (!startLocation.equals(other.startLocation))
			return false;
		return true;
	}
    
    
}
