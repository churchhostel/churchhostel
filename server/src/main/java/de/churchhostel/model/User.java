package de.churchhostel.model;

public class User {

    private Long id;
    private String name;
    private String pwHash;

    public User(Long id, String name, String pwHash) {
        this.id = id;
        this.name = name;
        this.pwHash = pwHash;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwHash() {
        return pwHash;
    }

    public void setPwHash(String pwHash) {
        this.pwHash = pwHash;
    }
}
