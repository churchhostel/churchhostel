package de.churchhostel.model;

import java.util.Objects;

public class Image {

    private Long id;
    private String fileUrl;
    private Long churchhostelId;
    
    public Image() {
    	
    }

    public Image(String fileUrl, long hostelId) {
    	this.fileUrl = fileUrl;
    	this.churchhostelId = hostelId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public Long getChurchhostelId() {
        return churchhostelId;
    }

    public void setChurchhostelId(Long churchhostelId) {
        this.churchhostelId = churchhostelId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Image image = (Image) o;
        return Objects.equals(id, image.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Image{" +
                "id=" + id +
                ", fileUrl='" + fileUrl + '\'' +
                ", churchhostelId=" + churchhostelId +
                '}';
    }
}
