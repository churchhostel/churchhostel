package de.churchhostel.model;

public class Contact {

	private Long id;
	private String name;
	private String phone;
	private String email;
	private Long churchhostelId;

	public Contact() {
	}

	public Contact(String name, String phone) {
		this.name = name;
		this.phone = phone;
	}

	public Contact(String name, String phone, String email) {
		this.name = name;
		this.phone = phone;
		this.email = email;
	}

	public Contact(String name, String phone, String email, Long churchhostelId) {
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.churchhostelId = churchhostelId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getChurchhostelId() {
		return churchhostelId;
	}

	public void setChurchhostelId(Long churchhostelId) {
		this.churchhostelId = churchhostelId;
	}

	public void setEmail(String newMail) {
		this.email = newMail;
	}

	public String getEmail() {
		return email;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Contact contact = (Contact) o;

		return id == contact.id;
	}

	@Override
	public int hashCode() {
		return (int) (id ^ (id >>> 32));
	}

	@Override
	public String toString() {
		return "Contact{" + "id=" + id + ", name='" + name + '\'' + ", phone='" + phone + '\'' + ", churchhostelId=" + churchhostelId + '}';
	}
}
