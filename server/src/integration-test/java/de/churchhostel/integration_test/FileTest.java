package de.churchhostel.integration_test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import de.churchhostel.ServerConfiguration;
import de.churchhostel.service.ImageFileService;
import de.churchhostel.service.TrailFileService;
import org.junit.*;
import org.junit.rules.TemporaryFolder;

import javax.ws.rs.core.Response;
import static org.junit.Assert.*;

public class FileTest extends IntegrationTest {

    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();
    private static Path trailFilesDir;
    private static Path imageFilesDir;

    @Before
    public void setFilesDir() throws Exception {
        String filesDir = testFolder.newFolder("ch").getAbsolutePath();
        ServerConfiguration.setFilesDir(filesDir);
        trailFilesDir = Paths.get(filesDir, "trails");
        imageFilesDir = Paths.get(filesDir, "images");
        // Initialize creation of directory
        new TrailFileService();
        new ImageFileService();
    }

    @Test
    public void getTrailFile_fileDoesNotExist_404Returned() throws IOException {
        Response response = GET("files/trail/nonexisting.kml", "application/vnd.google-earth.kml+xml");
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

	@Test
	public void getTrailFile_fileExists_fileReturned() throws IOException {
		// place a file
		Path trailFile = trailFilesDir.resolve("mytrail.kml");
		Files.write(trailFile, "hello".getBytes(), StandardOpenOption.CREATE);

		Response response = GET("files/trail/mytrail.kml", "application/vnd.google-earth.kml+xml");
		String responseEntity = response.readEntity(String.class);
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		assertEquals("hello", responseEntity);
	}
	
}

