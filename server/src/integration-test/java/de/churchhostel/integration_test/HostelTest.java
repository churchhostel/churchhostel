package de.churchhostel.integration_test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import de.churchhostel.model.Churchhostel;
import de.churchhostel.model.Contact;
import de.churchhostel.repository.ChurchhostelRepository;

public class HostelTest extends IntegrationTest {

    private static final Logger log = LogManager.getLogger(HostelTest.class);

	private ChurchhostelRepository chRepo = new ChurchhostelRepository();
	private Churchhostel testHostel1 = new Churchhostel.Builder()
										.withLatitude(50.000000)
										.withLongitude(10.000000)
										.withName("Coburg Hostel")
										.withAddress("Gasse der Gläubigen")
										.withBeds(5)
										.withEmail("contact@testhostel.com")
										.withFacilities(Arrays.asList("Badezimmer"))
										.withServices(Arrays.asList("Frühstück"))
										.withAdditionalHints(Arrays.asList("Keine Einkaufsmöglichkeiten in der Nähe"))
										.withPrice(BigDecimal.valueOf(14.99))
										.build();
	private Churchhostel testHostel2 = new Churchhostel.Builder()
										.withLatitude(51.111111)
										.withLongitude(11.111111)
										.withName("Anderes Hostel")
										.withAddress("Andere Gasse")
										.withBeds(5)
										.withEmail("contact@testhostel2.com")
										.withFacilities(Arrays.asList("Fernsehzimmer"))
										.withServices(Arrays.asList("Freibier"))
										.withAdditionalHints(new ArrayList<>())
										.withPrice(BigDecimal.valueOf(10.99))
										.build();

	@Test
	public void testGetAll() {
		Churchhostel persistedHostel1 = chRepo.create(testHostel1);
		Churchhostel persistedHostel2 = chRepo.create(testHostel2);
		JsonObject hostel1 = Json.createObjectBuilder()
				.add("id", persistedHostel1.getId())
				.add("latitude", 50.0)
				.add("longitude", 10.0)
				.add("name", "Coburg Hostel")
				.add("address", "Gasse der Gläubigen")
				.add("beds", 5)
				.add("email", "contact@testhostel.com")
				.add("facilities", Json.createArrayBuilder().add("Badezimmer").build())
				.add("services", Json.createArrayBuilder().add("Frühstück").build())
				.add("additionalHints",Json.createArrayBuilder().add("Keine Einkaufsmöglichkeiten in der Nähe").build())
				.add("price", 14.99)
				.add("images", Json.createArrayBuilder().build())
				.add("contacts", Json.createArrayBuilder().build())
				.build();
		JsonObject hostel2 = Json.createObjectBuilder()
				.add("id", persistedHostel2.getId())
				.add("latitude", 51.111111)
				.add("longitude", 11.111111)
				.add("name", "Anderes Hostel")
				.add("address", "Andere Gasse")
				.add("beds", 5)
				.add("email", "contact@testhostel2.com")
				.add("facilities", Json.createArrayBuilder().add("Fernsehzimmer").build())
				.add("services", Json.createArrayBuilder().add("Freibier").build())
				.add("additionalHints", Json.createArrayBuilder().build())
				.add("price", 10.99)
				.add("images", Json.createArrayBuilder().build())
				.add("contacts", Json.createArrayBuilder().build())
				.build();

		String response = GET("/hostel").readEntity(String.class);
		String expected = Json.createArrayBuilder().add(hostel1).add(hostel2).build().toString();

		Assert.assertEquals(expected, response);
	}

    @Test
    public void testGetAll_deactivatedHostelIsNotIncluded() {
        Churchhostel persistedHostel1 = chRepo.create(testHostel1);
        Churchhostel persistedHostel2 = chRepo.create(testHostel2);
        chRepo.delete(persistedHostel2.getId());
        JsonObject hostel1 = Json.createObjectBuilder()
                .add("id", persistedHostel1.getId())
                .add("latitude", 50.0)
                .add("longitude", 10.0)
                .add("name", "Coburg Hostel")
                .add("address", "Gasse der Gläubigen")
				.add("beds", 5)
				.add("email", "contact@testhostel.com")
                .add("facilities", Json.createArrayBuilder().add("Badezimmer").build())
                .add("services", Json.createArrayBuilder().add("Frühstück").build())
                .add("additionalHints",Json.createArrayBuilder().add("Keine Einkaufsmöglichkeiten in der Nähe").build())
                .add("price", 14.99)
                .add("images", Json.createArrayBuilder().build())
                .add("contacts", Json.createArrayBuilder().build())
                .build();

        String response = GET("/hostel").readEntity(String.class);
        String expected = Json.createArrayBuilder().add(hostel1).build().toString();

        Assert.assertEquals(expected, response);
    }

	@Test
	public void testGetOne() {
		Churchhostel persistedHostel = new ChurchhostelRepository().create(testHostel2);
		Response response = GET("/hostel/" + persistedHostel.getId());
		String expected = Json.createObjectBuilder()
				.add("id", persistedHostel.getId())
				.add("latitude", 51.111111)
				.add("longitude", 11.111111)
				.add("name", "Anderes Hostel")
				.add("address", "Andere Gasse")
				.add("beds", 5)
				.add("email", "contact@testhostel2.com")
				.add("facilities", Json.createArrayBuilder().add("Fernsehzimmer").build())
				.add("services", Json.createArrayBuilder().add("Freibier").build())
				.add("additionalHints", Json.createArrayBuilder().build())
				.add("price", 10.99)
				.add("images", Json.createArrayBuilder().build())
				.add("contacts", Json.createArrayBuilder().build())
				.build().toString();

		Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		Assert.assertEquals(expected, response.readEntity(String.class));
	}

    @Test
    public void testGetOne_contactShouldBeIncluded() {
        Churchhostel persistedHostel = chRepo.create(testHostel2);
        Contact persistedContact = contactRepo.create(new Contact("Hannes", "12345", "contact@hannes.com", persistedHostel.getId()));
        Response response = GET("/hostel/" + persistedHostel.getId());
        String expected = Json.createObjectBuilder()
                .add("id", persistedHostel.getId())
                .add("latitude", 51.111111)
                .add("longitude", 11.111111)
                .add("name", "Anderes Hostel")
                .add("address", "Andere Gasse")
				.add("beds", 5)
				.add("email", "contact@testhostel2.com")
                .add("facilities", Json.createArrayBuilder().add("Fernsehzimmer").build())
                .add("services", Json.createArrayBuilder().add("Freibier").build())
                .add("additionalHints", Json.createArrayBuilder().build())
                .add("price", 10.99)
                .add("images", Json.createArrayBuilder().build())
                .add("contacts", Json.createArrayBuilder().add(persistedContact.getId()).build())
                .build().toString();

        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        Assert.assertEquals(expected, response.readEntity(String.class));
    }

    @Test
    public void testUpdateHostelName() {
        Churchhostel persistedHostel = chRepo.create(testHostel2);
        JsonObject updateJson = Json.createObjectBuilder()
                .add("name", "Updated Name").build();
        Response response = PUT("/hostel/" + persistedHostel.getId(), updateJson.toString());

        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        Assert.assertEquals("Updated Name", chRepo.find(persistedHostel.getId()).get().getName());
    }

    @Test
    public void testUpdateHostelServices() {
        Churchhostel persistedHostel = chRepo.create(testHostel2);
        JsonObject updateJson = Json.createObjectBuilder()
                .add("services", Json.createArrayBuilder().add("Updated Facility 1").add("Updated Facility 2")).build();
        Response response = PUT("/hostel/" + persistedHostel.getId(), updateJson.toString());

        Churchhostel retrievedHostel = chRepo.find(persistedHostel.getId()).get();
        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        Assert.assertTrue(retrievedHostel.getServices().contains("Updated Facility 1"));
        Assert.assertTrue(retrievedHostel.getServices().contains("Updated Facility 2"));
    }

	@Test
	public void testGetOne_idIsNotFound_404Returned() {
		Response response = GET("/hostel/9999");
		Assert.assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreate_entityIsSuccessfullyCreated_CREATEDreturned() {
		JsonObject postJson = Json.createObjectBuilder()
			.add("latitude", 22.222222)
			.add("longitude", 33.333333)
			.add("name", "My Test Hostel")
			.add("address", "My Test Address")
			.add("beds", 3)
			.add("facilities", Json.createArrayBuilder().add("Test facility 1").add("Test facility 2").build())
			.add("services", Json.createArrayBuilder().add("Test Service 1").build())
			.add("additionalHints", Json.createArrayBuilder().add("Hint 1").add("Hint 2").build())
			.add("price", 14.99)
			.build();

		Response response = POST("/hostel", postJson.toString());
		JsonObject responseObject = string2Object(response.readEntity(String.class));
		log.info("CREATE hostel response text: " + responseObject.toString());

		Churchhostel ch = chRepo.find(responseObject.getJsonNumber("id").longValue()).get();

		Assert.assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		Assert.assertEquals("My Test Hostel", ch.getName());
	}

	@Test
	public void testCreate_noBodyPosted_400Returned() {
		Response response = POST("/hostel", null);
		Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreate_informationInBodyIsMissing_400Returned() {
		JsonObject postJsonWithMissingData = Json.createObjectBuilder()
				.add("latitude", 22.222222)
				.add("longitude", 33.333333)
				.add("name", "My Test Hostel")
				.build();
		Response response = POST("/hostel", postJsonWithMissingData.toString());

		Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

    @Test
    public void testDelete_hostelIsDeactivated() {
	    Churchhostel persistedHostel = chRepo.create(testHostel1);
	    Response response = DELETE("/hostel/" + persistedHostel.getId());
	    Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	    Assert.assertFalse(chRepo.find(persistedHostel.getId()).get().getActive());
    }

}
