package de.churchhostel.integration_test;

import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;

import de.churchhostel.repository.ChurchhostelRepository;
import de.churchhostel.repository.ContactRepository;
import de.churchhostel.repository.ImageRepository;
import de.churchhostel.repository.TrailRepository;

public class IntegrationTest {

    private static final Logger log = LogManager.getLogger(IntegrationTest.class);

	protected static final ChurchhostelRepository chRepo = new ChurchhostelRepository();
	protected static final TrailRepository trailRepo = new TrailRepository();
	protected static final ContactRepository contactRepo = new ContactRepository();
	protected static final ImageRepository imageRepo = new ImageRepository();
	protected static final Client client = ClientBuilder.newClient();

	private final static String LOGIN_USER = "test";
	private final static String LOGIN_PW = "test";

    public static String authToken;

    @BeforeClass
    public static void acquireAuthToken() {
		log.info("Attempting to acquire new auth token");
		JsonObject credentialsObject = Json.createObjectBuilder().add("username", LOGIN_USER).add("password", LOGIN_PW).build();
		WebTarget target = client.target("http://localhost:8080/api/").path("login");
		Builder invocationBuilder = target.request(MediaType.APPLICATION_JSON);
		Response authenticationResponse = invocationBuilder.post(Entity.entity(credentialsObject.toString(), MediaType.APPLICATION_JSON));
		authToken = authenticationResponse.getHeaderString("Authorization");
		if (authToken == null) {
			log.error("Could not acquire auth token from response " + authenticationResponse);
			log.error("Response is " + authenticationResponse.readEntity(String.class));
		} else {
			log.info("Successfully acquired new auth token for test");
		}
	}
	
	@Before
	public void setup() {
		System.out.println("Running test with auth token " + authToken);
		log.fatal("Running test with auth token " + authToken);
	}

	@After
	public void cleanTestData() {
		chRepo.deleteAll();
		contactRepo.deleteAll();
        trailRepo.deleteAll();
        imageRepo.deleteAll();
	}

	protected Response GET(String path) {
	    return GET(path, MediaType.APPLICATION_JSON);
	}
	protected Response GET(String path, String expectedResponseType) {
		WebTarget target = client.target("http://localhost:8080/api/").path(path);
		Builder invocationBuilder = target.request(expectedResponseType);
		return invocationBuilder.get();
	}
	protected Response POST(String path, Object entity) {
		WebTarget target = client.target("http://localhost:8080/api/").path(path);
		Builder invocationBuilder = target.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("Authorization", authToken);
		return invocationBuilder.post(Entity.entity(entity, MediaType.APPLICATION_JSON));
	}
    protected Response PUT(String path, Object entity) {
        WebTarget target = client.target("http://localhost:8080/api/").path(path);
        Builder invocationBuilder = target.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("Authorization", authToken);
        return invocationBuilder.put(Entity.entity(entity, MediaType.APPLICATION_JSON));
    }
    protected Response DELETE(String path) {
        WebTarget target = client.target("http://localhost:8080/api/").path(path);
        Builder invocationBuilder = target.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("Authorization", authToken);
        return invocationBuilder.delete();
    }
	protected JsonObject string2Object(String s) {
		JsonReader reader = Json.createReader(new StringReader(s));
		JsonObject obj = reader.readObject();
		reader.close();
		return obj;
	}
    protected JsonArray string2Array(String s) {
        JsonReader reader = Json.createReader(new StringReader(s));
        JsonArray obj = reader.readArray();
        reader.close();
        return obj;
	}

	public void main(String[] args) {
		JsonObject credentialsObject = Json.createObjectBuilder().add("username", LOGIN_USER).add("password", LOGIN_PW).build();
		WebTarget target = client.target("http://localhost:8080/api/").path("login");
		Builder invocationBuilder = target.request(MediaType.APPLICATION_JSON);
		Response authenticationResponse = invocationBuilder.post(Entity.entity(credentialsObject.toString(), MediaType.APPLICATION_JSON));
		authToken = authenticationResponse.getHeaderString("Authorization");
		System.out.println(authToken);
		System.out.println(authenticationResponse);
	}
}
