package de.churchhostel.integration_test;

import java.beans.PropertyVetoException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.testcontainers.containers.GenericContainer;

import de.churchhostel.ServerConfiguration;
import de.churchhostel.repository.database.ConnectionPool;

@RunWith(Suite.class)
@Suite.SuiteClasses({HostelTest.class, MarkerTest.class, ContactTest.class, TrailTest.class, ImageTest.class, FileTest.class
})
public class IntegrationTestSuite {

    private static final Logger log = LogManager.getLogger(IntegrationTestSuite.class);

    @Rule
    public static GenericContainer mysql = new GenericContainer("ch/database:latest").withExposedPorts(3306);

    public static Server server;

    @BeforeClass
    public static void setup() throws PropertyVetoException {
        log.info("Setting up test database...");
        mysql.start();

        // Set database connection properties for test
        Map<String, String> persistenceConfig = new HashMap<>();
        persistenceConfig.put("url", "jdbc:mysql://127.0.0.1:"+mysql.getMappedPort(3306) +"/ch");
        persistenceConfig.put("user", "root");
        persistenceConfig.put("password", "root");
        log.info(String.format("Test database container:\n\tContainer ID: %s\n\tJDBC URL: %s\n\tJDBC User: %s\n\tJDBC Password: %s",
                mysql.getContainerId(), persistenceConfig.get("url"), persistenceConfig.get("user"), persistenceConfig.get("password")));
        ConnectionPool.getInstance().setDatasourceProperties(persistenceConfig);
        
        // Set a JWT signing key for test
        ServerConfiguration.setJWTSigningKey("testkey");

        ServletContextHandler context =
                new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
        context.setContextPath("/");

        server = new Server(8080);
        server.setHandler(context);

        final ResourceConfig resourceConfig = new ResourceConfig();
        resourceConfig.packages("de.churchhostel.rest");
        resourceConfig.register(MultiPartFeature.class);

        ServletHolder jerseyServlet = new ServletHolder(new ServletContainer(resourceConfig));
        jerseyServlet.setInitOrder(1);
        context.addServlet(jerseyServlet, "/api/*");

        try {
            server.start();
        } catch (Exception e) {
            LogManager.getLogger(IntegrationTestSuite.class).error(e);
        }
    }

    @AfterClass
    public static void teardown() {
        System.out.println("Stopping server");
        try {
            server.stop();
        } catch (Exception e) {
            LogManager.getLogger(IntegrationTestSuite.class).error(e);
        } finally {
            server.destroy();
        }
        mysql.stop();
    }

}
