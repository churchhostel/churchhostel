package de.churchhostel.integration_test;

import java.math.BigDecimal;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import de.churchhostel.model.Churchhostel;
import de.churchhostel.model.Contact;

public class ContactTest extends IntegrationTest {

    private static final Logger log = LogManager.getLogger(ContactTest.class);
	private Churchhostel testHostel1 = new Churchhostel(50.000001, 10.000001, "Coburg Hostel", "Gasse der Gläubigen", 5, "info@hostel1.com", 
			new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), BigDecimal.valueOf(14.99));
    private Churchhostel testHostel2 = new Churchhostel(50.000001, 10.000001, "Anderes Hostel", "Gasse der Gläubigen", 5, "info@hostel2.com",
            new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), BigDecimal.valueOf(14.99));

	@Test
	public void testGetContact() {
        Churchhostel persistedHostel1 = chRepo.create(testHostel1);
        Contact persistedContact = contactRepo.create(new Contact("Hannes", "12345", "hannes@example.com", persistedHostel1.getId()));
		// GET marker
		JsonObject contact = Json.createObjectBuilder()
                .add("id", persistedContact.getId())
				.add("name", "Hannes")
				.add("phone", "12345")
				.add("email", "hannes@example.com")
				.add("churchhostelId", persistedHostel1.getId())
				.build();
		String expected = contact.toString();

		Response response = GET("/contact/" + persistedContact.getId());
		String responseText = response.readEntity(String.class);

		Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		Assert.assertEquals(expected, responseText);
	}

	@Test
    public void testCreateContact() {
        Churchhostel persistedHostel1 = chRepo.create(testHostel1);
        JsonObject createContactJson = Json.createObjectBuilder()
                .add("name", "Hannes")
                .add("phone", "01235647122")
                .add("churchhostelId", persistedHostel1.getId()).build();
        Response response = POST("/contact", createContactJson.toString());
        String responseText = response.readEntity(String.class);
        JsonObject responseObject = string2Object(responseText);

        Assert.assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
        Assert.assertEquals("Hannes", contactRepo.find(responseObject.getJsonNumber("id").longValue()).get().getName());
    }

    @Test
    public void testUpdateContactName() {
        Churchhostel persistedHostel1 = chRepo.create(testHostel1);
        Contact persistedContact = contactRepo.create(new Contact("Hannes", "12345", "hannes@example.com", persistedHostel1.getId()));

        JsonObject updateContactJson = Json.createObjectBuilder()
                .add("name", "Peter").build();
        Response response = PUT("/contact/" + persistedContact.getId(), updateContactJson.toString());

        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        Assert.assertEquals("Peter", contactRepo.find(persistedContact.getId()).get().getName());
    }

    @Test
    public void testUpdateContactHostel() {
        Churchhostel persistedHostel1 = chRepo.create(testHostel1);
        Churchhostel persistedHostel2 = chRepo.create(testHostel2);
        Contact persistedContact = contactRepo.create(new Contact("Hannes", "12345", "hannes@example.com", persistedHostel1.getId()));

        JsonObject updateContactJson = Json.createObjectBuilder()
                .add("churchhostelId", persistedHostel2.getId()).build();
        Response response = PUT("/contact/" + persistedContact.getId(), updateContactJson.toString());

        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        Assert.assertEquals(persistedHostel2.getId(), contactRepo.find(persistedContact.getId()).get().getChurchhostelId());
    }

    @Test
    public void testDeleteContact() {
        Churchhostel persistedHostel1 = chRepo.create(testHostel1);
        Contact persistedContact = contactRepo.create(new Contact("Hannes", "12345", "hannes@example.com", persistedHostel1.getId()));

        DELETE("/contact/" + persistedContact.getId());

        Assert.assertFalse(contactRepo.find(persistedContact.getId()).isPresent());
    }
}
