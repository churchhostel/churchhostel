package de.churchhostel.integration_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Arrays;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import de.churchhostel.model.Churchhostel;
import de.churchhostel.model.Image;

public class ImageTest extends IntegrationTest {

    private static final Logger log = LogManager.getLogger(ImageTest.class);

    private Churchhostel testHostel1 = new Churchhostel(50.000000, 10.000000, "Coburg Hostel", "Gasse der Gläubigen", 5, "contact@hostel.com",
			Arrays.asList("Badezimmer"), Arrays.asList("Frühstück"),
			Arrays.asList("Keine Einkaufsmöglichkeiten in der Nühe"), BigDecimal.valueOf(14.99));

    @Test
    public void testImageIsIncludedInHostelImages() {
        Churchhostel persistedHostel = chRepo.create(testHostel1);
        Image persistedImage = imageRepo.create(new Image("http://images.com/myImage", persistedHostel.getId()));

        assertTrue(imageRepo.findHostelImages(persistedHostel.getId()).contains(persistedImage));
    }
	@Test
	public void getHostel_imagesURLsAreIncluded() {
	    Churchhostel persistedHostel = chRepo.create(testHostel1);
	    Image persistedImage = imageRepo.create(new Image("http://images.com/myImage", persistedHostel.getId()));

		Response response = GET("/hostel/" + persistedHostel.getId());
        JsonObject responseObject = string2Object(response.readEntity(String.class));
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		assertEquals("http://images.com/myImage", responseObject.getJsonArray("images").getJsonObject(0).getString("fileUrl"));
	}

	@Test
	public void testCreate_imageIsSuccessfullyCreated() {
        Churchhostel persistedHostel = chRepo.create(testHostel1);
		JsonObject postJson = Json.createObjectBuilder()
            .add("fileUrl", "http://newimage.com/image")
			.add("churchhostelId", persistedHostel.getId())
			.build();

		Response response = POST("/image", postJson.toString());
        JsonObject responseObject = string2Object(response.readEntity(String.class));

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		assertTrue(imageRepo.exists(responseObject.getJsonNumber("id").longValue()));
		assertEquals("http://newimage.com/image", imageRepo.find(responseObject.getJsonNumber("id").longValue()).get().getFileUrl());
	}

    @Test
    public void testDeleteImage() {
        Churchhostel persistedHostel = chRepo.create(testHostel1);
        Image persistedImage = imageRepo.create(new Image("http://images.com/myImage", persistedHostel.getId()));

        Response response = DELETE("/image/" + persistedImage.getId());

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertFalse(imageRepo.exists(persistedImage.getId()));
        assertTrue(imageRepo.findHostelImages(persistedHostel.getId()).isEmpty());
    }
}
