package de.churchhostel.integration_test;

import de.churchhostel.model.Churchhostel;
import de.churchhostel.repository.ChurchhostelRepository;
import org.junit.Assert;
import org.junit.Test;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

public class MarkerTest extends IntegrationTest {

	ChurchhostelRepository chRepo = new ChurchhostelRepository();
	private Churchhostel testHostel1 = new Churchhostel(50.000001, 10.000001, "Coburg Hostel", "Gasse der Gläubigen", 5, "contact@hostel.com",
			Arrays.asList("Badezimmer"), Arrays.asList("Frühstück"),
			Arrays.asList("Keine Einkaufsmöglichkeiten in der Nähe"), BigDecimal.valueOf(14.99));
	private Churchhostel testHostel2 = new Churchhostel(51.111111, 11.111111, "Anderes Hostel", "Andere Gasse", 5, "contact@hostel.com", 
			Arrays.asList("Fernsehzimmer"), Arrays.asList("Freibier"), new ArrayList<>(), BigDecimal.valueOf(10.99));

	@Test
	public void testGetMarker() {
        Churchhostel persistedHostel1 = chRepo.create(testHostel1);
        Churchhostel persistedHostel2 = chRepo.create(testHostel2);
		// GET marker
		JsonObject marker1 = Json.createObjectBuilder()
				.add("longitude", 10.000001)
				.add("latitude", 50.000001)
				.add("beds", 5)
				.add("churchhostelId", persistedHostel1.getId())
				.build();
		JsonObject marker2 = Json.createObjectBuilder()
				.add("longitude", 11.111111)
				.add("latitude", 51.111111)
				.add("beds", 5)
				.add("churchhostelId", persistedHostel2.getId())
				.build();
		String expected = Json.createArrayBuilder().add(marker1).add(marker2).build().toString();

		Response response = GET("/marker");
		String responseText = response.readEntity(String.class);

		Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		Assert.assertEquals(expected, responseText);
	}

    @Test
    public void testGetMarker_deactivatedHostelsNotIncluded() {
        Churchhostel persistedHostel1 = chRepo.create(testHostel1);
        Churchhostel persistedHostel2 = chRepo.create(testHostel2);
        chRepo.delete(persistedHostel2.getId());
        // GET marker
        JsonObject marker1 = Json.createObjectBuilder()
                .add("longitude", 10.000001)
                .add("latitude", 50.000001)
                .add("beds", 5)
                .add("churchhostelId", persistedHostel1.getId())
                .build();
        String expected = Json.createArrayBuilder().add(marker1).build().toString();

        Response response = GET("/marker");
        String responseText = response.readEntity(String.class);

        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        Assert.assertEquals(expected, responseText);
    }

}
