package de.churchhostel.integration_test;

import de.churchhostel.model.Trail;
import org.junit.Ignore;
import org.junit.Test;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class TrailTest extends IntegrationTest {

	protected Trail testTrail = new Trail("test trail", "http://testtrail", "teststart", "testend", 100);
    protected Trail testTrail2 = new Trail("test trail 2", "http://testtrail2", "teststart2", "testend2", 100);

	@Test
	public void getTrail_trailExists_trailIsReturned() {
	    Trail persistedTrail = trailRepo.create(testTrail) ;
		Response response = GET("/trail/" + persistedTrail.getId());
        JsonObject responseObject = string2Object(response.readEntity(String.class));

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		assertEquals("test trail", responseObject.getString("name"));
	}

    @Test
    public void getTrail_trailDoesNotExist_404Returned() {
	    Response response = GET("/trail/9999");
	    assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void getAllTrails() {
	    trailRepo.create(testTrail) ;
        trailRepo.create(testTrail2) ;

        Response response = GET("/trail");
        JsonArray responseArray = string2Array(response.readEntity(String.class));
        List<String> responseTrailNames = responseArray.stream().map(jsonValue -> jsonValue.asJsonObject().getString("name")).collect(Collectors.toList());

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertTrue(responseTrailNames.contains("test trail"));
        assertTrue(responseTrailNames.contains("test trail 2"));
    }

	@Test
	public void testCreate_entityIsSuccessfullyCreated() {
		JsonObject postJson = Json.createObjectBuilder()
			.add("name", "Test Trail")
            .add("fileUrl", "http://myurl.com/trail")
			.add("startLocation", "Berlin")
			.add("endLocation", "Budapest")
			.add("length", "100")
			.build();

		Response response = POST("/trail", postJson.toString());
        JsonObject responseObject = string2Object(response.readEntity(String.class));

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		assertEquals("http://myurl.com/trail", trailRepo.find(responseObject.getJsonNumber("id").longValue()).get().getFileUrl());
	}

	@Test
    public void testUpdate_updateTrailName() {
        Trail persistedTrail = trailRepo.create(testTrail) ;
        JsonObject updateNameJson = Json.createObjectBuilder()
                .add("name", "New trail name")
                .build();

        Response response = PUT("/trail/" + persistedTrail.getId(), updateNameJson.toString());

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals("New trail name", trailRepo.find(persistedTrail.getId()).get().getName());
    }

    @Test
    public void testUpdate_updateTrailFileUrl() {
        Trail persistedTrail = trailRepo.create(testTrail) ;
        JsonObject updateNameJson = Json.createObjectBuilder()
                .add("fileUrl", "http://new.de/trail")
                .build();

        Response response = PUT("/trail/" + persistedTrail.getId(), updateNameJson.toString());

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals("http://new.de/trail", trailRepo.find(persistedTrail.getId()).get().getFileUrl());
    }

    @Test
    public void testDeleteTrail() {
        Trail persistedTrail = trailRepo.create(testTrail);
        Response response = DELETE("/trail/" + persistedTrail.getId());

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertFalse(trailRepo.exists(persistedTrail.getId()));
        assertFalse(trailRepo.find(persistedTrail.getId()).isPresent());
    }
}
