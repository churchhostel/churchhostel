USE `ch`;

START TRANSACTION;

INSERT INTO `churchhostel` VALUES
    (8,'Am Coburger Wege 9000',1,50.14567,11.05928,'Coburg Hostel',5.99, 1),
    (10,'Am Sonneberger Weg',12,50.3592,11.17463,'Sonneberg Hostel',10.99, 1),
    (31,'Ottowinder Str. 5',6,50.3334,10.901,'Gemeinde Meeder',14.99, 1),
    (27,'Kronacher Weg 55',3,50.23963,11.33308,' Kronach Unterkunft',10.99, 1);

INSERT INTO `additional_hint` VALUES
    (1, 10,'Keine Hunde erlaubt'),
    (2, 8,'Anreise bitte zwischen 16 und 20 Uhr.'),
    (3, 27,'Hunde sind leider nicht erwünscht.'),
    (4, 31,'Am Sonntag werden die Wanderer gebeten dem Gottesdienst beizuwohnen');

INSERT INTO `contact` VALUES
    (9,'Maus Hanser','+49152-12345',8),
    (11,'Herr Petersen','08612465123',10),
    (19,'Theophilus Fromm','018152623',8),
    (18,'Rainer Wahnsinn','+4915626734165',8),
    (28,'Kathrin Dresdner','002341273',27),
    (29,'Karolin Lange','56123516233',27),
    (30,'Bernd Lemann','025128365',27),
    (32,'Herr Olsen','1052457315',31);

INSERT INTO `facility` VALUES
    (1, 8,'Fernsehzimmer'),
    (2, 8,'Duschen'),
    (3, 10,'Swimmingpool'),
    (4, 27,'Betten vorhanden'),
    (5, 31,'Isomatten vorhanden'),
    (6, 10,'Fernsehzimmer mit großer Filmsammlung'),
    (7, 31,'Fernseher');

INSERT INTO `image` VALUES
    (20,'8/bla',8),
    (24,'8/blub',8);

INSERT INTO `service` VALUES
    (1, 8,'Weckservice'),
    (2, 10,'Wir begrüßen euch herzlich!'),
    (3, 8,'Mittagessen auf Wunsch und gegen Bezahlung'),
    (4, 27,'Brotzeit am Abend'),
    (5, 31,'Frühstück von 8-10'),
    (6, 31,'Auf Wunsch Abholung in einem 10 km Umkreis');

INSERT INTO `trail` VALUES
    (1,'trails/Neuer_Trail',123,'Coolest Trail','StartPunkt','StopPunkt'),
    (2,'trails/Different_Trail',100,'Different Trail','Berlin','München');

COMMIT;
