package de.churchhostel.util;

import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.Test;

public class EncryptionTest {

    @Test
    public void testSha256Base64() throws EncryptionException {
        final String myPassword = "My&Password1!";
        final String expected = "FUaM4wsz3zu5TyOnzmewruft4Faa/cyk3kT5RQwUXno=";
        Assert.assertEquals(expected, Encryption.sha256base64(myPassword));
    }

    @Test
    public void testPattern() {
        Assert.assertTrue("+152 3512".matches("^[\\+]?[\\d ]+$"));
    }
}
