package de.churchhostel.service;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Arrays;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import de.churchhostel.model.Churchhostel;
import de.churchhostel.model.Contact;
import de.churchhostel.repository.ChurchhostelRepository;
import de.churchhostel.repository.ContactRepository;
import de.churchhostel.service.exception.ContactNotFoundException;
import de.churchhostel.service.exception.HostelNotFoundException;

@Ignore
public class TransactionManagedContactServiceTest {

//	@Mock
//	private EntityManagerFactory emf;
//	@Mock
//	private EntityManager em;
//	@Mock
//	private ChurchhostelRepository chRepo;
//	@Mock
//	private ContactRepository contactRepo;
//
//	private ContactService sut;
//
//	@Before
//	public void setup() {
//		MockitoAnnotations.initMocks(this);
//		when(emf.createEntityManager()).thenReturn(em);
//		sut = new TransactionManagedContactService(emf, chRepo, contactRepo);
//	}
//
//
//	@Test
//	public void testGetContact_validContactIsReturned() throws Exception {
//		Contact myTestContact = new Contact("Test Name", "Test Number", null);
//		when(contactRepo.find(1, em)).thenReturn(myTestContact);
//
//		ContactGET result = sut.getContact(1, new ContactGETConverter("http://baseuri/"));
//
//		Assert.assertEquals("Test Name", result.getName());
//		Assert.assertEquals("Test Number", result.getNumber());
//	}
//
//	@Test(expected = ContactNotFoundException.class)
//	public void testGetContact_contactIsNotFound_contactNotFoundExceptionThrown() throws Exception {
//		when(contactRepo.find(1, em)).thenReturn(null);
//
//		sut.getContact(1,new ContactGETConverter("http://baseuri/"));
//	}
//
//	@Test
//	public void testAddContact_hostelExists_contactIsAdded() throws Exception {
//		Churchhostel ch = new Churchhostel(10.111111, 20.222222, "Test Name", "Test Address", 1, Arrays.asList("Test facility"), Arrays.asList("Test Service"), Arrays.asList("Test hint"), BigDecimal.ONE);
//		when(chRepo.find(1, em)).thenReturn(ch);
//
//		ContactPOST contactPostObject = new ContactPOST("Contact Name", "Contact Number", 1l);
//		sut.addContact(contactPostObject);
//
//		Contact expected = new Contact("Contact Name", "Contact Number", ch);
//		verify(chRepo).find(1, em);
//		verify(contactRepo).save(expected, em);
//	}
//
//	@Test(expected = HostelNotFoundException.class)
//	public void testAddContact_hosteldoesNotExist_exceptionThrown() throws Exception {
//		when(chRepo.find(1, em)).thenReturn(null);
//
//		sut.addContact(new ContactPOST("asd", "dsa", 5l));
//	}

}
