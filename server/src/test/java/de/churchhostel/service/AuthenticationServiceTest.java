package de.churchhostel.service;

import de.churchhostel.model.User;
import de.churchhostel.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import java.util.Optional;

public class AuthenticationServiceTest {

    @Mock
    private UserRepository userRepo;

    private AuthenticationService authService;

    private final String testPassword = "mycoolpassword";
    private final String testPasswordHash = "g10FLZhVF8sfpfxL9EIo+Jj3oiMzg/6vXNI+ov2bKj8=";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(userRepo.findByName("Hannes")).thenReturn(Optional.of(new User(1l, "Hannes", testPasswordHash)));
        authService = new AuthenticationService(userRepo);
    }

    @Test
    public void isAuthenticated_userAndPasswordCorrect_returnsTrue() {
        assertTrue(authService.isAuthenticated("Hannes", "mycoolpassword"));
    }

    @Test
    public void isAuthenticated_userDoesNotExist_returnsFalse() {
        assertFalse(authService.isAuthenticated("Ute Unbekannt", "mycoolpassword"));
    }

    @Test
    public void isAuthenticated_userExistsButPasswordIsIncorrect_returnsFalse() {
        assertFalse(authService.isAuthenticated("Hannes", "thisisdefinitelynotmypassword"));
    }
}
