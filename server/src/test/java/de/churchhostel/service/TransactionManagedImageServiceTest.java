package de.churchhostel.service;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Arrays;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.churchhostel.model.Churchhostel;
import de.churchhostel.model.Image;
import de.churchhostel.repository.ChurchhostelRepository;
import de.churchhostel.repository.ImageRepository;
import de.churchhostel.service.exception.HostelNotFoundException;
import de.churchhostel.service.exception.ImageNotFoundException;

@Ignore
public class TransactionManagedImageServiceTest {

//	@Mock
//	private EntityManagerFactory emf;
//	@Mock
//	private EntityManager em;
//	@Mock
//	private ChurchhostelRepository chRepo;
//	@Mock
//	private ImageRepository imageRepo;
//	@Mock
//	private FileService fileService;
//
//	private ImageService sut;
//
//	@Before
//	public void setup() {
//		MockitoAnnotations.initMocks(this);
//		when(emf.createEntityManager()).thenReturn(em);
//		sut = new TransactionManagedImageService(emf, chRepo, imageRepo, fileService);
//	}
//
//
//	@Test
//	public void testGetImage_validImageIsReturned_withFileUrlReplaced() throws Exception {
//		Image myTestImage = new Image("Image Name", "sub/image_name", null);
//		when(imageRepo.find(1, em)).thenReturn(myTestImage);
//
//		ImageGET result = sut.getImage(1, new ImageGETConverter("http://"));
//
//		Assert.assertEquals("http://file/sub/image_name", result.getFileUrl());
//	}
//
//	@Test(expected = ImageNotFoundException.class)
//	public void testGetImage_ImageIsNotFound_ImageNotFoundExceptionThrown() throws Exception {
//		when(imageRepo.find(1, em)).thenReturn(null);
//
//		sut.getImage(1, new ImageGETConverter("http://image"));
//	}
//
//	@Test
//	public void testAddImage_hostelExists_imageIsAdded() throws Exception {
//		Churchhostel ch = new Churchhostel(10.111111, 20.222222, "Test Name", "Test Address", 1, Arrays.asList("Test facility"), Arrays.asList("Test Service"), Arrays.asList("Test hint"), BigDecimal.ONE);
//		when(chRepo.find(1, em)).thenReturn(ch);
//
//		ImagePOST imagePostObject = new ImagePOST();
//		imagePostObject.setName("MyImage");
//		imagePostObject.setChurchhostelId(1);
//
//		when(fileService.findValidFilename("1", "MyImage")).thenReturn("MyImage");
////		when(fileService.saveFile("1", "MyImage", );).thenReturn(5);
//		ArgumentCaptor<Image> imageArgument = ArgumentCaptor.forClass(Image.class);
//		sut.addImage(imagePostObject);
//
//		Image expected = new Image("MyImage", "1/MyImage", ch);
//		verify(chRepo).find(1, em);
//		verify(imageRepo).save(imageArgument.capture(), Mockito.eq(em));
//		Assert.assertEquals(expected.getName(), imageArgument.getValue().getName());
//		Assert.assertEquals(expected.getFilePath(), imageArgument.getValue().getFilePath());
//	}
//
//	@Test(expected = HostelNotFoundException.class)
//	public void testAddImage_hosteldoesNotExist_exceptionThrown() throws Exception {
//		when(chRepo.find(1, em)).thenReturn(null);
//
//		sut.addImage(new ImagePOST("http://image", 1));
//	}

}
