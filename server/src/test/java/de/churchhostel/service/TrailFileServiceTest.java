package de.churchhostel.service;

import de.churchhostel.service.exception.FileServiceException;
import org.junit.*;
import org.junit.rules.TemporaryFolder;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

import static org.junit.Assert.*;

public class TrailFileServiceTest {

    private static FileService trailFileService;

    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();
    private static Path trailFilesDir;

    @Before
    public void setFilesDir() throws Exception {
        String filesDir = testFolder.newFolder("ch").getAbsolutePath();
        trailFileService = new TrailFileService(Paths.get(filesDir));
        trailFilesDir = Paths.get(filesDir, "trails");
    }

    @Test(expected = FileNotFoundException.class)
    public void getTrailFile_fileDoesNotExist_fileNotFoundExceptionThrown() throws IOException {
        trailFileService.getFile("notExisting");
    }

    @Test
    public void getTrailFile_fileExists_fileReturned() throws IOException {
        // place a file
        Path trailFile = trailFilesDir.resolve("mytrail.kml");
        Files.write(trailFile, "hello".getBytes(), StandardOpenOption.CREATE);

        File f = trailFileService.getFile("mytrail.kml");
        assertEquals("hello", Files.readAllLines(Paths.get(f.getAbsolutePath())).get(0));
    }

	@Test
	public void saveFile_fileIsSavedInTrailsDirectory() throws IOException {
		InputStream input = new ByteArrayInputStream("hello".getBytes());
        String actualFileName = trailFileService.saveFile("mytrail.kml", input);

        Path createdFile = trailFilesDir.resolve(actualFileName);
		Assert.assertTrue(Files.exists(createdFile));
		Assert.assertEquals("hello", Files.readAllLines(createdFile).get(0));
	}

	@Test
	public void saveFile_fileNameAlreadyExists_modifiedFileNameIsGenerated() throws IOException {
		InputStream input = new ByteArrayInputStream("hello".getBytes());
		trailFileService.saveFile("testfile", input);

		input = new ByteArrayInputStream("bye".getBytes());
        String modifiedFileName = trailFileService.saveFile("testfile", input);

        Path createdFile = trailFilesDir.resolve(modifiedFileName);
		Assert.assertTrue(Files.exists(createdFile));
		Assert.assertNotEquals("testfile", modifiedFileName);
		Assert.assertEquals("bye", Files.readAllLines(createdFile).get(0));
	}

	@Test
	public void deleteFile_fileIsDeleted() throws IOException {
		// Prepare a test file
		Path testFilePath = trailFilesDir.resolve("testfile");
		Files.write(testFilePath, Arrays.asList("Hello"), StandardOpenOption.CREATE);

		// File should be present now
		Assert.assertTrue(Files.exists(testFilePath));

		trailFileService.deleteFile("testfile");

		// File should be gone now
		Assert.assertFalse(Files.exists(testFilePath));
	}

	@Test
	public void deleteFile_fileDoesNotExist_nothingHappens() throws FileServiceException {
		trailFileService.deleteFile("nonexisting");
	}

}
