docker stop chdb 2&>/dev/null
docker rm chdb 2&>/dev/null
docker stop chserver 2&>/dev/null
docker rm chserver 2&>/dev/null
docker run --name chdb -p 3306:3306 -d --network host registry.gitlab.com/churchhostel/churchhostel/database:master
docker run --name chserver -v `pwd`/server/config.json:/mnt/config/config.json -p 8080:8080 -d --network host registry.gitlab.com/churchhostel/churchhostel/server:master
docker run --name chwebapp -p 8081:80 -d --network host registry.gitlab.com/churchhostel/churchhostel/webapp:master
