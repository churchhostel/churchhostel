## Run a test docker DB
`docker run --name chdb -e MYSQL_ROOT_PASSWORD=root -d mysql:5`
`mysql -uroot --protocol tcp -p -h 172.17.0.3 < /misc/chdump.sql` 

## Run a test docker server
`docker run --name chserver -v /home/reiscracker/Dev/Churchhostel/churchhostel/server/config/config.json:/mnt/config/config.json -p 8080:8080 -d registry.gitlab.com/churchhostel/churchhostel/server`