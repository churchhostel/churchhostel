import React from "react";
import classNames from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export function withLoader(checkPropsPredicate, WrappedComponent) {
    return React.forwardRef((props, ref) => {
        if (checkPropsPredicate(props)) {
            return <WrappedComponent forwardedRef={ref} {...props} />;
        } else {
            return <div style={{ display: "flex", justifyContent: "center", alignItems: "center", flexGrow: 1 }}> <FontAwesomeIcon icon="spinner" size="6x" spin /></ div >;
        }
    });

}

export function withClass(newClassName) {
    return (WrappedComponent) => ({ className = "", ...props }) => <WrappedComponent className={classNames(className, newClassName)} {...props} />;
}