const API_URL = process.env.REACT_APP_API_URL;

const JSON_HEADER = new Headers({
    "Content-Type": "application/json"
});

let token = null;

function getDefaultAuthHeader() {
    if (!token) {
        throw new Error("Token is not set. Is the user not logged in?");
    }
    return new Headers({
        "Content-Type": "application/json",
        Authorization: token
    });
}

function createHostelBody(hostel) {
    return {
        facilities: hostel.facilities,
        beds: hostel.beds,
        address: hostel.address,
        additionalHints: hostel.additionalHints,
        price: hostel.price,
        name: hostel.name,
        latitude: hostel.latitude,
        services: hostel.services,
        longitude: hostel.longitude,
        email: hostel.email
    };
}

function createContactBody(contact) {
    return {
        name: contact.name,
        phone: contact.phone,
        email: contact.email,
    };
}

function createTrailBody(trail) {
    return {
        name: trail.name,
        fileUrl: trail.fileUrl,
        startLocation: trail.startLocation,
        endLocation: trail.endLocation,
        length: trail.length
    };
}

/**
 * @returns {boolean} Whether the user is logged in
 */
export function isAuthenticated() {
    return token != null;
}

export async function login(username, password) {
    return new Promise((resolve, reject) => {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status === 200 && xhttp.getResponseHeader("authorization")) {
                    token = xhttp.getResponseHeader("authorization");
                    resolve();
                } else {
                    reject(new Error("Login API call returned " + this.status));
                }
            }
        };

        xhttp.open("POST", API_URL + "/login", true);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send(JSON.stringify({ username, password }));
    });
}

export async function removeHostel(id) {
    return await fetch(`${API_URL}/hostel/${id}`, {
        method: "DELETE",
        headers: getDefaultAuthHeader()
    });
}
export async function createHostel(hostel) {
    const response = await fetch(`${API_URL}/hostel`, {
        method: "POST",
        headers: getDefaultAuthHeader(),
        body: JSON.stringify(createHostelBody(hostel))
    });
    return await response.json();
}
export async function updateHostel(hostel) {
    if (!hostel.id) {
        console.warn("Cannot update a hostel without id");
        return;
    }
    await fetch(`${API_URL}/hostel/${hostel.id}`, {
        method: "PUT",
        headers: getDefaultAuthHeader(),
        body: JSON.stringify(createHostelBody(hostel))
    });
}

export async function removeContact(id) {
    return await fetch(`${API_URL}/contact/${id}`, {
        method: "DELETE",
        headers: getDefaultAuthHeader()
    });
}
export async function createContact(contact, churchhostelId) {
    const body = {
        ...createContactBody(contact),
        churchhostelId
    };
    await fetch(`${API_URL}/contact`, {
        method: "POST",
        headers: getDefaultAuthHeader(),
        body: JSON.stringify(body)
    });
}
export async function updateContact(contact) {
    if (!contact.id) {
        console.warn("Cannot update a contact without id");
        return;
    }
    await fetch(`${API_URL}/contact/${contact.id}`, {
        method: "PUT",
        headers: getDefaultAuthHeader(),
        body: JSON.stringify(createContactBody(contact))
    });
}

/**
 * 
 * @param {*} id 
 */
export async function fetchHostel(id) {
    const response = await fetch(`${API_URL}/hostel/${id}`);
    if (!response.ok) throw Error(response.status);
    return await response.json();
}

/**
 * 
 * @param {Number} id Trail ID
 */
export async function fetchTrail(id) {
    const response = await fetch(`${API_URL}/trail/${id}`);
    if (!response.ok) throw Error(response.status);
    return await response.json();
}

export async function fetchHostels() {
    return await (await fetch(`${API_URL}/hostel`)).json();
}

export async function fetchTrails() {
    return await (await fetch(`${API_URL}/trail`)).json();
}

export async function createTrail(trail) {
    const response = await fetch(`${API_URL}/trail`, {
        method: "POST",
        headers: getDefaultAuthHeader(),
        body: JSON.stringify(createTrailBody(trail))
    });
    return await response.json();
}

export async function updateTrail(trail) {
    if (!trail.id) {
        console.warn("Cannot update a hostel without id");
        return;
    }
    await fetch(`${API_URL}/trail/${trail.id}`, {
        method: "PUT",
        headers: getDefaultAuthHeader(),
        body: JSON.stringify(createTrailBody(trail))
    });
}

/**
 * 
 * @param {Number} id Trail ID
 */
export async function removeTrail(id) {
    return await fetch(`${API_URL}/trail/${id}`, {
        method: "DELETE",
        headers: getDefaultAuthHeader()
    });
}

export async function fetchContacts(contactIds) {
    return Promise.all(
        contactIds.map(id => fetch(`${API_URL}/contact/${id}`)
            .then(resp => resp.json()))
    );
}

/**
 * body: {name, email, communityName, communityAddress, phoneNumber, message}
 */
export async function sendContactMail(body) {
    const response = await fetch(`${API_URL}/mail/contact`, {
        method: "POST",
        body: JSON.stringify(body),
        headers: JSON_HEADER
    });
    if (!response.ok) throw Error(response.status);
    return Promise.resolve();
}

export function uploadImageFile(filesObject) {
    return new Promise((resolve, reject) => {
        const formData = new FormData();
        formData.append('file', filesObject);
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status >= 200 && this.status < 300) {
                    resolve(
                        `${API_URL}/files/image/${JSON.parse(this.response).fileName}`
                    );
                } else {
                    reject(new Error('UploadImageFile API call returned ' + this.status));
                }
            }
        };
        xhr.open('POST', `${API_URL}/files/image`, true);
        xhr.setRequestHeader('Authorization', token);
        xhr.send(formData);
    });
}

/**
 *  Uploads a file object and returns the URL.
 * @param {*} filesObject
 */
export function uploadTrailFile(filesObject) {
    return new Promise((resolve, reject) => {
        const formData = new FormData();
        formData.append('file', filesObject);
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status >= 200 && this.status < 300) {
                    resolve(
                        `${API_URL}/files/trail/${JSON.parse(this.response).fileName}`
                    );
                } else {
                    reject(new Error('UploadTrailFile API call returned ' + this.status));
                }
            }
        };
        xhr.open('POST', `${API_URL}/files/trail`, true);
        xhr.setRequestHeader('Authorization', token);
        xhr.send(formData);
    });
}

export async function addImage(churchhostelId, fileUrl) {
    const body = {
        churchhostelId,
        fileUrl
    };
    const response = await fetch(`${API_URL}/image`, {
        method: "POST",
        body: JSON.stringify(body),
        headers: getDefaultAuthHeader()
    });
    if (!response.ok) {
        throw Error("Failed to upload image: " + response.status);
    }
    return await response.json();
}

export async function removeImage(imageId) {
    const url = `${API_URL}/image/${imageId}`;
    const response = await fetch(url, {
        method: "DELETE",
        headers: getDefaultAuthHeader()
    });
    if (!response.ok) {
        throw Error("Failed to remove image: " + response.status);
    }
}

export function getGpxDownloadLink(churchhostelId) {
    return `${API_URL}/hostel/${churchhostelId}/gpx`;
}