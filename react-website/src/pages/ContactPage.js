import React, { Component } from "react";
import { Link } from "react-router-dom";
import { sendContactMail } from "../api";
import { Helmet } from "react-helmet";

import "./ContactPage.css";

export default class ContactPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isMailSent: false,
            inputs: {
                contactName: "",
                email: "",
                communityName: "",
                communityAddress: "",
                phoneNumber: "",
                message: ""
            }
        };
    }

    onValueChange(propName, event) {
        const val = event.target.value;
        this.setState(state => {
            return {
                inputs: {
                    ...state.inputs,
                    [propName]: val,
                }
            };
        });
    }

    async onSubmit(event) {
        event.preventDefault();
        await sendContactMail(this.state.inputs);
        this.setState({
            isMailSent: true
        });
        return false;
    }

    render() {
        const { contactName, email, communityName, communityAddress, phoneNumber, message } = this.state.inputs;
        return (
            <div className="container my-5">
                <Helmet>
                    <title>Kontakt | Churchhostel</title>
                    <meta name="description" content="Sie wollen als Gemeinde bei Churchhostel teilnehmen? Kontaktieren Sie uns jetzt." />
                </Helmet>
                <div className="text-center">
                    <p>
                        Wenn Sie Fragen zu Churchhostel haben, oder Mitglied einer Gemeinde sind, die an Churchhostel interessiert ist, können
                        Sie uns einfach über dieses Formular kontaktieren. Beachten Sie dazu auch unsere <Link to="/imprint#Dataprotection">Datenschutzhinweise</Link>.

                    </p>
                </div>
                <form className="contact-form" onSubmit={event => this.onSubmit(event)}>
                    <fieldset className="mb-2">
                        <legend>Pflichtangaben</legend>
                        <div className="form-group">
                            <label htmlFor="ContactFormName">Name des Ansprechpartners</label>
                            <input className="form-control" value={contactName} onChange={e => this.onValueChange("contactName", e)} type="text" id="ContactFormName" placeholder="Name..." required />
                        </div>
                        <div className="form-group">
                            <label htmlFor="ContactFormMail">Email-Addresse des Ansprechpartners</label>
                            <input className="form-control" value={email} onChange={e => this.onValueChange("email", e)} type="text" id="ContactFormMail" placeholder="Email-Addresse..." required />
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Optionale Angaben (Wenn Sie uns als Gemeinde kontaktieren)</legend>
                        <div className="form-group ">
                            <label htmlFor="ContactFormCommunityName">Name der Gemeinde</label>
                            <input className="form-control" value={communityName} onChange={e => this.onValueChange("communityName", e)} type="text" id="ContactFormCommunityName" placeholder="Name..." />
                        </div>
                        <div className="form-group ">
                            <label htmlFor="ContactFormCommunityLocation">Anschrift der Gemeinde</label>
                            <input className="form-control" value={communityAddress} onChange={e => this.onValueChange("communityAddress", e)} type="text" id="ContactFormCommunityLocation" placeholder="Ort..." />
                        </div>
                        <div className="form-group ">
                            <label htmlFor="ContactFormTel">Telefonnummer</label>
                            <input className="form-control" value={phoneNumber} onChange={e => this.onValueChange("phoneNumber", e)} type="text" id="ContactFormTel" placeholder="Telefonnummer..." />
                        </div>
                        <div className="form-group ">
                            <label htmlFor="ContactFormComment">Ihre Nachricht an uns..</label>
                            <textarea className="form-control" value={message} onChange={e => this.onValueChange("message", e)} id="ContactFormComment" placeholder="Ihre Nachricht..."></textarea>
                        </div>
                    </fieldset>
                    <p>
                        <button type="submit" className="btn btn-action">Absenden</button>
                    </p>
                    {this.state.isMailSent &&
                        <h3>Vielen Dank! Wir werden Ihre Anfrage so schnell wie möglich beantworten.</h3>
                    }
                </form>
            </div>
        );
    }
}