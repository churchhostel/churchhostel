import React from "react";
import { Helmet } from "react-helmet";

import "./AboutPage.css";

export default () => (
    <div className="container my-5">
        <Helmet>
            <title>Über Churchhostel | Churchhostel</title>
            <meta name="description" content="Churchhostel erlaubt es Gemeinden in ganz Deutschland ihre Gemeindehäuser für Reisende anzubieten. Mehr über die Idee Churchhostel." />
        </Helmet>

        <h1>Über Churchhostel</h1>
        <p>
            „Churchhostel“ ermöglicht den Pilgern (Wanderern, Radfahrern), neben
            Gasthöfen, Hotels usw. auch kirchliche Gemeindehäuser („Churchhostels“)
            für die Übernachtung einzuplanen, denn nicht immer sind die kommerziellen Übernachtungsgelegenheiten in
            „wanderbarer“ oder „radelbarer“ Entfernung.
        </p>
        <p>
            „Churchhostel“ ist die Plattform, die Pilger (Wanderer, Radfahrer) und
            Kirchengemeinden zusammenbringt.
            <br />
            Pilger können mithilfe von „Churchhostel“ ihre Tagesetappen flexibler
            planen: Heute Abend im Gasthof, morgen im Churchhostel, übermorgen
            im Hotel.
            <br />
            Kirchengemeinden und ihre Gemeindehäuser („Churchhostels“) bieten für
            eine, maximal zwei Nächte eine „Heimat auf Zeit“, wie man es von
            Pilgerherbergen kennt.
        </p>
        <p>
            „Churchhostel“ ist keine Buchungsplattform, sondern ermögpcht nur die
            bessere Planung und direkte Kommunikation zwischen Reisenden und
            Gemeinden
        </p>

        <h3>Informationen für neue Gemeinden</h3>
        <p>
            Ihre Gemeinde liegt an einem bekannten Pilger-, Wander- oder Radweg?
        </p>
        <p>
            Immer wieder kommen Menschen, die Sie unvorbereitet nach einer
            Übernachtungsmöglichkeit fragen?
            Sie möchten Teil der weltweiten Pilgerbewegung werden und
            <br />
            Jesu Gebot verwirklichen („Ich war fremd, und Ihr habt mich
            aufgenommen“)?
        </p>
        <p>
            Dann machen Sie aus Ihren Gemeinderäumen doch ein „Churchhostel“:
            „Churchhostel“ ermöglicht Ihnen eine längerfristige und dennoch
            flexible Planung, kein Pilger oder Wanderer steht plötzlich vor der Tür, wo
            Sie doch gerade Feierabend machen wollten. Denn mit „Churchhostel“ hat
            der Pilger schon lange vorher mit Ihnen Kontakt aufgenommen und seine
            Pilgerung / Wanderung geplant. Schauen Sie doch die Teilnahmeerklärung
            und den Teilnahmevertrag an. Auch der Flyer, den die Reisenden in die
            Hand bekommen, ist hier hinterlegt.
        </p>
        <p>
            Das Beste an „Churchhostel“ ist: Sie entscheiden ganz allein, wann Platz
            für Pilger (Wanderer, Radfahrer) ist.
        </p>
        <p>
            Senden Sie uns eine Mail über den Punkt "Kontakt" und wir nehmen Sie in unser Programm auf.
        </p>
        <ul className="no-bullets">
            <li><a target="_blank" rel="noopener noreferrer" href="http://assets.churchhostel.de/Churchhostel_Flyer.pdf">Download Churchhostel Flyer</a></li>
            <li><a target="_blank" rel="noopener noreferrer" href="http://assets.churchhostel.de/Teilnahmeerklaerung.pdf">Download Teilnahmeerklärung
                    für Gemeinden</a></li>
            <li><a target="_blank" rel="noopener noreferrer" href="http://assets.churchhostel.de/Teilnahmevertrag.pdf">Download beispielhafter
                    Teilnahmevertrag für Gemeinden</a></li>
        </ul>
    </div >

);