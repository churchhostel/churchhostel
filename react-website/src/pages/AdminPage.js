import React, { Component } from "react";
import { Provider as AlertProvider } from "react-alert";
import { Alert } from "reactstrap";
import { fetchHostel, fetchHostels, fetchTrails } from "../api";
import HostelForm from "../containers/admin/HostelForm";
import Login from "../containers/admin/Login";
import { HostelSidebarList, TrailSidebarList } from "../containers/admin/SidebarList";
import TrailForm from "../containers/admin/TrailForm";
import { normalize } from "../util";
import "./AdminPage.css";

const VIEW_HOSTEL_FORM = 1;
const VIEW_TRAIL_FORM = 2;

const ALERT_OPTIONS = {
    timeout: 2000
};

function AlertTemplate(props) {
    // the style contains only the margin given as offset
    // options contains all alert given options
    // message is the alert message...
    // close is a function that closes the alert
    const { style, options, message, close } = props;

    return (
        <div style={{ ...style, width: "80vw", textAlign: "center" }}>
            <Alert color={options.type} toggle={close}>{message}</Alert>

        </div>
    );
}

class AdminPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            hostels: [],
            trails: [],
            currentHostelId: null,
            currentTrailId: null,
            view: VIEW_HOSTEL_FORM
        };
    }

    async componentDidMount() {
        await this.updateHostels();
        this.selectFirstHostel();
        this.updateTrails();
    }

    selectFirstHostel() {
        if (this._atLeastOneHostelLoaded()) {
            const firstHostelKey = Object.keys(this.state.hostels)[0];
            const firstHostelId = this.state.hostels[firstHostelKey].id;
            this.onSelectHostel(firstHostelId);
        }
    }

    selectFirstTrail() {
        if (this._atLeastOneTrailLoaded()) {
            const firstTrailKey = Object.keys(this.state.trails)[0];
            const firstTrailId = this.state.trails[firstTrailKey].id;
            this.onSelectTrail(firstTrailId);
        }
    }

    async updateHostels() {
        const updatedHostels = await fetchHostels();
        return new Promise((res, rej) => {
            this.setState({
                hostels: normalize(updatedHostels)
            }, res());
        });
    }

    async updateCurrentHostel() {
        if (!this.state.currentHostelId) {
            console.warn("Cannot update current hostel because no current hostel ID is set.");
            return;
        }
        const updatedHostel = await fetchHostel(this.state.currentHostelId);
        this.setState(currentState => ({
            hostels: { ...currentState.hostels, [this.state.currentHostelId]: updatedHostel }
        }));
    }

    async updateTrails() {
        this.setState({
            trails: normalize(await fetchTrails())
        });
    }

    onSelectHostel(selectedHostelId) {
        this.setState({
            currentHostelId: selectedHostelId,
            currentTrailId: null,
            view: VIEW_HOSTEL_FORM
        });
    }
    onSelectTrail(selectedTrailId) {
        this.setState({
            currentHostelId: null,
            currentTrailId: selectedTrailId,
            view: VIEW_TRAIL_FORM
        });
    }

    onNewHostelClick() {
        this.setState({
            currentHostelId: null,
            view: VIEW_HOSTEL_FORM
        });
    }
    onNewTrailClick() {
        this.setState({
            currentTrailId: null,
            view: VIEW_TRAIL_FORM
        });
    }


    _atLeastOneHostelLoaded() {
        return this.state.hostels && Object.keys(this.state.hostels).length > 0;
    }

    _atLeastOneTrailLoaded() {
        return this.state.trails && Object.keys(this.state.trails).length > 0;
    }

    render() {
        let formInView;
        if (this.state.view === VIEW_HOSTEL_FORM) {
            const currentHostel = this.state.hostels[this.state.currentHostelId] || {};
            formInView = <HostelForm hostel={currentHostel}
                setCurrentHostel={id => this.onSelectHostel(id)}
                selectFirstHostel={() => this.selectFirstHostel()}
                updateCurrentHostel={() => this.updateCurrentHostel()}
                updateHostelList={() => this.updateHostels()}
            />;
        } else {
            const currentTrail = this.state.trails[this.state.currentTrailId] || {};
            formInView = <TrailForm trail={currentTrail}
                setCurrentTrail={id => this.onSelectTrail(id)}
                selectFirstTrail={() => this.selectFirstTrail()}
                updateTrailList={() => this.updateTrails()} />;
        }

        return (
            <AlertProvider template={AlertTemplate} {...ALERT_OPTIONS}>
                <Login /> {/* Login will not render if the user is successfully logged in */}
                <div className="adminpage">
                    <div className="adminpage__sidebar">
                        <HostelSidebarList list={Object.values(this.state.hostels)}
                            onNewClick={() => this.onNewHostelClick()}
                            onSelectClick={id => this.onSelectHostel(id)} />
                        <TrailSidebarList list={Object.values(this.state.trails)}
                            onNewClick={id => this.onNewTrailClick()}
                            onSelectClick={id => this.onSelectTrail(id)} />
                    </div>
                    <div id="view">
                        {formInView}
                    </div>
                </div>
            </AlertProvider>
        );
    }
}

export default AdminPage;