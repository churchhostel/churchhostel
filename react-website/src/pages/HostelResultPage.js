import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { fetchHostel, fetchContacts, getGpxDownloadLink } from "../api";
import { withLoader } from "../hocs";
import ImageBox from "../components/result/ImageBox";
import HostelSection, { ContactsSection, PriceSection } from "../components/result/HostelSection";
import Lightbox from "../components/Lightbox";
import { Helmet } from "react-helmet";

import "./HostelResultPage.css";

const ViewWithLoader = withLoader(props => props.hostel, ({ hostel, contacts, gpxDownloadLink, onImageClick }) => {
    return <div className="hostel">
        <Helmet>
            <title>Übernachtung in {hostel.name} |  Einfach und preiswert | Churchhostel</title>
            <meta name="description" content={`Gemeinde ${hostel.name} bietet günstige Übernachtungen an. Jeder willkommen - egal ob Wanderer, Radfahrer oder Pilger. Jetzt telefonisch reservieren.`} />
        </Helmet>

        <h1 className="hostel__title">{hostel.name}</h1>
        <div className="hostel__subtitle">
            <h4 className="hostel__address">{hostel.address}</h4>
            <span className="hostel__buttons">
                <a href={gpxDownloadLink} className="btn btn-action"><FontAwesomeIcon icon="download" />GPX herunterladen</a>
            </span>
        </div>

        {hostel.images && hostel.images.length > 0 &&
            <div className="hostel__images">
                <ImageBox images={hostel.images.map(({ fileUrl: url }) => ({ url, onClick: () => onImageClick(url) }))} />
            </div>
        }
        <div className="hostel__sections">
            {contacts && contacts.length > 0 &&
                <ContactsSection className="hostel__contacts" heading="Ihre Ansprechpartner">
                    <ul>
                        {contacts.map(c => (
                            <li key={c.id}>
                                <div>{c.name}</div>
                                <div className="hostel__contact-information">
                                    {c.phone && <a href={"tel: " + c.phone} className="phone-link"><FontAwesomeIcon icon="phone" />{c.phone}</a>}
                                    {c.email && <a href={"mailto: " + c.email} className="phone-link"><FontAwesomeIcon icon="envelope" />{c.email}</a>}
                                </div>
                            </li>
                        ))}
                    </ul>
                </ContactsSection>
            }
            {hostel.facilities && hostel.facilities.length > 0 &&
                <HostelSection className="hostel__facilities" heading="Ausstattung der Unterkunft">
                    <ul className="hostel__checkmarklist">
                        {hostel.facilities.map((f, index) => <li key={index}>{f}</li>)}
                    </ul>
                </HostelSection>
            }
            {hostel.services && hostel.services.length > 0 &&
                <HostelSection className="hostel__services" heading="Serviceangebot">
                    <ul className="hostel__bulletlist">
                        {hostel.services.map((f, index) => <li key={index}>{f}</li>)}
                    </ul>
                </HostelSection>
            }
            {hostel.additionalHints && hostel.additionalHints.length > 0 &&
                <HostelSection className="hostel__hints" heading="Hinweise">
                    <ul className="hostel__infolist">
                        {hostel.additionalHints.map((f, index) => <li key={index}>{f}</li>)}
                    </ul>
                </HostelSection>
            }
            <PriceSection className="hostel__price" heading="Preis pro Person">
                {hostel.price}€
            </PriceSection>
        </div>
    </div>;
});


export default class HostelResultPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            hostel: null,
            contacts: [],
            lightboxImageUrl: null,
        };
    }
    async componentDidMount() {
        const hostel = await fetchHostel(this.props.match.params.hostelId);
        this.setState({
            hostel
        });
        const contacts = await fetchContacts(hostel.contacts);
        this.setState({
            contacts
        });
    }

    openLightbox(lightboxImageUrl) {
        this.setState({ lightboxImageUrl });
    }

    closeLightbox() {
        this.setState({ lightboxImageUrl: null });
    }

    render() {
        const gpxDownloadLink = this.state.hostel ? getGpxDownloadLink(this.state.hostel.id) : "";
        return (
            <div className="content">
                <div style={{ marginLeft: "1%", fontSize: "1.3em" }}>
                    <Link to={"/hostel"} className="inline-navigation-link">
                        <FontAwesomeIcon icon="angle-left" /> Zurück zur Suche
                    </Link>
                </div>
                <ViewWithLoader hostel={this.state.hostel} contacts={this.state.contacts}
                    onImageClick={imageUrl => this.openLightbox(imageUrl)} gpxDownloadLink={gpxDownloadLink} />
                {this.state.lightboxImageUrl &&
                    <Lightbox onClose={() => this.closeLightbox()}>
                        <img src={this.state.lightboxImageUrl} alt="Larger view in a lightbox" />
                    </Lightbox>
                }
            </div>
        );
    }

}

