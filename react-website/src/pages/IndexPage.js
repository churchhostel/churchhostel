import React from "react";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";

import indexLinkImage1 from "../images/indexLinkImage1_lq.jpg";
import indexLinkImage2 from "../images/indexLinkImage2_lq.jpg";
import indexLinkImage3 from "../images/indexLinkImage3_lq.jpg";
import indexLinkImage4 from "../images/indexLinkImage4_lq.jpg";

import "./IndexPage.css";

export default () => (
    <div className="">
        <Helmet>
            <title>Churchhostel: Günstige Übernachtungen in Gemeindehäusern</title>
            <meta name="description" content="Gemeindehäuser öffnen Ihre Türen für Pilger, Radfahrer und Wanderer. Übernachten Sie für einen kleinen Unkostenbeitrag. Einfach telefonisch reservieren." />
        </Helmet>
        <div className="container-fluid index-cards">
            <div className="card">
                <Link to="/hostel"><img src={indexLinkImage2} className="card-img-top" alt="A church" /></Link>
                <div className="card-body">
                    <h2 className="card-title">Ein Churchhostel finden</h2>
                    <p className="card-text">Suche direkt nach einem Ort oder finde das nächste Churchhostel entlang eines Wanderweges.</p>
                </div>
                <p className="card-footer bg-white border-top-0 lead">Alle Gemeinden findest du auf unserer <Link to="/hostel" className="link-card-link">Karte</Link>.</p>
            </div>
            <div className="card">
                <Link to="/contact"><img src={indexLinkImage3} className="card-img-top" alt="A church" /></Link>
                <div className="card-body">
                    <h2 className="card-title">Als Gemeinde teilnehmen</h2>
                    <p className="card-text">Jede Gemeinde kann einfach an Churchhostel teilnehmen.</p>
                </div>
                <p className="card-footer bg-white border-top-0 lead">Bitte kontaktieren Sie uns hierfür über unsere <Link to="/contact" className="link-card-link">Kontaktseite</Link>.</p>
            </div>
            <div className="card">
                <Link to="/howitworks"><img src={indexLinkImage4} className="card-img-top" alt="A church" /></Link>
                <div className="card-body">
                    <h2 className="card-title">Churchhostel ist einfach!</h2>
                    <p className="card-text">Churchhostel finden, kontaktieren und Übernachtung ausmachen.</p>
                </div>
                <div className="card-footer bg-white border-top-0">
                    <p className="lead">Genaue Anleitungen findest du unter <Link to="/howitworks" className="link-card-link">So funktioniert's</Link>.</p>
                </div>
            </div>
            <div className="card">
                <Link to="/about"><img src={indexLinkImage1} className="card-img-top" alt="A church" /></Link>
                <div className="card-body">
                    <h2 className="card-title">Die Idee Churchhostel</h2>
                    <p className="card-text">Was ist Churchhostel eigentlich und für wen sind wir da?</p>
                </div>
                <p className="card-footer bg-white border-top-0 lead">Alle Informationen über das Projekt und was unser <Link to="/about" className="link-card-link">Ziel</Link> ist.</p>
            </div>
        </div>
    </div>
);

