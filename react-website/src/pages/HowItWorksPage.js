import React from "react";
import { HashLink as Link } from "react-router-hash-link";
import { Helmet } from "react-helmet";

export default () => (
    <div className="container my-5">
        <Helmet>
            <title>So funktionieren Gemeinde-Hostels | Churchhostel</title>
            <meta name="description" content="Finden Sie schnell und einfach Übernachtungsmöglichkeiten entlang von Wanderwegen. In wenigen Schritten die passende Unterkunft finden." />
        </Helmet>

        <div>
            <h1>So funktionierts</h1>
            <p className="">
                Auf Churchhostel.de findest du ganz einfach eine günstige Übernachtungsmöglichkeit.
                Dafür bieten Kirchengemeinden ihre Gemeindehäuser an.
                In nur wenigen Schritten kannst du eine Gemeinde kontaktieren, um eine Übernachtung zu buchen.
                So findest du bei jeder Wanderung, jeder Reise oder jeder Fahrrad-Fahrt eine günstige Unterkunft.
            </p>
        </div>
        <div className="px-3">
            <div>
                <h3>1. Ein Churchhostel finden</h3>
                <p className="">
                    Finde zuerst eine teilnehmende Kirchengemeinde, die für deine Wanderung oder deine Reise günstig liegt.
                </p>
                <p>
                    Dazu kannst du in unserer <Link to={"/hostel#SearchList"}>Suche</Link> entweder direkt nach einem Ort suchen,
                    oder unsere <Link to={"/hostel#SearchMap"}>Karte</Link> nutzen.
                    Auf der Karte lassen sich auch häufig genutzte Wanderwege einblenden, sodass du schnell eine Übernachtungsmöglichkeit entlang deiner Strecke findest.
                    Suchst Du für dich allein oder vielleicht für eine ganze Gruppe eine Übernachtung? Die Anzahl der Schlafplätze lässt sich auf unserer Karte einstellen!
                </p>
            </div>
            <div>
                <h3>2. Churchhostel ansehen</h3>
                <p className="">
                    Die Gemeinden bieten dir verschiedene Ausstattungen und Services an.
                </p>
                <p>
                    Betrachte die Unterkünfte, die für dich in Frage kommen und sieh dir die Bilder und Hinweise an, die die Gemeinde zur Verfügung stellt.
                </p>
            </div>
            <div>
                <h3>3. Den Inhaber kontaktieren</h3>
                <p className="">
                    Hast du eine passende Gemeinde gefunden, kannst du den Gastgeber kontaktieren.
                </p>
                <p>
                    Auf der Seite jeder Gemeinde findest du die jeweiligen Ansprechpartner und ihre Kontaktinformationen.
                    Diese kannst du per Telefon oder Email kontaktieren um nach einer Übernachtung zu fragen.
                </p>
            </div>
        </div>
    </div>
);