import React, { Component } from "react";
import { fetchHostels } from "../api";
import HostelMap from "../containers/HostelMap";
import HostelSearch from "../containers/HostelSearch";
import { withLoader } from "../hocs";
import { normalize } from "../util";
import { Helmet } from "react-helmet";

import "./HostelSearchPage.css";

let cachedHostels = null;

export default class HostelSearchPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            hostels: cachedHostels
        };
    }

    async componentDidMount() {
        // Load and cache the hostels only the first time
        if (cachedHostels === null) {
            cachedHostels = normalize(await fetchHostels());
        }
        this.setState({
            hostels: cachedHostels
        });
    }

    render() {
        return <SearchPageWithLoader hostels={this.state.hostels} />;
    }

}

function areHostelsLoaded(props) {
    return props.hostels != null && Object.keys(props.hostels).length > 0;
}

const SearchPageWithLoader = withLoader(props => areHostelsLoaded(props), ({ hostels }) => {
    return (
        <div className="container-fluid my-3">
            <Helmet>
                <title>Wanderunterkunft online finden I Einfach und preiswert I Churchhostel</title>
                <meta name="description" content="Finden Sie Übernachtungsmöglichkeiten beim Wandern. Gemeindehäuser bieten günstige Unterkünfte für Wanderer, Pilger &uuml; Radfahrer. Jetzt eine Unterkunft suchen." />
            </Helmet>
            <div className="searchpage">
                <div className="searchpage__container searchpage__container--map">
                    <h4 className="searchpage__containerheading" id="SearchMap">Finde eine Unterkunft auf unserer Karte</h4>
                    <HostelMap hostels={hostels} />
                </div>
                <div className="searchpage__container searchpage__container--list">
                    <h4 className="searchpage__containerheading" id="SearchList">Finde eine Unterkunft in unserer Liste</h4>
                    <HostelSearch hostels={hostels} />
                </div>
            </div>
        </div>
    );
});