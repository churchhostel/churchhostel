import markerBlueImg from "./images/marker_blue.png";
import markerGreenImg from "./images/marker_green.png";
import markerYellowImg from "./images/marker_yellow.png";
import markerRedImg from "./images/marker_red.png";

/**
 *  Returns an object in the form of
 * {
 *  1:  { hostel with id 1 },
 *  3:  { hostel with id 3 },
 *  ...
 * } 
 * @param {Array} hostels 
 */
export function normalize(hostels) {
    return hostels.reduce((obj, hostel) => ({ [hostel.id]: hostel, ...obj }), {});
}

/**
 * Contacts passed in like
 * email: "info@hannes.de"
    id: 1
    name: "Hannes"
    phone: "12345"
 */
export function findChangedContacts(oldContacts, newContacts) {
    const oldContactIds = oldContacts.map(c => c.id);
    return newContacts.filter(c => oldContactIds.includes(c.id));
}

export function findDeletedContacts(oldContacts, newContacts) {
    // Deleted are contacts that were in oldContacts but are no longer in newContacts
    const newContactIds = newContacts.map(c => c.id);
    return oldContacts.filter(c => !newContactIds.includes(c.id));

}

export function findCreatedContacts(oldContacts, newContacts) {
    return newContacts.filter(c => !c.id);
}

export function getMarkerIcon(bedCount) {
    if (!bedCount || bedCount <= 2) {
        return markerBlueImg;
    } else if (bedCount <= 5) {
        return markerGreenImg;
    } else if (bedCount <= 10) {
        return markerYellowImg;
    } else if (bedCount > 10) {
        return markerRedImg;
    }
}