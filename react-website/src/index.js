import 'react-app-polyfill/stable';

import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Helmet } from "react-helmet";

import Menu from "./containers/Menu";
import Footer from "./components/Footer";
import HostelResultPage from "./pages/HostelResultPage";
import HostelSearchPage from "./pages/HostelSearchPage";
import IndexPage from "./pages/IndexPage";
import HowItWorksPage from "./pages/HowItWorksPage";
import ImprintPage from "./pages/ImprintPage";
import AboutPage from "./pages/AboutPage";
import ContactPage from "./pages/ContactPage";
import AdminPage from "./pages/AdminPage";

import "./index.css";
import "./custom.scss";

import { library } from "@fortawesome/fontawesome-svg-core";
import { faDownload, faCaretDown, faBan, faPlusSquare, faUpload, faBars, faPhone, faEdit, faEnvelope, faAngleLeft, faSpinner, faTimesCircle, faCircle } from "@fortawesome/free-solid-svg-icons";
library.add(faBars);
library.add(faPhone);
library.add(faEnvelope);
library.add(faAngleLeft);
library.add(faSpinner);
library.add(faTimesCircle);
library.add(faCircle);
library.add(faEdit);
library.add(faUpload);
library.add(faPlusSquare);
library.add(faBan);
library.add(faCaretDown);
library.add(faDownload);


const GA_TRACKING_ID = "UA-133719883-1";

window.IS_DEBUG = process.env.REACT_APP_IS_DEBUG || false;
window.log = function () {
    if (window.IS_DEBUG) {
        console.log(...arguments); // eslint-disable-line no-console
    }
};

function MainApp() {
    return (
        <Fragment>
            <Helmet
                script={[{
                    type: 'text/javascript',
                    innerHTML: `
                        var gaProperty = '${GA_TRACKING_ID}'; 
                        var disableStr = 'ga-disable-' + gaProperty; 
                        if (document.cookie.indexOf(disableStr + '=true') > -1) { 
                            window[disableStr] = true;
                        } 
                        function gaOptout() { 
                            document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/'; 
                            window[disableStr] = true; 
                            alert('Das Tracking ist jetzt deaktiviert'); 
                        } 
                        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ 
                                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), 
                            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) 
                        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga'); 

                        ga('create', '${GA_TRACKING_ID}', 'auto'); 
                        ga('set', 'anonymizeIp', true); 
                        ga('send', 'pageview'); 
                    `
                }]}
            />
            <Menu />
            <div className="main-content">
                <Route path="/" exact component={IndexPage} />
                <Route path="/howitworks" component={HowItWorksPage} />
                <Route path="/hostel" exact component={HostelSearchPage} />
                <Route path="/hostel/:hostelId" component={HostelResultPage} />
                <Route path="/contact" component={ContactPage} />
                <Route path="/about" component={AboutPage} />
                <Route path="/imprint" component={ImprintPage} />
            </div>
            <Footer />
        </Fragment >
    );
}


class App extends Component {

    render() {
        return (
            <Router>
                <Switch>
                    <Route path="/admin" component={AdminPage} />
                    <Route path="/" component={MainApp} />
                </Switch>
            </Router>
        );
    }

}

ReactDOM.render(<App />, document.getElementById("root"));
