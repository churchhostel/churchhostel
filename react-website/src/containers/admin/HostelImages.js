import PropTypes from "prop-types";
import React, { Component } from "react";
import { addImage, removeImage, uploadImageFile } from "../../api";
import HostelImagesComp from "../../components/admin/HostelImages";

class HostelImages extends Component {

    /**
     * Uploads an image and then merges the new image into this hostels images
     * @param {File} file 
     */
    async onUploadImageFile(file) {
        if (!this.props.currentHostelId) {
            return;
        }
        const uploadedFileUrl = await uploadImageFile(file);
        const addedImageObject = await addImage(this.props.currentHostelId, uploadedFileUrl);
        this.props.images.push(addedImageObject);
        this.forceUpdate();
    }

    async onRemoveImage(imageId) {
        await removeImage(imageId);
        const removedImageIndex = this.props.images.map(img => img.id).indexOf(imageId);
        this.props.images.splice(removedImageIndex, 1);
        this.forceUpdate();
    }

    render() {
        return (
            <HostelImagesComp
                images={this.props.images}
                isDisabled={!this.props.currentHostelId}
                onUploadImageFile={file => this.onUploadImageFile(file)}
                onRemoveImage={id => this.onRemoveImage(id)}
            />
        );
    }
}

HostelImages.propTypes = {
    images: PropTypes.array,
    currentHostelId: PropTypes.number.isRequired,
};

HostelImages.defaultProps = {
    images: []
};

export default HostelImages;