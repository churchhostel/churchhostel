import React, { Component } from "react";
import LoginComponent from "../../components/admin/Login";
import { login } from "../../api";
import { isAuthenticated } from "../../api";
import { Formik } from "formik";
import { withAlert } from "react-alert";

class Login extends Component {

    async onLogin(values) {
        try {
            await login(values.name, values.password);
            this.props.alert.show("Anmeldung erfolgreich.", { type: "success" });
        } catch (e) {
            this.props.alert.show("Anmeldung fehlgeschlagen.", { type: "danger" });
        }
        this.forceUpdate();
    }

    render() {
        if (isAuthenticated()) {
            return null;
        } else {
            return (
                <Formik
                    initialValues={{ name: "", password: "" }}
                    onSubmit={values => this.onLogin(values)}
                >
                    {props => (
                        <LoginComponent {...props} />
                    )}
                </Formik>
            );
        }
    }
}

Login.propTypes = {

};

export default withAlert(Login);