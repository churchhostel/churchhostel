import React, { Component } from "react";
import PropTypes from "prop-types";
import { fetchContacts, removeHostel, createHostel, updateHostel, removeContact, updateContact, createContact } from "../../api";
import { findChangedContacts, findCreatedContacts, findDeletedContacts } from "../../util";
import { Formik } from "formik";
import { withAlert } from "react-alert";
import HostelView from "../../components/admin/HostelView";
import HostelImages from "./HostelImages";

function normalizeFormValues(hostel) {
    return {
        id: hostel.id,
        name: hostel.name || "",
        address: hostel.address || "",
        beds: hostel.beds || 0,
        email: hostel.email || "",
        latitude: hostel.latitude || 0,
        longitude: hostel.longitude || 0,
        images: hostel.images || [],
        facilities: hostel.facilities || [],
        services: hostel.services || [],
        additionalHints: hostel.additionalHints || [],
        price: hostel.price || 0,
    };
}

class HostelForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loadedContacts: [],
            contacts: []
        };
    }


    async componentDidUpdate(prevProps) {
        if (this.props.hostel.id !== prevProps.hostel.id) {
            const contacts = await this.loadContacts();
            this.setState({
                contacts,
                loadedContacts: contacts // Save these contacts for comparing later 
            });
        }
    }

    async loadContacts() {
        if (!this.props.hostel.id) {
            return [];
        }
        const loadedContacts = await fetchContacts((this.props.hostel.contacts) || []);
        return loadedContacts.map(c => ({ ...c, email: c.email || "" }));
    }

    async onSubmitHostel(hostel) {
        // This Id will be used to create new contacts. It needs to be the newly created hostel ID when a new hostel is created.
        let hostelIdForContactsUpdate;
        if (this.props.hostel.id) {
            try {
                await updateHostel(hostel);
                hostelIdForContactsUpdate = this.props.hostel.id;
                this.props.alert.show("Das Hostel wurde aktualisiert.", { type: "success" });
            } catch (e) {
                this.props.alert.show("Aktualisieren fehlgeschlagen.", { type: "danger" });
            }
        } else {
            hostelIdForContactsUpdate = (await createHostel(hostel)).id;
            this.props.alert.show("Das Hostel wurde neu angelegt.", { type: "success" });
        }
        const oldContactsList = this.state.loadedContacts;
        const newContactsList = hostel.contacts;
        await Promise.all(findChangedContacts(oldContactsList, newContactsList).map(c => updateContact(c)));
        await Promise.all(findDeletedContacts(oldContactsList, newContactsList).map(c => removeContact(c.id)));
        await Promise.all(findCreatedContacts(oldContactsList, newContactsList).map(c => createContact(c, hostelIdForContactsUpdate)));
        await this.props.updateHostelList();
        // Set the current hostel on the parent page to the newly created hostel ID
        // or to the current hostel ID if this is not a new hostel, which should be the same as the current one anyway
        this.props.setCurrentHostel(hostelIdForContactsUpdate);

    }

    async onRemoveHostel() {
        if (!this.props.hostel.id) {
            return;
        }
        await removeHostel(this.props.hostel.id);
        await this.props.updateHostelList();
        await this.props.selectFirstHostel();
        this.props.alert.show("Das Hostel wurde entfernt.", { type: "danger" });
    }


    createEmptyContact() {
        return {
            name: "",
            phone: "",
            email: ""
        };
    }


    render() {
        let formValues = normalizeFormValues(this.props.hostel);
        formValues.contacts = this.state.contacts;
        const isNewHostel = !this.props.hostel.id;
        const imageBox = <HostelImages
            images={this.props.hostel.images}
            currentHostelId={this.props.hostel.id}
        />;
        return (
            <Formik
                initialValues={formValues}
                enableReinitialize={true}
                onSubmit={values => this.onSubmitHostel(values)}
            >
                {props => (
                    <HostelView
                        isNewHostel={isNewHostel}
                        onRemoveHostel={() => this.onRemoveHostel()}
                        createEmptyContactObject={this.createEmptyContact}
                        imageBox={imageBox}
                        {...props}
                    />
                )}
            </Formik>
        );
    }
}

HostelForm.propTypes = {
    hostel: PropTypes.object.isRequired,
    setCurrentHostel: PropTypes.func.isRequired,
    selectFirstHostel: PropTypes.func.isRequired,
    updateCurrentHostel: PropTypes.func.isRequired,
    updateHostelList: PropTypes.func.isRequired,
};

export default withAlert(HostelForm);