import React, { Component } from "react";
import PropTypes from "prop-types";
import { uploadTrailFile, removeTrail, updateTrail, createTrail } from "../../api";
import { Formik } from "formik";
import TrailView from "../../components/admin/TrailView";
import { withAlert } from "react-alert";

function normalizeFormValues(trail) {
    return {
        id: trail.id,
        name: trail.name || "",
        fileUrl: trail.fileUrl || "",
        startLocation: trail.startLocation || "",
        endLocation: trail.endLocation || "",
        length: trail.length || 0
    };
}

class TrailForm extends Component {

    async onSubmitTrail(formValues) {
        // FIrst upload the new trail file if the user selected one
        // and add the url of the uploaded file to the create/update body
        if (formValues.trailFile) {
            const uploadedTrailFileUrl = await uploadTrailFile(formValues.trailFile);
            formValues.fileUrl = uploadedTrailFileUrl;
        }

        // Now create the actual trail object
        if (this.props.trail.id) {
            await updateTrail(formValues);
            this.props.alert.show("Der Wanderweg wurde aktualisiert.", { type: "success" });
        } else {
            const newTrailId = (await createTrail(formValues)).id;
            this.props.setCurrentTrail(newTrailId);
            this.props.alert.show("Der Wanderweg wurde neu angelegt.", { type: "success" });
        }
        await this.props.updateTrailList();
    }

    async onRemoveTrail() {
        if (!this.props.trail.id) {
            return;
        }
        await removeTrail(this.props.trail.id);
        await this.props.updateTrailList();
        this.props.selectFirstTrail();
        this.props.alert.show("Der Wanderweg wurde entfernt.", { type: "danger" });
    }

    validateForm(values) {
        const errors = {};
        if (!values.id && !values.trailFile) {
            errors.trailFile = "Es muss eine Weg-Datei hochgeladen werden.";
        }
        return errors;
    }

    render() {
        const formValues = normalizeFormValues(this.props.trail);
        return (
            <Formik
                initialValues={formValues}
                enableReinitialize={true}
                onSubmit={values => this.onSubmitTrail(values)}
                validateOnChange={false}
                validateOnBlur={false}
                validate={values => this.validateForm(values)}
            >
                {props => (
                    <TrailView
                        isNewTrail={props.values.id === null || props.values.id === undefined}
                        onRemoveTrail={() => this.onRemoveTrail()}
                        onUploadTrailFile={file => this.uploadTrailFile(file)}
                        {...props}
                    />
                )}
            </Formik>
        );
    }
}

TrailForm.propTypes = {
    trail: PropTypes.object.isRequired,
    setCurrentTrail: PropTypes.func.isRequired,
    selectFirstTrail: PropTypes.func.isRequired,
    updateTrailList: PropTypes.func.isRequired,
};

export default withAlert(TrailForm);