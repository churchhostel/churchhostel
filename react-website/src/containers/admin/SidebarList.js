import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { HostelSidebarList as HostelSidebarListComp, TrailSidebarList as TrailSidebarListComp } from "../../components/admin/SidebarList";

class HostelSidebarList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: ""
        };
    }

    onValueChange({ target: { value } }) {
        this.setState({
            filter: value
        });
    }

    getMatches() {
        return Object.values(this.props.list).filter(
            hostel =>
                hostel.name.includes(this.state.filter) ||
                hostel.address.includes(this.state.filter)
        );
    }

    render() {
        return (
            <Fragment>
                <HostelSidebarListComp list={this.getMatches()}
                    onNewClick={this.props.onNewClick}
                    onSelectClick={this.props.onSelectClick}
                    onFilterChange={event => this.onValueChange(event)} />
            </Fragment>
        );
    }
}
class TrailSidebarList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: ""
        };
    }

    onValueChange({ target: { value } }) {
        this.setState({
            filter: value
        });
    }

    getMatches() {
        return Object.values(this.props.list).filter(
            trail => trail.name.includes(this.state.filter)
        );
    }

    render() {
        return (
            <Fragment>
                <TrailSidebarListComp list={this.getMatches()}
                    onSelectClick={this.props.onSelectClick}
                    onNewClick={this.props.onNewClick}
                    onFilterChange={event => this.onValueChange(event)} />
            </Fragment>
        );
    }
}

HostelSidebarList.propTypes = {
    list: PropTypes.array.isRequired,
    onNewClick: PropTypes.func.isRequired,
    onSelectClick: PropTypes.func.isRequired,
};
TrailSidebarList.propTypes = {
    list: PropTypes.array.isRequired,
    onNewClick: PropTypes.func.isRequired,
    onSelectClick: PropTypes.func.isRequired,
};

export { HostelSidebarList, TrailSidebarList };