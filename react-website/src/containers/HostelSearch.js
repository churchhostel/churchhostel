import React, { Fragment } from "react";
import PropTypes from "prop-types";
import SearchInput from "../components/search/SearchInput";
import HostelList from "../components/search/HostelList";

export default class HostelSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: ""
        };
    }

    onValueChange({ target: { value } }) {
        this.setState({
            filter: value
        });
    }

    getMatches() {
        const lowerCaseFilter = this.state.filter.toLowerCase();
        return Object.values(this.props.hostels).filter(
            hostel =>
                hostel.name.toLowerCase().includes(lowerCaseFilter) ||
                hostel.address.toLowerCase().includes(lowerCaseFilter)
        );
    }

    render() {
        return (
            <Fragment>
                <SearchInput onChange={e => this.onValueChange(e)} />
                <HostelList hostels={this.getMatches()} />
            </Fragment>
        );
    }
}

HostelSearch.propTypes = {
    hostels: PropTypes.object.isRequired,
};
