import React from "react";
import Menu from "../components/Menu";

export default class MenuContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isMenuOpen: false };
    }

    toggleMenu() {
        this.setState(state => ({
            isMenuOpen: !state.isMenuOpen
        }));
    }

    render() {
        return <Menu isOpen={this.state.isMenuOpen} onToggleClick={() => this.toggleMenu()} />
    }
}