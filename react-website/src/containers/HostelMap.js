import React, { Fragment } from "react";
import PropTypes from "prop-types";
import GoogleMap from "../components/GoogleMap";
import ToggleSlider from "../components/ToggleSlider";
import Slider from "../components/Slider";
import Select from "react-select";
import { fetchTrails } from "../api";
import { normalize } from "../util";

import "./HostelMap.css";

let cachedTrails = null;

export default class HostelMap extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bedsFilter: 0,
            priceFilter: 0,
            trails: {},
            selectedTrailId: ""
        };
    }

    async componentDidMount() {
        if (cachedTrails === null) {
            window.log("Fetching trails");
            cachedTrails = normalize(await fetchTrails());
        }
        this.setState({
            trails: cachedTrails
        });
    }

    onBedsFilterChange(event) {
        this.setState({
            bedsFilter: parseInt(event.target.value)
        });
    }

    onPriceFilterChange(event) {
        this.setState({
            priceFilter: parseInt(event.target.value)
        });
    }

    onTrailChange({ value: selectedTrailId }) {
        this.setState({
            selectedTrailId
        });
    }

    render() {
        let markers = Object.values(this.props.hostels);
        if (this.state.bedsFilter > 0) {
            markers = markers.filter(h => h.beds >= this.state.bedsFilter);
        }
        if (this.state.priceFilter > 0) {
            markers = markers.filter(h => h.price <= this.state.priceFilter);
        }
        markers = markers.map(hostel => ({ lat: hostel.latitude, lng: hostel.longitude, id: hostel.id, beds: hostel.beds }));
        const selectedTrail = this.state.trails[this.state.selectedTrailId];
        const selectedTrailUrl = (selectedTrail && selectedTrail.fileUrl) || null;

        const styles = {
            input: (provided) => ({
                ...provided,
            })
        };
        return (
            <Fragment>
                <div className="hostel-map__controls">
                    <Select
                        styles={styles}
                        options={Object.entries(this.state.trails).map(([id, trail]) => ({ value: id, label: trail.name }))}
                        value={this.state.selectedTrailId}
                        onChange={val => this.onTrailChange(val)}
                        placeholder="Wanderweg wählen..."
                    />
                    <div className="hostel-map__slider-control">
                        <ToggleSlider
                            title="Min. Betten"
                            value={this.state.bedsFilter}
                            sliderComponent={<Slider value={this.state.bedsFilter} onChange={event => this.onBedsFilterChange(event)} />} />
                        <ToggleSlider
                            title="Max. Preis"
                            value={this.state.priceFilter}
                            suffix="€"
                            sliderComponent={<Slider value={this.state.priceFilter} onChange={event => this.onPriceFilterChange(event)} max="20" />} />
                    </div>
                </div>
                {/* Map */}
                <GoogleMap markers={markers} kmlUrl={selectedTrailUrl} />
            </Fragment>
        );
    }
}

HostelMap.propTypes = {
    hostels: PropTypes.object.isRequired,
};