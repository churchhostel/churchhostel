import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function ({ onClick, isOpen }) {
    return (
        <div className="toggle-button" onClick={onClick}>
            {isOpen ?
                <FontAwesomeIcon icon="bars" rotation={90} />
                : <FontAwesomeIcon icon="bars" />
            }
        </div>
    );
}
