import React from "react";
import PropTypes from "prop-types";
import { withScriptjs, withGoogleMap, GoogleMap, Marker, KmlLayer } from "react-google-maps";
import { withRouter } from "react-router-dom";
import { getMarkerIcon } from "../util";

const withMapDefaults = WrappedMapComponent => props => (<WrappedMapComponent
    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8pnCXTFee3HYZ4ItyPU5KAp32qH6akvs&v=3.exp&libraries=places"
    loadingElement={<div style={{ height: `100%` }} />}
    containerElement={<div className="map-container" />}
    mapElement={<div className="map-element" />}
    {...props} />
);

function Map({ markers, kmlUrl, history }) {
    return <GoogleMap
        defaultZoom={8}
        defaultCenter={{ lat: 50.25937, lng: 10.96384 }}> {/*FIXME*/}
        {
            markers.map(marker =>
                <Marker
                    key={marker.id}
                    position={{ lat: marker.lat, lng: marker.lng }}
                    icon={getMarkerIcon(marker.beds)}
                    onClick={() => history.push("/hostel/" + marker.id)}
                />)
        }
        {kmlUrl &&
            <KmlLayer
                url={kmlUrl}
            />
        }
    </GoogleMap>;
}

Map.propTypes = {
    markers: PropTypes.array.isRequired,
};

export default withRouter(withMapDefaults(withScriptjs(withGoogleMap(Map))));