import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const HostelImages = ({
    images,
    isDisabled,
    onUploadImageFile,
    onRemoveImage
}) => {
    return (
        <Fragment>
            <h3>Bilder</h3>
            <div className="view-header-imagebox">
                {images.map(({ id, fileUrl }) => (
                    <div className="image" key={id}>
                        <img src={fileUrl} alt="Ein Raum oder Austattung dieses Hostels" />
                        <button type="button" onClick={() => onRemoveImage(id)} className="image-delete-button">X</button>
                    </div>
                ))}

                <div className="add-image-button-wrapper">
                    <label htmlFor="ImageFileInput" className="add-image-button">
                        <span className="fa-layers fa-fw">
                            <FontAwesomeIcon icon="upload" size="2x" />
                            {isDisabled &&
                                <FontAwesomeIcon icon="ban" size="2x" color="red" />
                            }
                        </span>
                    </label>
                    <input id="ImageFileInput" type="file" style={{ display: "none" }}
                        onChange={event => onUploadImageFile(event.target.files[0])} disabled={isDisabled}
                        accept="image/x-png,image/jpeg" />
                </div>
            </div>
        </Fragment>
    );
};

HostelImages.propTypes = {
    images: PropTypes.array.isRequired,
    isDisabled: PropTypes.bool.isRequired,
    onUploadImageFile: PropTypes.func.isRequired,
    onRemoveImage: PropTypes.func.isRequired,
};

export default HostelImages;