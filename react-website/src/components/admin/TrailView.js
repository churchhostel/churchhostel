import { Field, Form as FormikForm } from "formik";
import PropTypes from "prop-types";
import React, { Fragment } from "react";
import { Alert, Button, Form, Input, InputGroup, InputGroupAddon } from "reactstrap";
import { TrailMap } from "./Map";
import "./TrailView.css";


const TrailView = ({
    errors,
    values,
    setFieldValue,
    isNewTrail, onRemoveTrail
}) => {
    return (
        <Fragment>
            <Form tag={FormikForm}>
                <div className="view-header">
                    <div className="float-right">
                        <Button color="secondary" onClick={onRemoveTrail} disabled={isNewTrail} className="mr-2" formNoValidate>Trail entfernen</Button>
                        <Button color="primary" type="submit">Trail speichern</Button>
                        <br />
                        <b>Aktuelle Weg-ID: {values.id}</b>
                    </div>
                    <h1>{values.name}</h1>
                    <div className="view-section trail-info w-50">
                        <InputGroup className="mb-2">
                            <InputGroupAddon addonType="prepend">Name</InputGroupAddon>
                            <Input tag={Field} name="name" required />
                        </InputGroup>
                        <InputGroup className="mb-2">
                            <InputGroupAddon addonType="prepend">Wegbeginn</InputGroupAddon>
                            <Input tag={Field} name="startLocation" required />
                        </InputGroup>
                        <InputGroup className="mb-2">
                            <InputGroupAddon addonType="prepend">Wegende</InputGroupAddon>
                            <Input tag={Field} name="endLocation" required />
                        </InputGroup>
                        <InputGroup className="mb-2">
                            <InputGroupAddon addonType="prepend">Länge</InputGroupAddon>
                            <Input tag={Field} name="length" type="number" title="Zahlenwert, der die Kilometer angibt" required />
                        </InputGroup>
                        <div className="mt-3">
                            <h6 for="uploadTrailFile">Neue Wegdatei hochladen</h6>
                            <input id="uploadTrailFile" accept=".kml" type="file" name="trailFile" onChange={event => {
                                setFieldValue("trailFile", event.currentTarget.files[0]);
                            }}
                            />
                            {errors.trailFile &&
                                <Alert color="warning">{errors.trailFile}</Alert>
                            }
                        </div>
                    </div>
                </div>
            </Form>
            <TrailMap kmlFile={values.fileUrl} />
        </Fragment>
    );
};

TrailView.propTypes = {
    values: PropTypes.object.isRequired,
    isNewTrail: PropTypes.bool.isRequired,
    onRemoveTrail: PropTypes.func.isRequired,
};

export default TrailView;