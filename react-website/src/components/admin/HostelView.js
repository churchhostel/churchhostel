import { Field, FieldArray, Form as FormikForm } from "formik";
import PropTypes from "prop-types";
import React, { Fragment } from "react";
import { Button, Form, Input, InputGroup, InputGroupAddon } from "reactstrap";
import "./HostelView.css";
import Map from "./Map";



const HostelView = ({
    values,
    isNewHostel, onRemoveHostel,
    createEmptyContactObject,
    imageBox,
}) => {
    function createHeaderButtons() {
        return (
            <Fragment>
                <div className="view-header-buttons float-right">
                    <Button color="secondary" onClick={() => onRemoveHostel()} disabled={isNewHostel} className="mr-2" formNoValidate="">Hostel entfernen</Button>
                    <Button color="primary" type="submit">Hostel speichern</Button>
                    <br />
                    <b>Aktuelle Hostel-ID: {values.id}</b>
                </div>
            </Fragment>
        );
    }
    function createNameInput() {
        return (
            <div className="view-header-name display-4">
                <InputGroup className="w-50">
                    <InputGroupAddon addonType="prepend">Name</InputGroupAddon>
                    <Input tag={Field} name="name" required />
                </InputGroup>
            </div>);
    }
    function createAddressInput() {
        return (
            <InputGroup className="view-section w-50">
                <InputGroupAddon addonType="prepend">Adresse</InputGroupAddon>
                <Input tag={Field} name="address" required />
            </InputGroup>
        );
    }
    function createEmailInput() {
        return (
            <InputGroup className="view-section w-50">
                <InputGroupAddon addonType="prepend">E-Mail</InputGroupAddon>
                <Input tag={Field} type="email" name="email" value={values.email || ""} title="Bitte gib eine gültige E-Mail-Adresse ein" />
            </InputGroup>);
    }
    function createContacts() {
        return (
            <div id="contacts" className="view-section">
                <div className="view-section-heading">
                    <h3>Kontakte</h3>
                </div>
                <FieldArray name="contacts" render={arrayHelpers => (
                    <Fragment>
                        {values.contacts.map(({ id }, i) => (
                            <div key={i} >
                                <InputGroup className="mb-2 contact">
                                    <InputGroupAddon addonType="prepend">Name</InputGroupAddon>
                                    <Input tag={Field} name={`contacts.${i}.name`} required />
                                    <InputGroupAddon addonType="prepend">Tel.</InputGroupAddon>
                                    <Input tag={Field} name={`contacts.${i}.phone`} pattern="^[+]?[\d ]+" title="Telefonnummern dürfen nur Ziffern und Leerzeichen beinhalten. Ein '+' am Anfang für die Ländervorwahl ist erlaubt" required />
                                    <InputGroupAddon addonType="append">
                                        <Button onClick={() => arrayHelpers.remove(i)} color="secondary" className="remove-input-button btn-outline">Entfernen</Button>
                                    </InputGroupAddon>
                                </InputGroup>
                                <InputGroup className="mb-3">
                                    <InputGroupAddon addonType="prepend">E-Mail</InputGroupAddon>
                                    <Input tag={Field} name={`contacts.${i}.email`} pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" />
                                </InputGroup>
                            </div>
                        ))}
                        <Button type="button" onClick={() => arrayHelpers.push(createEmptyContactObject())}>Kontakt hinzufügen</Button>
                    </Fragment>
                )} />
            </div>
        );
    }

    function createBedsInput() {
        return (
            <Fragment>
                <h3 className="view-section-heading">Betten</h3>
                <div className="w-25">
                    <Input tag={Field} name="beds" type="number" title="Gültig ist eine Zahl zwischen 1 und 50" min="1" max="50" required />
                </div>
            </Fragment>
        );
    }

    function createFacilities() {
        return (
            <Fragment>
                <div className="view-section-heading">
                    <h3>Ausstattung der Unterkunft</h3>
                </div>
                <FieldArray name="facilities" render={arrayHelpers => (
                    <Fragment>
                        {values.facilities.map((f, i) => (
                            <InputGroup className="mb-2" key={i}>
                                <Input tag={Field} name={`facilities.${i}`} />
                                <InputGroupAddon addonType="append">
                                    <Button color="secondary" onClick={() => arrayHelpers.remove(i)}>Entfernen</Button>
                                </InputGroupAddon>
                            </InputGroup>
                        ))}
                        <Button type="button" onClick={() => arrayHelpers.push("")}>Ausstattung hinzufügen</Button>
                    </Fragment>
                )}
                />
            </Fragment>
        );
    }
    function createServices() {
        return (
            <Fragment>
                <div className="view-section-heading">
                    <h3>Serviceangebot</h3>
                </div>
                <FieldArray name="services" render={arrayHelpers => (
                    <Fragment>
                        {values.services.map((s, i) => (
                            <InputGroup className="mb-2" key={i}>
                                <Input tag={Field} name={`services.${i}`} />
                                <InputGroupAddon addonType="append">
                                    <Button color="secondary" onClick={() => arrayHelpers.remove(i)}>Entfernen</Button>
                                </InputGroupAddon>
                            </InputGroup>
                        ))}
                        <Button type="button" onClick={() => arrayHelpers.push("")}>Service hinzufügen</Button>
                    </Fragment>
                )} />
            </Fragment>
        );
    }
    function createAdditionalHints() {
        return (
            <Fragment>
                <div className="view-section-heading">
                    <h3>Hinweise</h3>
                </div>
                <FieldArray name="additionalHints" render={arrayHelpers => (
                    <Fragment>
                        {values.additionalHints.map((h, i) => (
                            <InputGroup className="mb-2" key={i}>
                                <Input tag={Field} name={`additionalHints.${i}`} />
                                <InputGroupAddon addonType="append">
                                    <Button color="secondary" onClick={() => arrayHelpers.remove(i)}>Entfernen</Button>
                                </InputGroupAddon>
                            </InputGroup>
                        ))}
                        <Button type="button" onClick={() => arrayHelpers.push("")}>Hinweis hinzufügen</Button>
                    </Fragment>
                )} />
            </Fragment>
        );
    }
    function createPriceInput() {
        return (<Fragment>
            <h3 className="view-section-heading">Preis pro Person</h3>
            <InputGroup>
                <Input tag={Field} name="price" type="number" min="1" max="50" step="any" title="Eine Zahl, die den Preis in Euro angibt" required />
                <InputGroupAddon addonType="append">€</InputGroupAddon>
            </InputGroup>
        </Fragment>);
    }

    function createChurchhostelMarkerInput() {
        return (
            <Fragment>
                <h3 className="view-section-heading">Markierung des Churchhostels</h3>
                <InputGroup>
                    <InputGroupAddon addonType="prepend">Lat/Long</InputGroupAddon>
                    <Input tag={Field} name="latitude" type="number" min="-85" max="85" step="any" required />
                    <Input tag={Field} name="longitude" type="number" min="-180" max="180" step="any" required />
                </InputGroup>
            </Fragment>
        );
    }

    return (
        <div id="hostelView">
            <Form tag={FormikForm}>
                <div className="view-header">
                    {createHeaderButtons()}
                    <h1>{values.name}</h1>
                    {createNameInput()}
                    {createAddressInput()}
                    {createEmailInput()}
                    {imageBox}
                </div>
                <div className="view-sides-wrapper">
                    <div className="view-side view-left-side">
                        {createContacts()}
                        <div className="view-section view-section-beds">
                            {createBedsInput()}
                        </div>
                        <div id="facilities" className="view-section">
                            {createFacilities()}
                        </div>
                        <div id="services" className="view-section">
                            {createServices()}
                        </div>
                        <div id="additionalHints" className="view-section">
                            {createAdditionalHints()}
                        </div>
                        <div className="view-section view-section-price">
                            {createPriceInput()}
                        </div>
                    </div>

                    <div className="view-side view-right-side">
                        <div className="view-section">
                            {createChurchhostelMarkerInput()}
                        </div>
                        <Map latitude={values.latitude} longitude={values.longitude} beds={values.beds} />
                    </div>
                </div>
            </Form>
        </div >
    );
};

HostelView.propTypes = {
    onRemoveHostel: PropTypes.func.isRequired,
    imageBox: PropTypes.element.isRequired,
};


export default HostelView;
