import React from 'react';
import PropTypes from "prop-types";
import { withScriptjs, withGoogleMap, GoogleMap, Marker, KmlLayer } from "react-google-maps";
import { getMarkerIcon } from "../../util";

const withMapDefaults = WrappedMapComponent => props => (<WrappedMapComponent
    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8pnCXTFee3HYZ4ItyPU5KAp32qH6akvs&v=3.exp"
    loadingElement={<div style={{ height: `100%` }} />}
    containerElement={<div className="map-wrapper" />}
    mapElement={<div id="hostelMap" />}
    {...props} />
);

const Map = withMapDefaults(withScriptjs(withGoogleMap(props => (
    <GoogleMap
        defaultZoom={8}
        center={props.center}>
        {
            props.children
        }
    </GoogleMap>
))));

function HostelMarkerMap(props) {
    const markerPosition = { lat: props.latitude, lng: props.longitude };
    const marker = <Marker position={markerPosition} icon={getMarkerIcon(props.beds)} />;
    return <Map center={markerPosition} children={marker} />;
}

function TrailMap(props) {
    if (props.kmlFile) {
        const kmlUrlWithDisabledCaching = `${props.kmlFile}?dummy=${new Date().getTime()}`;
        const kmlLayer = <KmlLayer url={kmlUrlWithDisabledCaching} />;
        return <Map children={kmlLayer} />;
    } else {
        return <Map />;
    }
}

HostelMarkerMap.propTypes = {
    latitude: PropTypes.number,
    longitude: PropTypes.number,
    beds: PropTypes.number,
};

HostelMarkerMap.propTypes = {
    kmlFile: PropTypes.string
};

export default HostelMarkerMap;
export { TrailMap };