import React from "react";
import PropTypes from "prop-types";
import { Form, Input, InputGroup, InputGroupAddon, Modal, ModalHeader, ModalBody, ModalFooter, Button } from "reactstrap";
import { Field, Form as FormikForm } from "formik";

const Login = ({ values }) => (
    <Modal isOpen={true} zIndex={2}>
        <Form tag={FormikForm}>
            <ModalHeader>Anmelden</ModalHeader>
            <ModalBody>
                <InputGroup className="mb-2">
                    <InputGroupAddon addonType="prepend">Name</InputGroupAddon>
                    <Input tag={Field} name="name" required />
                </InputGroup>
                <InputGroup className="mb-2">
                    <InputGroupAddon addonType="prepend">Passwort</InputGroupAddon>
                    <Input tag={Field} name="password" type="password" required />
                </InputGroup>
            </ModalBody>
            <ModalFooter>
                <Button color="primary" type="submit">Anmelden</Button>
            </ModalFooter>
        </Form>
    </Modal>
);

Login.propTypes = {
    values: PropTypes.object.isRequired
};

export default Login;