import PropTypes from "prop-types";
import React from "react";
import "./SidebarList.css";


const SidebarList = ({ title, list, onNewClick, onSelectClick, onFilterChange }) => {
    return (
        <div className="sidebarlist">
            <div className="sidebarlist__header">
                <input className="form-control sidebarlist__input" placeholder={title + " durchsuchen..."} onChange={onFilterChange} />
                <button className="btn btn-primary" onClick={() => onNewClick()}>Neu</button>
            </div>
            <div className="sidebarlist__content">
                <ul className="list-group">
                    {list.map(({ id, name }) => <li key={id} className="list-group-item" onClick={() => onSelectClick(id)}>{name}</li>)}
                </ul>
            </div>
        </div>
    );
};

SidebarList.propTypes = {
    title: PropTypes.string.isRequired,
    list: PropTypes.array.isRequired,
    onNewClick: PropTypes.func.isRequired,
    onSelectClick: PropTypes.func.isRequired,
};

export const HostelSidebarList = (props) => <SidebarList title="Hostels" {...props} />;
export const TrailSidebarList = (props) => <SidebarList title="Wanderwege" {...props} />;

export default SidebarList;