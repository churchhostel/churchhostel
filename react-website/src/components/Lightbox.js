import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import "./Lightbox.css";

export default function Lightbox({ children, onClose }) {
    return (
        <div className="lightbox" onClick={onClose}>
            <div className="lightbox__content">
                {children}
            </div>
            <span className="fa-layers fa-fw lightbox__close">
                <FontAwesomeIcon icon="circle" size="3x" color="white" />
                <FontAwesomeIcon icon="times-circle" size="3x" color="black" />
            </span>
        </div>
    );
}

