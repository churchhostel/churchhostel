import React, { Fragment } from "react";
import PropTypes from "prop-types";

import "./SearchInput.css";

export default function SearchInput({ onChange }) {
    return (
        <Fragment>
            <input className="hostel-search__input form-control"
                onChange={onChange}
                placeholder="Name, Ort oder Adresse suchen..."
            />
        </Fragment>
    );
}

SearchInput.propTypes = {
    onChange: PropTypes.func.isRequired,
};
