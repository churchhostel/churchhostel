import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import "./HostelList.css";
import NoImageAvailableImage from "../../images/no_image_available.png";

export default function HostelList({ hostels }) {
    const matchItems = hostels.map(match => (
        <ListEntry key={match.id} id={match.id} name={match.name} address={match.address} imageLink={match.images[0] ? match.images[0].fileUrl : undefined} />
    ));
    return matchItems.length > 0 && (
        <ul className="hostel-search__list">{matchItems}</ul>
    );
}

function ListEntry({ id, name, address, imageLink }) {
    const imageSrc = imageLink ? `${imageLink}?thumb=true` : NoImageAvailableImage;
    return (
        <li className="list-entry card">
            <img className="card-img-top list-entry__img" src={imageSrc} alt="" />
            <div className="card-body list-entry__body">
                <h4 className="card-title">{name}</h4>
                <p className="card-text">{address}</p>
                <Link to={"/hostel/" + id} className="list-entry__link card-link btn btn-primary">Zum Hostel</Link>
            </div>
        </li>
    );
}

HostelList.propTypes = {
    hostels: PropTypes.array.isRequired
};
HostelList.defaultProps = {
    hostels: []
};
ListEntry.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    address: PropTypes.string.isRequired,
    imageLink: PropTypes.string
};
