import React from "react";
import PropTypes from "prop-types";

import "./Slider.css";

export default function Slider({ value, onChange, max }) {
    return (
        <label className="slider">
            <input min="0" max={max} value={value} type="range" onChange={e => onChange(e)} className="slider__input" autoFocus />
        </label>
    );
}

Slider.propTypes = {
    onChange: PropTypes.func.isRequired,
    value: PropTypes.number.isRequired,
    max: PropTypes.number,
};

Slider.defaultProps = {
    max: 10,
};