import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import "./NavLinks.css";

const linkMap = {
    "/howitworks": "So funktioniert's",
    "/hostel": "Churchhostel finden",
    "/contact": "Kontakt",
    "/about": "Über Churchhostel",
    "/imprint": "Impressum"
};
function createLink(onClick) {
    return ([href, text]) => (
        <Link to={href} onClick={onClick} key={href} className="nav__link">
            {text}
        </Link>
    );
}

export default function NavLinks({ className, onClick }) {
    const links = Object.entries(linkMap).map(createLink(onClick));
    return (
        <nav className={`nav ${className}`}>
            {links}
        </nav>
    );
}

export const HeaderNavLinks = ({ className, onClick }) => (
    NavLinks({ className: `nav--header ${className}`, onClick })
);

export const FooterNavLinks = ({ className, onClick }) => (
    NavLinks({ className: `nav--inline ${className}`, onClick })
);

NavLinks.propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func
};
NavLinks.defaultProps = {
    className: "",
    onClick: null
};