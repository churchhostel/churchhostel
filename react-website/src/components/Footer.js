import React from "react";
import { FooterNavLinks } from "./NavLinks";

import "./Footer.css";

export default () => (
    <div className="footer">
        <FooterNavLinks />
        <div className="footer__copyright">
        </div>
    </div >
);
