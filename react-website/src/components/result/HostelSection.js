import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withClass } from "../../hocs";

import "./HostelSection.css";

export default function HostelSection({ heading, children, className }) {
    return (
        <div className={classNames("hostelsection", className)}>
            <h4 className="hostelsection__heading">{heading}</h4>
            <div>
                {children}
            </div>
        </div>
    );
}

export const ContactsSection = withClass("hostelsection--contacts")(HostelSection);
export const PriceSection = withClass("hostelsection--price")(HostelSection);

HostelSection.propTypes = {
    heading: PropTypes.string.isRequired,
    children: PropTypes.node,
    className: PropTypes.string
};
HostelSection.defaultProps = {
};