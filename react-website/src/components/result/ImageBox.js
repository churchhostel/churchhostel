import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import "./ImageBox.css";

export default function ImageBox({ images, className }) {
    return (
        <div className={classNames("imagebox", className)}>
            {images.map(({ url, onClick }, index) => <img key={index} src={url} alt="Representation of the currently selected hostel" onClick={onClick} />)}
        </div>
    );
}

ImageBox.propTypes = {
    images: PropTypes.array.isRequired,
    className: PropTypes.string,
};
ImageBox.defaultProps = {
    images: [],
};