import React from "react";
import { Link } from "react-router-dom";
import ToggleMenuButton from "./ToggleMenuButton";
import { HeaderNavLinks } from "./NavLinks";
import classnames from "classnames";

import "./Menu.css";

import logoImg from "../images/logo.png";

export default function Menu({ isOpen, onToggleClick }) {
    return (
        <div className={classnames("menu", { "menu--open": isOpen })}>
            <Link to="/" className="logo">
                <img className="logo__image" src={logoImg} alt="Churchhostel Logo" />
                <div className="logo__text">
                    <h1 className="logo__main">Churchhostel</h1>
                    <h2 className="logo__sub">- einfach übernachten</h2>
                </div>
            </Link>
            <HeaderNavLinks onClick={onToggleClick} className={classnames("menu-links", { "menu-links--open": isOpen })} />
            <div className="menu__toggle">
                <ToggleMenuButton onClick={onToggleClick} isOpen={isOpen} />
            </div>
        </div>
    );
}