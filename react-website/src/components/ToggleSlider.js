import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import classnames from "classnames";

import "./ToggleSlider.css";

class ToggleSlider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpened: false
        };
    }

    toggle() {
        this.setState(prevState => ({
            isOpened: !prevState.isOpened
        }));
    }

    close() {
        this.setState({ isOpened: false });
    }

    render() {
        return (
            <Fragment>
                <span className={classnames("toggleslider__display", { "toggled": this.state.isOpened })} onClick={() => this.toggle()}>
                    <span>{this.props.title}:</span>
                    <span>{this.props.value > 0 ? `${this.props.value}${this.props.suffix}` : "-"}</span>
                    <span><FontAwesomeIcon icon="caret-down" color="silver" /></span>
                    {
                        this.state.isOpened &&
                        <div className="toggleslider__slider" onBlur={() => this.close()}>
                            {this.props.sliderComponent}
                        </div>
                    }
                </span>
            </Fragment >
        );
    }
}

ToggleSlider.propTypes = {
    sliderComponent: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired,
    suffix: PropTypes.string,
};

ToggleSlider.defaultProps = {
    suffix: ""
};

export default ToggleSlider;